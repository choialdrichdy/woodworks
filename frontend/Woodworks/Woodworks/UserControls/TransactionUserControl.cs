﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Woodworks.Models;
using RestSharp;
using Woodworks.Network;
using Newtonsoft.Json;
using Equin.ApplicationFramework;
using Woodworks.Services;

namespace Woodworks.UserControls
{
    public partial class TransactionUserControl : UserControl
    {
        RestClient client;
        List<Order> orders;
        List<Customer> customers;
        List<Wood> woods;
        List<Discount> discounts;

        BindingListView<Order> orderlist;

        Customer selectedCustomer;
        Wood selectedWood;
        public TransactionUserControl()
        {
            InitializeComponent();
            init();
        }
        public void init()
        {
            client = WoodworksClient.Instance;

            var request = new RestRequest("customers", Method.GET);

            IRestResponse response = client.Execute(request);
            var content = response.Content;


            customers = JsonConvert.DeserializeObject<List<Customer>>(content);

            orders = new List<Order>();
            orderlist = new BindingListView<Order>(orders);
            orderDGV.DataSource = orderlist;

            customerCombobox.DataSource = customers;
            customerCombobox.DisplayMember = "customer_name";

            var customerAutocomplete = new AutoCompleteStringCollection();
            customerAutocomplete.AddRange(customers.Select(customer => customer.customer_name).ToArray());
            customerCombobox.AutoCompleteCustomSource = customerAutocomplete;
            customerCombobox.SelectedIndex = -1;

        }

        private void customerCombobox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(customerCombobox.SelectedIndex != -1)
            {
                selectedCustomer = (Customer)customerCombobox.SelectedItem;
                setCustomerCombobox(selectedCustomer);

                woods = requestWood(selectedCustomer);

                woodCombobox.DataSource = woods;
                woodCombobox.DisplayMember = "full_name";

                var woodAutocomplete = new AutoCompleteStringCollection();
                woodAutocomplete.AddRange(woods.Select(wood => wood.full_name).ToArray());
                woodCombobox.AutoCompleteCustomSource = woodAutocomplete;
                woodCombobox.SelectedIndex = -1;

                woodGroupbox.Enabled = true;
            }
        }

        private void woodCombobox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(woodCombobox.SelectedIndex != -1)
            {
                selectedWood = (Wood)woodCombobox.SelectedItem;
                setWoodCombobox(selectedWood);
            }
        }

        private void setWoodCombobox(Wood wood)
        {
            typeTextbox.Text = wood.wood_type.ToString();
            lengthTextbox.Text = wood.wood_thickness.ToString();
            widthTextbox.Text = wood.wood_width.ToString();
            heightTextbox.Text = wood.wood_height.ToString();
            priceTextbox.Text = wood.wood_price.ToString();
        }

        private void setCustomerCombobox(Customer customer)
        {
            addressTextbox.Text = customer.customer_address;
            currentBalanceTextbox.Text = customer.current_balance.ToString();
            termsTextbox.Text = customer.customer_terms.ToString();
        }

        private void confirmButton_Click(object sender, EventArgs e)
        {
            if (selectedWood != null)
            {
                Order order = new Order();
                order.wood_id = selectedWood.id;
                order.wood_type = selectedWood.wood_type;
                order.wood_thickness = Services.Services.doubleToFraction(selectedWood.wood_thickness);
                order.wood_width = Services.Services.doubleToFraction(selectedWood.wood_width);
                order.wood_height = Services.Services.doubleToFraction(selectedWood.wood_height);
                order.wood_price = selectedWood.wood_price;
                order.quantity = (int) quantityNumeric.Value;
                order.bf_lf = Services.Services.computeBFLF(order);

                Discount discount = discounts.SingleOrDefault(i => i.wood_id == order.wood_id) ?? new Discount();
                order.discount_percent = discount.discount_percent;
                order.total_price = (order.bf_lf * order.wood_price * (100 - order.discount_percent)) / 100;

                addToOrders(order);
            }
        }

        private void deleteButton_Click(object sender, EventArgs e)
        {

        }

        private List<Wood> requestWood(Customer selectedCustomer)
        {
            var request = new RestRequest("customers/" + selectedCustomer.id + "/woods", Method.GET);

            IRestResponse response = client.Execute(request);
            var content = response.Content;


            var request2 = new RestRequest("customers/" + selectedCustomer.id + "/discounts", Method.GET);

            IRestResponse response2 = client.Execute(request2);
            var content2 = response2.Content;

            discounts = JsonConvert.DeserializeObject<List<Discount>>(content2);
            return JsonConvert.DeserializeObject<List<Wood>>(content);
        }

        private void addToOrders(Order order)
        {
            if (!orders.Any(i => i.wood_id == order.wood_id))
            {
                orders.Add(order);
            }
            else
            {
                Order existingOrder = orders.Single(i => i.wood_id == order.wood_id);
                existingOrder.quantity = existingOrder.quantity + order.quantity;
                existingOrder.bf_lf = Services.Services.computeBFLF(existingOrder);
                existingOrder.total_price = (existingOrder.bf_lf * existingOrder.wood_price * (100 - existingOrder.discount_percent)) / 100;
            }
            orderlist.Refresh();
            setTotalAmount();
        }

        private void setTotalAmount()
        {
            float totalPrice = 0;
            foreach (Order order in orders)
            {
                totalPrice = totalPrice + order.total_price;
            }
            totalAmountTextbox.Text = totalPrice.ToString();
        }

        private float getTruePrice(Order order)
        {
            return order.wood_price;
        }

        private void addTransactionButton_Click(object sender, EventArgs e)
        {
            Transaction transaction = new Transaction();
            transaction.total_amount = float.Parse(totalAmountTextbox.Text);
            transaction.customer_id = selectedCustomer.id;
            transaction.delivery_receipt_no = drTextbox.Text;
            transaction.details = orders;
            string transactionjson = JsonConvert.SerializeObject(transaction);

            var request = new RestRequest("transactions", Method.POST);
            request.AddParameter("application/json", transactionjson, ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            var content = response.Content;

        }
    }
}
