﻿namespace Woodworks.UserControls
{
    partial class DiscountUserControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.rulesDataGridView = new System.Windows.Forms.DataGridView();
            this.discountsDataGridView = new System.Windows.Forms.DataGridView();
            this.rulesLabel = new System.Windows.Forms.Label();
            this.discountsLabel = new System.Windows.Forms.Label();
            this.addRuleButton = new System.Windows.Forms.Button();
            this.setCustomerBtn = new System.Windows.Forms.Button();
            this.addSpecialPricesButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.rulesDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.discountsDataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // rulesDataGridView
            // 
            this.rulesDataGridView.AllowUserToAddRows = false;
            this.rulesDataGridView.AllowUserToDeleteRows = false;
            this.rulesDataGridView.AllowUserToResizeColumns = false;
            this.rulesDataGridView.AllowUserToResizeRows = false;
            this.rulesDataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.rulesDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.rulesDataGridView.Location = new System.Drawing.Point(6, 31);
            this.rulesDataGridView.MultiSelect = false;
            this.rulesDataGridView.Name = "rulesDataGridView";
            this.rulesDataGridView.ReadOnly = true;
            this.rulesDataGridView.RowHeadersVisible = false;
            this.rulesDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.rulesDataGridView.Size = new System.Drawing.Size(582, 277);
            this.rulesDataGridView.TabIndex = 0;
            this.rulesDataGridView.SelectionChanged += new System.EventHandler(this.rulesDataGridView_selectionchanged);
            // 
            // discountsDataGridView
            // 
            this.discountsDataGridView.AllowUserToAddRows = false;
            this.discountsDataGridView.AllowUserToDeleteRows = false;
            this.discountsDataGridView.AllowUserToResizeColumns = false;
            this.discountsDataGridView.AllowUserToResizeRows = false;
            this.discountsDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.discountsDataGridView.Location = new System.Drawing.Point(6, 327);
            this.discountsDataGridView.Name = "discountsDataGridView";
            this.discountsDataGridView.ReadOnly = true;
            this.discountsDataGridView.RowHeadersVisible = false;
            this.discountsDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.discountsDataGridView.Size = new System.Drawing.Size(582, 277);
            this.discountsDataGridView.TabIndex = 1;
            // 
            // rulesLabel
            // 
            this.rulesLabel.AutoSize = true;
            this.rulesLabel.Location = new System.Drawing.Point(3, 15);
            this.rulesLabel.Name = "rulesLabel";
            this.rulesLabel.Size = new System.Drawing.Size(40, 13);
            this.rulesLabel.TabIndex = 2;
            this.rulesLabel.Text = "Rules :";
            // 
            // discountsLabel
            // 
            this.discountsLabel.AutoSize = true;
            this.discountsLabel.Location = new System.Drawing.Point(3, 311);
            this.discountsLabel.Name = "discountsLabel";
            this.discountsLabel.Size = new System.Drawing.Size(60, 13);
            this.discountsLabel.TabIndex = 3;
            this.discountsLabel.Text = "Discounts :";
            // 
            // addRuleButton
            // 
            this.addRuleButton.Location = new System.Drawing.Point(614, 31);
            this.addRuleButton.Name = "addRuleButton";
            this.addRuleButton.Size = new System.Drawing.Size(75, 23);
            this.addRuleButton.TabIndex = 4;
            this.addRuleButton.Text = "Add Rule";
            this.addRuleButton.UseVisualStyleBackColor = true;
            this.addRuleButton.Click += new System.EventHandler(this.addRuleButton_Click);
            // 
            // setCustomerBtn
            // 
            this.setCustomerBtn.Location = new System.Drawing.Point(614, 98);
            this.setCustomerBtn.Name = "setCustomerBtn";
            this.setCustomerBtn.Size = new System.Drawing.Size(244, 23);
            this.setCustomerBtn.TabIndex = 5;
            this.setCustomerBtn.Text = "Set customer";
            this.setCustomerBtn.UseVisualStyleBackColor = true;
            this.setCustomerBtn.Click += new System.EventHandler(this.setCustomerBtn_Click);
            // 
            // addSpecialPricesButton
            // 
            this.addSpecialPricesButton.Location = new System.Drawing.Point(737, 30);
            this.addSpecialPricesButton.Name = "addSpecialPricesButton";
            this.addSpecialPricesButton.Size = new System.Drawing.Size(75, 23);
            this.addSpecialPricesButton.TabIndex = 6;
            this.addSpecialPricesButton.Text = "Add Special Prices";
            this.addSpecialPricesButton.UseVisualStyleBackColor = true;
            this.addSpecialPricesButton.Click += new System.EventHandler(this.addSpecialPricesButton_Click);
            // 
            // DiscountUserControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.addSpecialPricesButton);
            this.Controls.Add(this.setCustomerBtn);
            this.Controls.Add(this.addRuleButton);
            this.Controls.Add(this.discountsLabel);
            this.Controls.Add(this.rulesLabel);
            this.Controls.Add(this.discountsDataGridView);
            this.Controls.Add(this.rulesDataGridView);
            this.Name = "DiscountUserControl";
            this.Size = new System.Drawing.Size(875, 622);
            ((System.ComponentModel.ISupportInitialize)(this.rulesDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.discountsDataGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView rulesDataGridView;
        private System.Windows.Forms.DataGridView discountsDataGridView;
        private System.Windows.Forms.Label rulesLabel;
        private System.Windows.Forms.Label discountsLabel;
        private System.Windows.Forms.Button addRuleButton;
        private System.Windows.Forms.Button setCustomerBtn;
        private System.Windows.Forms.Button addSpecialPricesButton;
    }
}
