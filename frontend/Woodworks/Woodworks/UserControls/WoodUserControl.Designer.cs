﻿namespace Woodworks.UserControls
{
    partial class WoodUserControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.woodDGV = new System.Windows.Forms.DataGridView();
            this.editWoodButton = new System.Windows.Forms.Button();
            this.deleteWoodButton = new System.Windows.Forms.Button();
            this.addWoodButton = new System.Windows.Forms.Button();
            this.addStockButton = new System.Windows.Forms.Button();
            this.woodControlGroupbox = new System.Windows.Forms.GroupBox();
            this.excelAddButton = new System.Windows.Forms.Button();
            this.inventoryGroupbox = new System.Windows.Forms.GroupBox();
            this.excelButton = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.searchTextbox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.numericUpDown1 = new System.Windows.Forms.NumericUpDown();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.woodDGV)).BeginInit();
            this.woodControlGroupbox.SuspendLayout();
            this.inventoryGroupbox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).BeginInit();
            this.SuspendLayout();
            // 
            // woodDGV
            // 
            this.woodDGV.AllowUserToAddRows = false;
            this.woodDGV.AllowUserToDeleteRows = false;
            this.woodDGV.AllowUserToResizeColumns = false;
            this.woodDGV.AllowUserToResizeRows = false;
            this.woodDGV.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.woodDGV.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.woodDGV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.woodDGV.Location = new System.Drawing.Point(3, 39);
            this.woodDGV.MultiSelect = false;
            this.woodDGV.Name = "woodDGV";
            this.woodDGV.ReadOnly = true;
            this.woodDGV.RowHeadersVisible = false;
            this.woodDGV.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.woodDGV.Size = new System.Drawing.Size(617, 597);
            this.woodDGV.TabIndex = 0;
            // 
            // editWoodButton
            // 
            this.editWoodButton.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.editWoodButton.Location = new System.Drawing.Point(152, 24);
            this.editWoodButton.Name = "editWoodButton";
            this.editWoodButton.Size = new System.Drawing.Size(110, 23);
            this.editWoodButton.TabIndex = 2;
            this.editWoodButton.Text = "Edit Wood";
            this.editWoodButton.UseVisualStyleBackColor = true;
            this.editWoodButton.Click += new System.EventHandler(this.editWoodButton_Click);
            // 
            // deleteWoodButton
            // 
            this.deleteWoodButton.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.deleteWoodButton.Location = new System.Drawing.Point(152, 53);
            this.deleteWoodButton.Name = "deleteWoodButton";
            this.deleteWoodButton.Size = new System.Drawing.Size(110, 23);
            this.deleteWoodButton.TabIndex = 3;
            this.deleteWoodButton.Text = "Delete Wood";
            this.deleteWoodButton.UseVisualStyleBackColor = true;
            this.deleteWoodButton.Click += new System.EventHandler(this.deleteWoodButton_Click);
            // 
            // addWoodButton
            // 
            this.addWoodButton.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.addWoodButton.Location = new System.Drawing.Point(6, 24);
            this.addWoodButton.Name = "addWoodButton";
            this.addWoodButton.Size = new System.Drawing.Size(110, 23);
            this.addWoodButton.TabIndex = 4;
            this.addWoodButton.Text = "Add Wood";
            this.addWoodButton.UseVisualStyleBackColor = true;
            this.addWoodButton.Click += new System.EventHandler(this.addWoodButton_Click);
            // 
            // addStockButton
            // 
            this.addStockButton.Location = new System.Drawing.Point(6, 39);
            this.addStockButton.Name = "addStockButton";
            this.addStockButton.Size = new System.Drawing.Size(110, 23);
            this.addStockButton.TabIndex = 5;
            this.addStockButton.Text = "Add Stock";
            this.addStockButton.UseVisualStyleBackColor = true;
            this.addStockButton.Click += new System.EventHandler(this.addStockButton_Click);
            // 
            // woodControlGroupbox
            // 
            this.woodControlGroupbox.Controls.Add(this.excelAddButton);
            this.woodControlGroupbox.Controls.Add(this.addWoodButton);
            this.woodControlGroupbox.Controls.Add(this.deleteWoodButton);
            this.woodControlGroupbox.Controls.Add(this.editWoodButton);
            this.woodControlGroupbox.Location = new System.Drawing.Point(632, 13);
            this.woodControlGroupbox.Name = "woodControlGroupbox";
            this.woodControlGroupbox.Size = new System.Drawing.Size(271, 100);
            this.woodControlGroupbox.TabIndex = 6;
            this.woodControlGroupbox.TabStop = false;
            this.woodControlGroupbox.Text = "Wood Controls";
            // 
            // excelAddButton
            // 
            this.excelAddButton.Location = new System.Drawing.Point(6, 53);
            this.excelAddButton.Name = "excelAddButton";
            this.excelAddButton.Size = new System.Drawing.Size(110, 23);
            this.excelAddButton.TabIndex = 5;
            this.excelAddButton.Text = "Bulk Add";
            this.excelAddButton.UseVisualStyleBackColor = true;
            this.excelAddButton.Click += new System.EventHandler(this.excelAddButton_Click);
            // 
            // inventoryGroupbox
            // 
            this.inventoryGroupbox.Controls.Add(this.excelButton);
            this.inventoryGroupbox.Controls.Add(this.addStockButton);
            this.inventoryGroupbox.Location = new System.Drawing.Point(632, 130);
            this.inventoryGroupbox.Name = "inventoryGroupbox";
            this.inventoryGroupbox.Size = new System.Drawing.Size(271, 100);
            this.inventoryGroupbox.TabIndex = 7;
            this.inventoryGroupbox.TabStop = false;
            this.inventoryGroupbox.Text = "Inventory Management";
            // 
            // excelButton
            // 
            this.excelButton.Location = new System.Drawing.Point(152, 39);
            this.excelButton.Name = "excelButton";
            this.excelButton.Size = new System.Drawing.Size(110, 23);
            this.excelButton.TabIndex = 8;
            this.excelButton.Text = "Excel Add";
            this.excelButton.UseVisualStyleBackColor = true;
            this.excelButton.Click += new System.EventHandler(this.excelButton_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(18, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(47, 13);
            this.label1.TabIndex = 8;
            this.label1.Text = "Search :";
            // 
            // searchTextbox
            // 
            this.searchTextbox.Location = new System.Drawing.Point(71, 13);
            this.searchTextbox.Name = "searchTextbox";
            this.searchTextbox.Size = new System.Drawing.Size(164, 20);
            this.searchTextbox.TabIndex = 9;
            this.searchTextbox.TextChanged += new System.EventHandler(this.searchTextbox_TextChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(389, 16);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(105, 13);
            this.label2.TabIndex = 10;
            this.label2.Text = "Show stocks below :";
            // 
            // numericUpDown1
            // 
            this.numericUpDown1.Location = new System.Drawing.Point(500, 13);
            this.numericUpDown1.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numericUpDown1.Name = "numericUpDown1";
            this.numericUpDown1.Size = new System.Drawing.Size(120, 20);
            this.numericUpDown1.TabIndex = 11;
            this.numericUpDown1.ValueChanged += new System.EventHandler(this.numericUpDown1_ValueChanged);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(632, 237);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(116, 23);
            this.button1.TabIndex = 12;
            this.button1.Text = "Clear Search";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(784, 236);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(110, 23);
            this.button2.TabIndex = 13;
            this.button2.Text = "Refresh";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // WoodUserControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.numericUpDown1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.searchTextbox);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.inventoryGroupbox);
            this.Controls.Add(this.woodControlGroupbox);
            this.Controls.Add(this.woodDGV);
            this.Name = "WoodUserControl";
            this.Size = new System.Drawing.Size(916, 652);
            ((System.ComponentModel.ISupportInitialize)(this.woodDGV)).EndInit();
            this.woodControlGroupbox.ResumeLayout(false);
            this.inventoryGroupbox.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView woodDGV;
        private System.Windows.Forms.Button editWoodButton;
        private System.Windows.Forms.Button deleteWoodButton;
        private System.Windows.Forms.Button addWoodButton;
        private System.Windows.Forms.Button addStockButton;
        private System.Windows.Forms.GroupBox woodControlGroupbox;
        private System.Windows.Forms.GroupBox inventoryGroupbox;
        private System.Windows.Forms.Button excelButton;
        private System.Windows.Forms.Button excelAddButton;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox searchTextbox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.NumericUpDown numericUpDown1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
    }
}
