﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using RestSharp;
using Woodworks.Network;
using Woodworks.Models;
using Newtonsoft.Json;
using Woodworks.Models.DGVModels;
using Equin.ApplicationFramework;

namespace Woodworks.UserControls
{
    public partial class WoodUserControl : UserControl
    {
        private RestClient client;
        private List<Wood> woods;
        BindingListView<WoodTable> woodlists;

        public WoodUserControl()
        {
            InitializeComponent();
            refreshWoods();
            setPermissions();
        }

        private void refreshWoods()
        {
            client = WoodworksClient.Instance;

            var request = new RestRequest("woods", Method.GET);
            //request.AddParameter("application/json", credentials, ParameterType.RequestBody);

            IRestResponse response = client.Execute(request);
            String content = response.Content;

            woods = JsonConvert.DeserializeObject<List<Wood>>(content);
            woodlists = new BindingListView<WoodTable>(WoodTable.convertToTableReady(woods));
            woodDGV.DataSource = woodlists;

        }

        private void addWoodButton_Click(object sender, EventArgs e)
        {
            this.Enabled = false;
            WoodView window = new WoodView("Add", null);
            window.FormClosed += (s, args) => { this.Enabled = true; refreshWoods(); };
            window.Show();
        }

        private void deleteWoodButton_Click(object sender, EventArgs e)
        {
            if (woodDGV.CurrentCell != null)
            {
                WoodTable selected = woodlists.ElementAt(woodDGV.CurrentCell.RowIndex);
                Wood wood = woods.Find(x => x.id == selected.id);
                this.Enabled = false;

                DialogResult result = MessageBox.Show("Are you sure you want to delete the selected wood?", "Delete confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                if (result == DialogResult.Yes)
                {
                    var request = new RestRequest("woods/" + wood.id, Method.DELETE);

                    IRestResponse response = client.Execute(request);
                    if (response.StatusCode.ToString().Equals("OK"))
                    {
                        MessageBox.Show("Delete successful", "Success", MessageBoxButtons.OK, MessageBoxIcon.None);
                        refreshWoods();
                    }
                    else
                    {
                        MessageBox.Show("Something went wrong. Please try again.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }  
                }

                this.Enabled = true;
            }
            else
            {
                MessageBox.Show("No wood selected.", "Edit error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void editWoodButton_Click(object sender, EventArgs e)
        {
            if (woodDGV.CurrentCell != null)
            {
                WoodTable selected = woodlists.ElementAt(woodDGV.CurrentCell.RowIndex);
                Wood wood = woods.Find(x => x.id == selected.id);
                this.Enabled = false;
                WoodView window = new WoodView("Edit", wood);
                window.FormClosed += (s, args) => { this.Enabled = true; refreshWoods(); };
                window.Show();
            }
            else
            {
                MessageBox.Show("No wood selected.", "Edit error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void bulkAddWoodButton_Click(object sender, EventArgs e)
        {

        }

        private void addStockButton_Click(object sender, EventArgs e)
        {
            if (woodDGV.CurrentCell != null)
            {
                Wood wood = woods.ElementAt(woodDGV.CurrentCell.RowIndex);
                this.Enabled = false;
                StockView window = new StockView(wood);
                window.FormClosed += (s, args) => { this.Enabled = true; refreshWoods(); };
                window.Show();
            }
            else
            {
                MessageBox.Show("No wood selected.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void excelButton_Click(object sender, EventArgs e)
        {
            OpenFileDialog a = new OpenFileDialog();
            DialogResult b = a.ShowDialog();
            if (b == DialogResult.OK)
            {
                var request = new RestRequest("woods/stocks/excel", Method.POST);
                request.AddFile("filename", a.FileName);
                IRestResponse response = client.Execute(request);
                string content = response.Content;
                MessageBox.Show(content, "Message", MessageBoxButtons.OK, MessageBoxIcon.None);
                refreshWoods();
            }
        }

        private void excelAddButton_Click(object sender, EventArgs e)
        {
            OpenFileDialog a = new OpenFileDialog();
            DialogResult b = a.ShowDialog();
            if (b == DialogResult.OK)
            {
                var request = new RestRequest("woods/excel", Method.POST);
                request.AddFile("filename", a.FileName);
                IRestResponse response = client.Execute(request);
                string content = response.Content;
                MessageBox.Show(content, "Message", MessageBoxButtons.OK, MessageBoxIcon.None);
                refreshWoods();
            }
        }

        private void setPermissions()
        {
            if (!Services.Session.loggedInUser.user_permissions.Contains("create_item"))
            {
                addWoodButton.Enabled = false;
                excelAddButton.Enabled = false;
            }
            if (!Services.Session.loggedInUser.user_permissions.Contains("edit_items"))
            {
                editWoodButton.Enabled = false;
            }
            if (!Services.Session.loggedInUser.user_permissions.Contains("delete_items"))
            {
                deleteWoodButton.Enabled = false;
            }
            if (!Services.Session.loggedInUser.user_permissions.Contains("manage_stocks"))
            {
                inventoryGroupbox.Enabled = false;
            }
        }

        private void searchTextbox_TextChanged(object sender, EventArgs e)
        {
            string searchString = ((TextBox)sender).Text;
            woodlists.ApplyFilter(
                delegate (WoodTable wood)
                {
                    string wholeName = (wood.wood_type + " " + wood.wood_thickness + " " + wood.wood_width + " " + wood.wood_height).Replace(" ", "");
                    return wholeName.ToLower().Contains(searchString.ToLower());
                }
            );
        }

        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {
            woodlists.ApplyFilter(
                delegate (WoodTable wood)
                {
                    if (numericUpDown1.Value <= 0)
                    {
                        return true;
                    }
                    return wood.wood_stock <= numericUpDown1.Value;
                }
            );
        }

        private void button2_Click(object sender, EventArgs e)
        {
            refreshWoods();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            woodlists.RemoveFilter();
        }
    }
}
