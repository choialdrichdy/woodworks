﻿namespace Woodworks.UserControls
{
    partial class UserServicesUserControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.chngpwBtn = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // chngpwBtn
            // 
            this.chngpwBtn.Location = new System.Drawing.Point(23, 23);
            this.chngpwBtn.Name = "chngpwBtn";
            this.chngpwBtn.Size = new System.Drawing.Size(178, 23);
            this.chngpwBtn.TabIndex = 0;
            this.chngpwBtn.Text = "Change password";
            this.chngpwBtn.UseVisualStyleBackColor = true;
            this.chngpwBtn.Click += new System.EventHandler(this.chngpwBtn_Click);
            // 
            // UserServicesUserControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.chngpwBtn);
            this.Name = "UserServicesUserControl";
            this.Size = new System.Drawing.Size(349, 150);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button chngpwBtn;
    }
}
