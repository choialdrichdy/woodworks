﻿namespace Woodworks.UserControls
{
    partial class TransactionUserControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.customerGroupbox = new System.Windows.Forms.GroupBox();
            this.currentBalanceLabel = new System.Windows.Forms.Label();
            this.customerLabel = new System.Windows.Forms.Label();
            this.customerCombobox = new System.Windows.Forms.ComboBox();
            this.currentBalanceTextbox = new System.Windows.Forms.TextBox();
            this.orderDGV = new System.Windows.Forms.DataGridView();
            this.woodGroupbox = new System.Windows.Forms.GroupBox();
            this.typeTextbox = new System.Windows.Forms.TextBox();
            this.typeLabel = new System.Windows.Forms.Label();
            this.deleteButton = new System.Windows.Forms.Button();
            this.quantityLabel = new System.Windows.Forms.Label();
            this.confirmButton = new System.Windows.Forms.Button();
            this.priceLabel = new System.Windows.Forms.Label();
            this.quantityNumeric = new System.Windows.Forms.NumericUpDown();
            this.priceTextbox = new System.Windows.Forms.TextBox();
            this.heightLabel = new System.Windows.Forms.Label();
            this.widthLabel = new System.Windows.Forms.Label();
            this.lengthLabel = new System.Windows.Forms.Label();
            this.heightTextbox = new System.Windows.Forms.TextBox();
            this.widthTextbox = new System.Windows.Forms.TextBox();
            this.lengthTextbox = new System.Windows.Forms.TextBox();
            this.woodLabel = new System.Windows.Forms.Label();
            this.woodCombobox = new System.Windows.Forms.ComboBox();
            this.transactionGroupbox = new System.Windows.Forms.GroupBox();
            this.addTransactionButton = new System.Windows.Forms.Button();
            this.transactionStatusLabel = new System.Windows.Forms.Label();
            this.totalAmountLabel = new System.Windows.Forms.Label();
            this.transactionDateLabel = new System.Windows.Forms.Label();
            this.transactionStatusTextbox = new System.Windows.Forms.TextBox();
            this.totalAmountTextbox = new System.Windows.Forms.TextBox();
            this.transactionDateTextbox = new System.Windows.Forms.TextBox();
            this.termsLabel = new System.Windows.Forms.Label();
            this.termsTextbox = new System.Windows.Forms.TextBox();
            this.drLabel = new System.Windows.Forms.Label();
            this.drTextbox = new System.Windows.Forms.TextBox();
            this.addressTextbox = new System.Windows.Forms.TextBox();
            this.addressLabel = new System.Windows.Forms.Label();
            this.customerGroupbox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.orderDGV)).BeginInit();
            this.woodGroupbox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.quantityNumeric)).BeginInit();
            this.transactionGroupbox.SuspendLayout();
            this.SuspendLayout();
            // 
            // customerGroupbox
            // 
            this.customerGroupbox.Controls.Add(this.addressLabel);
            this.customerGroupbox.Controls.Add(this.addressTextbox);
            this.customerGroupbox.Controls.Add(this.termsTextbox);
            this.customerGroupbox.Controls.Add(this.termsLabel);
            this.customerGroupbox.Controls.Add(this.currentBalanceLabel);
            this.customerGroupbox.Controls.Add(this.customerLabel);
            this.customerGroupbox.Controls.Add(this.customerCombobox);
            this.customerGroupbox.Controls.Add(this.currentBalanceTextbox);
            this.customerGroupbox.Location = new System.Drawing.Point(4, 4);
            this.customerGroupbox.Name = "customerGroupbox";
            this.customerGroupbox.Size = new System.Drawing.Size(264, 177);
            this.customerGroupbox.TabIndex = 0;
            this.customerGroupbox.TabStop = false;
            this.customerGroupbox.Text = "Customer Information";
            // 
            // currentBalanceLabel
            // 
            this.currentBalanceLabel.AutoSize = true;
            this.currentBalanceLabel.Location = new System.Drawing.Point(7, 135);
            this.currentBalanceLabel.Name = "currentBalanceLabel";
            this.currentBalanceLabel.Size = new System.Drawing.Size(89, 13);
            this.currentBalanceLabel.TabIndex = 3;
            this.currentBalanceLabel.Text = "Current Balance :";
            // 
            // customerLabel
            // 
            this.customerLabel.AutoSize = true;
            this.customerLabel.Location = new System.Drawing.Point(4, 30);
            this.customerLabel.Name = "customerLabel";
            this.customerLabel.Size = new System.Drawing.Size(57, 13);
            this.customerLabel.TabIndex = 2;
            this.customerLabel.Text = "Customer :";
            // 
            // customerCombobox
            // 
            this.customerCombobox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.customerCombobox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.customerCombobox.FormattingEnabled = true;
            this.customerCombobox.Location = new System.Drawing.Point(6, 46);
            this.customerCombobox.Name = "customerCombobox";
            this.customerCombobox.Size = new System.Drawing.Size(252, 21);
            this.customerCombobox.TabIndex = 1;
            this.customerCombobox.SelectedIndexChanged += new System.EventHandler(this.customerCombobox_SelectedIndexChanged);
            // 
            // currentBalanceTextbox
            // 
            this.currentBalanceTextbox.Enabled = false;
            this.currentBalanceTextbox.Location = new System.Drawing.Point(6, 151);
            this.currentBalanceTextbox.Name = "currentBalanceTextbox";
            this.currentBalanceTextbox.Size = new System.Drawing.Size(119, 20);
            this.currentBalanceTextbox.TabIndex = 0;
            // 
            // orderDGV
            // 
            this.orderDGV.AllowUserToAddRows = false;
            this.orderDGV.AllowUserToDeleteRows = false;
            this.orderDGV.AllowUserToResizeRows = false;
            this.orderDGV.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.orderDGV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.orderDGV.Location = new System.Drawing.Point(274, 187);
            this.orderDGV.Name = "orderDGV";
            this.orderDGV.RowHeadersVisible = false;
            this.orderDGV.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.orderDGV.Size = new System.Drawing.Size(619, 414);
            this.orderDGV.TabIndex = 1;
            // 
            // woodGroupbox
            // 
            this.woodGroupbox.Controls.Add(this.typeTextbox);
            this.woodGroupbox.Controls.Add(this.typeLabel);
            this.woodGroupbox.Controls.Add(this.deleteButton);
            this.woodGroupbox.Controls.Add(this.quantityLabel);
            this.woodGroupbox.Controls.Add(this.confirmButton);
            this.woodGroupbox.Controls.Add(this.priceLabel);
            this.woodGroupbox.Controls.Add(this.quantityNumeric);
            this.woodGroupbox.Controls.Add(this.priceTextbox);
            this.woodGroupbox.Controls.Add(this.heightLabel);
            this.woodGroupbox.Controls.Add(this.widthLabel);
            this.woodGroupbox.Controls.Add(this.lengthLabel);
            this.woodGroupbox.Controls.Add(this.heightTextbox);
            this.woodGroupbox.Controls.Add(this.widthTextbox);
            this.woodGroupbox.Controls.Add(this.lengthTextbox);
            this.woodGroupbox.Controls.Add(this.woodLabel);
            this.woodGroupbox.Controls.Add(this.woodCombobox);
            this.woodGroupbox.Enabled = false;
            this.woodGroupbox.Location = new System.Drawing.Point(4, 187);
            this.woodGroupbox.Name = "woodGroupbox";
            this.woodGroupbox.Size = new System.Drawing.Size(264, 243);
            this.woodGroupbox.TabIndex = 2;
            this.woodGroupbox.TabStop = false;
            this.woodGroupbox.Text = "Wood Information";
            // 
            // typeTextbox
            // 
            this.typeTextbox.Enabled = false;
            this.typeTextbox.Location = new System.Drawing.Point(6, 87);
            this.typeTextbox.Name = "typeTextbox";
            this.typeTextbox.Size = new System.Drawing.Size(237, 20);
            this.typeTextbox.TabIndex = 13;
            // 
            // typeLabel
            // 
            this.typeLabel.AutoSize = true;
            this.typeLabel.Location = new System.Drawing.Point(7, 70);
            this.typeLabel.Name = "typeLabel";
            this.typeLabel.Size = new System.Drawing.Size(37, 13);
            this.typeLabel.TabIndex = 12;
            this.typeLabel.Text = "Type :";
            // 
            // deleteButton
            // 
            this.deleteButton.Location = new System.Drawing.Point(135, 214);
            this.deleteButton.Name = "deleteButton";
            this.deleteButton.Size = new System.Drawing.Size(123, 23);
            this.deleteButton.TabIndex = 5;
            this.deleteButton.Text = "Remove";
            this.deleteButton.UseVisualStyleBackColor = true;
            this.deleteButton.Click += new System.EventHandler(this.deleteButton_Click);
            // 
            // quantityLabel
            // 
            this.quantityLabel.AutoSize = true;
            this.quantityLabel.Location = new System.Drawing.Point(165, 163);
            this.quantityLabel.Name = "quantityLabel";
            this.quantityLabel.Size = new System.Drawing.Size(52, 13);
            this.quantityLabel.TabIndex = 11;
            this.quantityLabel.Text = "Quantity :";
            // 
            // confirmButton
            // 
            this.confirmButton.Location = new System.Drawing.Point(6, 214);
            this.confirmButton.Name = "confirmButton";
            this.confirmButton.Size = new System.Drawing.Size(119, 23);
            this.confirmButton.TabIndex = 4;
            this.confirmButton.Text = "Add";
            this.confirmButton.UseVisualStyleBackColor = true;
            this.confirmButton.Click += new System.EventHandler(this.confirmButton_Click);
            // 
            // priceLabel
            // 
            this.priceLabel.AutoSize = true;
            this.priceLabel.Location = new System.Drawing.Point(6, 163);
            this.priceLabel.Name = "priceLabel";
            this.priceLabel.Size = new System.Drawing.Size(37, 13);
            this.priceLabel.TabIndex = 10;
            this.priceLabel.Text = "Price :";
            // 
            // quantityNumeric
            // 
            this.quantityNumeric.Location = new System.Drawing.Point(168, 179);
            this.quantityNumeric.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.quantityNumeric.Name = "quantityNumeric";
            this.quantityNumeric.Size = new System.Drawing.Size(75, 20);
            this.quantityNumeric.TabIndex = 9;
            // 
            // priceTextbox
            // 
            this.priceTextbox.Enabled = false;
            this.priceTextbox.Location = new System.Drawing.Point(6, 179);
            this.priceTextbox.Name = "priceTextbox";
            this.priceTextbox.Size = new System.Drawing.Size(75, 20);
            this.priceTextbox.TabIndex = 8;
            // 
            // heightLabel
            // 
            this.heightLabel.AutoSize = true;
            this.heightLabel.Location = new System.Drawing.Point(165, 112);
            this.heightLabel.Name = "heightLabel";
            this.heightLabel.Size = new System.Drawing.Size(44, 13);
            this.heightLabel.TabIndex = 7;
            this.heightLabel.Text = "Height :";
            // 
            // widthLabel
            // 
            this.widthLabel.AutoSize = true;
            this.widthLabel.Location = new System.Drawing.Point(84, 112);
            this.widthLabel.Name = "widthLabel";
            this.widthLabel.Size = new System.Drawing.Size(41, 13);
            this.widthLabel.TabIndex = 6;
            this.widthLabel.Text = "Width :";
            // 
            // lengthLabel
            // 
            this.lengthLabel.AutoSize = true;
            this.lengthLabel.Location = new System.Drawing.Point(7, 112);
            this.lengthLabel.Name = "lengthLabel";
            this.lengthLabel.Size = new System.Drawing.Size(46, 13);
            this.lengthLabel.TabIndex = 5;
            this.lengthLabel.Text = "Length :";
            // 
            // heightTextbox
            // 
            this.heightTextbox.Enabled = false;
            this.heightTextbox.Location = new System.Drawing.Point(168, 128);
            this.heightTextbox.Name = "heightTextbox";
            this.heightTextbox.Size = new System.Drawing.Size(75, 20);
            this.heightTextbox.TabIndex = 4;
            // 
            // widthTextbox
            // 
            this.widthTextbox.Enabled = false;
            this.widthTextbox.Location = new System.Drawing.Point(87, 128);
            this.widthTextbox.Name = "widthTextbox";
            this.widthTextbox.Size = new System.Drawing.Size(75, 20);
            this.widthTextbox.TabIndex = 3;
            // 
            // lengthTextbox
            // 
            this.lengthTextbox.Enabled = false;
            this.lengthTextbox.Location = new System.Drawing.Point(6, 128);
            this.lengthTextbox.Name = "lengthTextbox";
            this.lengthTextbox.Size = new System.Drawing.Size(75, 20);
            this.lengthTextbox.TabIndex = 2;
            // 
            // woodLabel
            // 
            this.woodLabel.AutoSize = true;
            this.woodLabel.Location = new System.Drawing.Point(6, 26);
            this.woodLabel.Name = "woodLabel";
            this.woodLabel.Size = new System.Drawing.Size(42, 13);
            this.woodLabel.TabIndex = 1;
            this.woodLabel.Text = "Wood :";
            // 
            // woodCombobox
            // 
            this.woodCombobox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.woodCombobox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.woodCombobox.FormattingEnabled = true;
            this.woodCombobox.Location = new System.Drawing.Point(6, 42);
            this.woodCombobox.Name = "woodCombobox";
            this.woodCombobox.Size = new System.Drawing.Size(252, 21);
            this.woodCombobox.TabIndex = 0;
            this.woodCombobox.SelectedIndexChanged += new System.EventHandler(this.woodCombobox_SelectedIndexChanged);
            // 
            // transactionGroupbox
            // 
            this.transactionGroupbox.Controls.Add(this.drTextbox);
            this.transactionGroupbox.Controls.Add(this.drLabel);
            this.transactionGroupbox.Controls.Add(this.transactionStatusLabel);
            this.transactionGroupbox.Controls.Add(this.totalAmountLabel);
            this.transactionGroupbox.Controls.Add(this.transactionDateLabel);
            this.transactionGroupbox.Controls.Add(this.transactionStatusTextbox);
            this.transactionGroupbox.Controls.Add(this.totalAmountTextbox);
            this.transactionGroupbox.Controls.Add(this.transactionDateTextbox);
            this.transactionGroupbox.Location = new System.Drawing.Point(274, 4);
            this.transactionGroupbox.Name = "transactionGroupbox";
            this.transactionGroupbox.Size = new System.Drawing.Size(426, 129);
            this.transactionGroupbox.TabIndex = 3;
            this.transactionGroupbox.TabStop = false;
            this.transactionGroupbox.Text = "Transaction Information";
            // 
            // addTransactionButton
            // 
            this.addTransactionButton.Location = new System.Drawing.Point(731, 24);
            this.addTransactionButton.Name = "addTransactionButton";
            this.addTransactionButton.Size = new System.Drawing.Size(132, 23);
            this.addTransactionButton.TabIndex = 6;
            this.addTransactionButton.Text = "Complete Order";
            this.addTransactionButton.UseVisualStyleBackColor = true;
            this.addTransactionButton.Click += new System.EventHandler(this.addTransactionButton_Click);
            // 
            // transactionStatusLabel
            // 
            this.transactionStatusLabel.AutoSize = true;
            this.transactionStatusLabel.Location = new System.Drawing.Point(241, 70);
            this.transactionStatusLabel.Name = "transactionStatusLabel";
            this.transactionStatusLabel.Size = new System.Drawing.Size(43, 13);
            this.transactionStatusLabel.TabIndex = 5;
            this.transactionStatusLabel.Text = "Status :";
            // 
            // totalAmountLabel
            // 
            this.totalAmountLabel.AutoSize = true;
            this.totalAmountLabel.Location = new System.Drawing.Point(17, 70);
            this.totalAmountLabel.Name = "totalAmountLabel";
            this.totalAmountLabel.Size = new System.Drawing.Size(76, 13);
            this.totalAmountLabel.TabIndex = 4;
            this.totalAmountLabel.Text = "Total Amount :";
            // 
            // transactionDateLabel
            // 
            this.transactionDateLabel.AutoSize = true;
            this.transactionDateLabel.Location = new System.Drawing.Point(241, 30);
            this.transactionDateLabel.Name = "transactionDateLabel";
            this.transactionDateLabel.Size = new System.Drawing.Size(36, 13);
            this.transactionDateLabel.TabIndex = 3;
            this.transactionDateLabel.Text = "Date :";
            // 
            // transactionStatusTextbox
            // 
            this.transactionStatusTextbox.Enabled = false;
            this.transactionStatusTextbox.Location = new System.Drawing.Point(244, 86);
            this.transactionStatusTextbox.Name = "transactionStatusTextbox";
            this.transactionStatusTextbox.Size = new System.Drawing.Size(164, 20);
            this.transactionStatusTextbox.TabIndex = 2;
            // 
            // totalAmountTextbox
            // 
            this.totalAmountTextbox.Enabled = false;
            this.totalAmountTextbox.Location = new System.Drawing.Point(244, 46);
            this.totalAmountTextbox.Name = "totalAmountTextbox";
            this.totalAmountTextbox.Size = new System.Drawing.Size(162, 20);
            this.totalAmountTextbox.TabIndex = 1;
            // 
            // transactionDateTextbox
            // 
            this.transactionDateTextbox.Enabled = false;
            this.transactionDateTextbox.Location = new System.Drawing.Point(20, 86);
            this.transactionDateTextbox.Name = "transactionDateTextbox";
            this.transactionDateTextbox.Size = new System.Drawing.Size(163, 20);
            this.transactionDateTextbox.TabIndex = 0;
            // 
            // termsLabel
            // 
            this.termsLabel.AutoSize = true;
            this.termsLabel.Location = new System.Drawing.Point(126, 135);
            this.termsLabel.Name = "termsLabel";
            this.termsLabel.Size = new System.Drawing.Size(75, 13);
            this.termsLabel.TabIndex = 4;
            this.termsLabel.Text = "Terms (Days) :";
            // 
            // termsTextbox
            // 
            this.termsTextbox.Enabled = false;
            this.termsTextbox.Location = new System.Drawing.Point(129, 151);
            this.termsTextbox.Name = "termsTextbox";
            this.termsTextbox.Size = new System.Drawing.Size(129, 20);
            this.termsTextbox.TabIndex = 5;
            // 
            // drLabel
            // 
            this.drLabel.AutoSize = true;
            this.drLabel.Location = new System.Drawing.Point(17, 30);
            this.drLabel.Name = "drLabel";
            this.drLabel.Size = new System.Drawing.Size(108, 13);
            this.drLabel.TabIndex = 7;
            this.drLabel.Text = "Delivery Receipt No :";
            // 
            // drTextbox
            // 
            this.drTextbox.Location = new System.Drawing.Point(20, 46);
            this.drTextbox.Name = "drTextbox";
            this.drTextbox.Size = new System.Drawing.Size(163, 20);
            this.drTextbox.TabIndex = 8;
            // 
            // addressTextbox
            // 
            this.addressTextbox.Enabled = false;
            this.addressTextbox.Location = new System.Drawing.Point(7, 86);
            this.addressTextbox.Multiline = true;
            this.addressTextbox.Name = "addressTextbox";
            this.addressTextbox.Size = new System.Drawing.Size(251, 43);
            this.addressTextbox.TabIndex = 6;
            // 
            // addressLabel
            // 
            this.addressLabel.AutoSize = true;
            this.addressLabel.Location = new System.Drawing.Point(6, 70);
            this.addressLabel.Name = "addressLabel";
            this.addressLabel.Size = new System.Drawing.Size(51, 13);
            this.addressLabel.TabIndex = 7;
            this.addressLabel.Text = "Address :";
            // 
            // TransactionUserControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.transactionGroupbox);
            this.Controls.Add(this.woodGroupbox);
            this.Controls.Add(this.addTransactionButton);
            this.Controls.Add(this.orderDGV);
            this.Controls.Add(this.customerGroupbox);
            this.Name = "TransactionUserControl";
            this.Size = new System.Drawing.Size(896, 615);
            this.customerGroupbox.ResumeLayout(false);
            this.customerGroupbox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.orderDGV)).EndInit();
            this.woodGroupbox.ResumeLayout(false);
            this.woodGroupbox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.quantityNumeric)).EndInit();
            this.transactionGroupbox.ResumeLayout(false);
            this.transactionGroupbox.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox customerGroupbox;
        private System.Windows.Forms.DataGridView orderDGV;
        private System.Windows.Forms.GroupBox woodGroupbox;
        private System.Windows.Forms.GroupBox transactionGroupbox;
        private System.Windows.Forms.Label currentBalanceLabel;
        private System.Windows.Forms.Label customerLabel;
        private System.Windows.Forms.ComboBox customerCombobox;
        private System.Windows.Forms.TextBox currentBalanceTextbox;
        private System.Windows.Forms.Label quantityLabel;
        private System.Windows.Forms.Label priceLabel;
        private System.Windows.Forms.NumericUpDown quantityNumeric;
        private System.Windows.Forms.TextBox priceTextbox;
        private System.Windows.Forms.Label heightLabel;
        private System.Windows.Forms.Label widthLabel;
        private System.Windows.Forms.Label lengthLabel;
        private System.Windows.Forms.TextBox heightTextbox;
        private System.Windows.Forms.TextBox widthTextbox;
        private System.Windows.Forms.TextBox lengthTextbox;
        private System.Windows.Forms.Label woodLabel;
        private System.Windows.Forms.ComboBox woodCombobox;
        private System.Windows.Forms.Label transactionStatusLabel;
        private System.Windows.Forms.Label totalAmountLabel;
        private System.Windows.Forms.Label transactionDateLabel;
        private System.Windows.Forms.TextBox transactionStatusTextbox;
        private System.Windows.Forms.TextBox totalAmountTextbox;
        private System.Windows.Forms.TextBox transactionDateTextbox;
        private System.Windows.Forms.Button confirmButton;
        private System.Windows.Forms.Button deleteButton;
        private System.Windows.Forms.TextBox typeTextbox;
        private System.Windows.Forms.Label typeLabel;
        private System.Windows.Forms.Button addTransactionButton;
        private System.Windows.Forms.TextBox termsTextbox;
        private System.Windows.Forms.Label termsLabel;
        private System.Windows.Forms.Label addressLabel;
        private System.Windows.Forms.TextBox addressTextbox;
        private System.Windows.Forms.TextBox drTextbox;
        private System.Windows.Forms.Label drLabel;
    }
}
