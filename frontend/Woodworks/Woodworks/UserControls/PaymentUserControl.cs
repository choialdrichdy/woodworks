﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using RestSharp;
using Woodworks.Models;
using Woodworks.Network;
using Newtonsoft.Json;
using Equin.ApplicationFramework;
using Woodworks.Models.DGVModels;
using Woodworks.Forms;

namespace Woodworks.UserControls
{
    public partial class PaymentUserControl : UserControl
    {
        private RestClient client;
        List<PaymentTable> payments;
        //List<Customer> customers;
        //BindingListView<Customer> customerlists;
        BindingListView<PaymentTable> paymentlists;

        public PaymentUserControl()
        {
            InitializeComponent();
            refreshPayments();
        }

        private void refreshPayments()
        {
            client = WoodworksClient.Instance;

            var request = new RestRequest("payments", Method.GET);
            //request.AddParameter("application/json", credentials, ParameterType.RequestBody);

            IRestResponse response = client.Execute(request);
            String content = response.Content;

            payments = JsonConvert.DeserializeObject<List<PaymentTable>>(content);

            paymentlists = new BindingListView<PaymentTable>(payments);
            paymentDGV.DataSource = paymentlists;

        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (paymentDGV.CurrentCell != null)
            {
                PaymentTable payment = paymentlists.ElementAt(paymentDGV.CurrentCell.RowIndex);
                this.Enabled = false;
                EditPaymentView window = new EditPaymentView(payment);
                window.FormClosed += (s, args) => { this.Enabled = true; refreshPayments(); };
                window.Show();
            }
            else
            {
                MessageBox.Show("No payment selected.", "Edit error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void searchTextbox_TextChanged(object sender, EventArgs e)
        {
            string searchString = ((TextBox)sender).Text;
            paymentlists.ApplyFilter(
                delegate (PaymentTable payment)
                {
                    return payment.customer_name.ToLower().Contains(searchString.ToLower());
                }
            );
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Services.Services.exportPayments(paymentlists.ToList());
        }

        private void addPaymentBtn_Click(object sender, EventArgs e)
        {
            PaymentView window = new PaymentView();
            window.FormClosed += (s, args) => { this.Enabled = true; refreshPayments(); };
            window.Show();
        }

        private void searchBtn_Click(object sender, EventArgs e)
        {
            paymentlists.ApplyFilter(
                delegate (PaymentTable payment)
                {
                    return payment.payment_date >= dateTimePicker1.Value.Date && payment.payment_date <= dateTimePicker2.Value.Date;
                }
            );
        }

        private void clrBtn_Click(object sender, EventArgs e)
        {
            paymentlists.RemoveFilter();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            refreshPayments();
        }
    }
}
