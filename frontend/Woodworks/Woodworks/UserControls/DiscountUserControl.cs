﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using RestSharp;
using Newtonsoft.Json;
using Woodworks.Models;
using Woodworks.Network;
using Equin.ApplicationFramework;
using Woodworks.Forms;
using Woodworks.Models.DGVModels;

namespace Woodworks.UserControls
{
    public partial class DiscountUserControl : UserControl
    {
        private RestClient client;
        private List<DiscountProfile> profiles;
        private List<DiscountDetails> details;
        BindingListView<DiscountDetailsTable> detailslist;

        public DiscountUserControl()
        {
            InitializeComponent();
            setPermissions();
            client = WoodworksClient.Instance;

            var request = new RestRequest("discounts", Method.GET);
            //request.AddParameter("application/json", credentials, ParameterType.RequestBody);

            IRestResponse response = client.Execute(request);
            String content = response.Content;

            profiles = JsonConvert.DeserializeObject<List<DiscountProfile>>(content);
            rulesDataGridView.DataSource = profiles;

        }

        private void addRuleButton_Click(object sender, EventArgs e)
        {
            RulesView window = new RulesView("rules");
            window.FormClosed += (s, args) => { this.Enabled = true;};
            window.Show();
        }

        private void rulesDataGridView_selectionchanged(object sender, EventArgs e)
        {
            DiscountProfile profile = profiles.ElementAt(rulesDataGridView.CurrentCell.RowIndex);

            details = profile.details;

            detailslist = new BindingListView<DiscountDetailsTable>(DiscountDetailsTable.convertToTableReady(details));
            discountsDataGridView.DataSource = detailslist;
        }

        private void setCustomerBtn_Click(object sender, EventArgs e)
        {
            if (rulesDataGridView.CurrentCell != null)
            {
                DiscountProfile profile = profiles.ElementAt(rulesDataGridView.CurrentCell.RowIndex);
                CustomerDiscountView window = new CustomerDiscountView(profile);
                window.FormClosed += (s, args) => { this.Enabled = true; };
                window.Show();
            }
            else
            {
                MessageBox.Show("No profile selected.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void addSpecialPricesButton_Click(object sender, EventArgs e)
        {
            RulesView window = new RulesView("special");
            window.FormClosed += (s, args) => { this.Enabled = true; };
            window.Show();
        }

        private void setPermissions()
        {
            if (!Services.Session.loggedInUser.user_permissions.Contains("manage_discount_rules"))
            {
                addRuleButton.Enabled = false;
                
            }
            if (!Services.Session.loggedInUser.user_permissions.Contains("manage_special_price"))
            {
                addSpecialPricesButton.Enabled = false;
            }
            if (!Services.Session.loggedInUser.user_permissions.Contains("manage_customer_discounts"))
            {
                setCustomerBtn.Enabled = false;
            }
        }
    }
}
