﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using RestSharp;
using Woodworks.Models;
using Woodworks.Network;
using Newtonsoft.Json;
using Woodworks.Forms;
using NPOI.XSSF.UserModel;
using System.IO;
using NPOI.SS.UserModel;
using Woodworks.Models.DGVModels;
using Equin.ApplicationFramework;

namespace Woodworks.UserControls
{
    public partial class TransactionViewUserControl : UserControl
    {
        private RestClient client;
        List<Transaction> transactions;
        List<TransactionTable> transactionstable;
        BindingListView<TransactionTable> transactionlists;
        public TransactionViewUserControl()
        {
            InitializeComponent();
            refreshTransactions();
            setPermissions();
        }

        public void refreshTransactions()
        {
            client = WoodworksClient.Instance;

            var request = new RestRequest("transactions", Method.GET);

            IRestResponse response = client.Execute(request);
            String content = response.Content;

            transactions = JsonConvert.DeserializeObject<List<Transaction>>(content);
            transactionstable = TransactionTable.convertToTableReady(transactions);
            transactionlists = new BindingListView<TransactionTable>(transactionstable);
            transactionDGV.DataSource = transactionlists;
        }

        private void editButton_Click(object sender, EventArgs e)
        {
            TransactionTable selected = transactionlists.ElementAt(transactionDGV.CurrentCell.RowIndex);
            Transaction transaction = transactions.Find(x => x.id == selected.id);
            this.Enabled = false;
            TransactionView window = new TransactionView(transaction);
            window.FormClosed += (s, args) => { this.Enabled = true; refreshTransactions(); };
            window.Show();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            PaymentView window = new PaymentView();
            window.FormClosed += (s, args) => { this.Enabled = true; refreshTransactions(); };
            window.Show();
        }

        private void addButton_Click(object sender, EventArgs e)
        {
            TransactionView window = new TransactionView();
            window.FormClosed += (s, args) => { this.Enabled = true; refreshTransactions(); };
            window.Show();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (transactionDGV.CurrentCell != null)
            {
                Transaction transaction = transactions.ElementAt(transactionDGV.CurrentCell.RowIndex);
                this.Enabled = false;

                DialogResult result = MessageBox.Show("Are you sure you want to export the selected transaction?", "Export confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                if (result == DialogResult.Yes)
                {
                    var request = new RestRequest("transactions/" + transaction.id + "/export", Method.GET);

                    IRestResponse response = client.Execute(request);
                    var content = response.Content;
                    try
                    {
                        TransactionExport onetransaction = JsonConvert.DeserializeObject<TransactionExport>(content);
                        Services.Services.exportTransaction(onetransaction);
                    }
                    catch (JsonSerializationException ex)
                    {
                        MessageBox.Show("Something went wrong with parsing transaction data. Please try again.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    /*if (response.StatusCode.ToString().Equals("OK"))
                    {
                        TransactionExport onetransaction = JsonConvert.DeserializeObject<TransactionExport>(content);
                        SaveFileDialog a = new SaveFileDialog();
                        a.Filter = "Excel File|*.xlsx";
                        a.Title = "Save an Excel File";
                        DialogResult b = a.ShowDialog();
                        XSSFWorkbook workbook = new XSSFWorkbook();
                        ICellStyle centerStyle = workbook.CreateCellStyle();
                        centerStyle.Alignment = NPOI.SS.UserModel.HorizontalAlignment.Center;
                        XSSFSheet sheet = (XSSFSheet)workbook.CreateSheet("Sheet1");
                        sheet.AddMergedRegion(new NPOI.SS.Util.CellRangeAddress(5, 5, 2, 4));
                        sheet.AddMergedRegion(new NPOI.SS.Util.CellRangeAddress(0, 1, 1, 4));
                        sheet.AddMergedRegion(new NPOI.SS.Util.CellRangeAddress(2, 2, 1, 4));
                        sheet.AddMergedRegion(new NPOI.SS.Util.CellRangeAddress(3, 3, 1, 4));
                        sheet.AddMergedRegion(new NPOI.SS.Util.CellRangeAddress(4, 4, 1, 4));
                        IRow row1 = sheet.CreateRow(0);
                        row1.RowStyle = centerStyle;
                        row1.CreateCell(1).SetCellValue("B88");
                        sheet.CreateRow(1);
                        IRow infoRow = sheet.CreateRow(3);
                        infoRow.RowStyle = centerStyle;
                        infoRow.CreateCell(0).SetCellValue("Customer:");
                        infoRow.CreateCell(1).SetCellValue(onetransaction.customer);
                        infoRow.CreateCell(5).SetCellValue("DR No.");
                        infoRow.CreateCell(6).SetCellValue(onetransaction.dr_no);
                        IRow infoRow2 = sheet.CreateRow(4);
                        infoRow2.RowStyle = centerStyle;
                        infoRow2.CreateCell(0).SetCellValue("Address:");
                        infoRow2.CreateCell(1).SetCellValue(onetransaction.address);
                        infoRow2.CreateCell(5).SetCellValue("Date");
                        infoRow2.CreateCell(6).SetCellValue(onetransaction.date);
                        IRow headers = sheet.CreateRow(5);
                        ICellStyle headerStyle = workbook.CreateCellStyle();
                        headerStyle.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thick;
                        headerStyle.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thick;
                        headerStyle.BorderRight = NPOI.SS.UserModel.BorderStyle.Thick;
                        headerStyle.BorderTop = NPOI.SS.UserModel.BorderStyle.Thick;
                        headerStyle.Alignment = NPOI.SS.UserModel.HorizontalAlignment.Center;
                        //headers.RowStyle = headerStyle;
                        ICell header1 = headers.CreateCell(0);
                        header1.SetCellValue("QTY");
                        ICell header2 = headers.CreateCell(1);
                        header2.SetCellValue("Description");
                        ICell header3 = headers.CreateCell(2);
                        header3.SetCellValue("Dimension");
                        ICell header4 = headers.CreateCell(5);
                        header4.SetCellValue("BF/LF");
                        ICell header5 = headers.CreateCell(6);
                        header5.SetCellValue("Price");
                        ICell header6 = headers.CreateCell(7);
                        header6.SetCellValue("Less");
                        ICell header7 = headers.CreateCell(8);
                        header7.SetCellValue("TOTAL AMOUNT");
                        header1.CellStyle = headerStyle;
                        header2.CellStyle = headerStyle;
                        header3.CellStyle = headerStyle;
                        header4.CellStyle = headerStyle;
                        header5.CellStyle = headerStyle;
                        header6.CellStyle = headerStyle;
                        header7.CellStyle = headerStyle;
                        int rownumber = 6;
                        foreach (DetailExport onedetail in onetransaction.details)
                        {
                            IRow detail = sheet.CreateRow(rownumber);
                            detail.RowStyle = centerStyle;
                            detail.CreateCell(0).SetCellValue(onedetail.qty);
                            detail.CreateCell(1).SetCellValue(onedetail.description);
                            detail.CreateCell(2).SetCellValue(onedetail.thickness);
                            detail.CreateCell(3).SetCellValue(onedetail.width);
                            detail.CreateCell(4).SetCellValue(onedetail.height);
                            detail.CreateCell(5).SetCellValue(onedetail.bf_lf);
                            detail.CreateCell(6).SetCellValue(onedetail.price);
                            detail.CreateCell(7).SetCellValue(onedetail.less);
                            detail.CreateCell(8).SetCellValue(onedetail.total_amount);
                            rownumber++;
                        }
                        IRow totalrow = sheet.CreateRow(rownumber);
                        totalrow.RowStyle = centerStyle;
                        totalrow.CreateCell(8).SetCellValue("PHP " + onetransaction.total);
                        FileStream fs = new FileStream(a.FileName, FileMode.Create, FileAccess.Write);
                        workbook.Write(fs);
                    }
                    else
                    {
                        MessageBox.Show("Something went wrong. Please try again.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }*/
                }

                this.Enabled = true;
            }
            else
            {
                MessageBox.Show("No customer selected.", "Edit error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void setPermissions()
        {
            if (!Services.Session.loggedInUser.user_permissions.Contains("create_transactions"))
            {
                addButton.Enabled = false;
            }
            if (!Services.Session.loggedInUser.user_permissions.Contains("edit_transactions"))
            {
                editButton.Enabled = false;
            }
            if (!Services.Session.loggedInUser.user_permissions.Contains("export_transaction"))
            {
                button2.Enabled = false;
            }
            if (!Services.Session.loggedInUser.user_permissions.Contains("manage_payments"))
            {
                button1.Enabled = false;
            }
        }

        private void searchTextbox_TextChanged(object sender, EventArgs e)
        {
            string textBox = ((TextBox)sender).Name;
            transactionlists.ApplyFilter(
                delegate (TransactionTable transaction)
                {
                    if (textBox.Equals(drnoTextbox.Name))
                    {
                        return transaction.delivery_receipt_no.ToLower().Contains(drnoTextbox.Text.ToLower());
                    }
                    else
                    {
                        return transaction.customer_name.ToLower().Contains(customerNameTextbox.Text.ToLower());
                    }
                }
            );
        }

        private void button4_Click(object sender, EventArgs e)
        {
            refreshTransactions();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            transactionlists.RemoveFilter();
        }
    }
}
