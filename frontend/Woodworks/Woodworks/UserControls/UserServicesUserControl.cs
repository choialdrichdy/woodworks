﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Woodworks.Models;
using Woodworks.Forms;

namespace Woodworks.UserControls
{
    public partial class UserServicesUserControl : UserControl
    {
        public UserServicesUserControl()
        {
            InitializeComponent();
        }

        private void chngpwBtn_Click(object sender, EventArgs e)
        {
            ChangePasswordView window = new ChangePasswordView();
            window.Show();
        }
    }
}
