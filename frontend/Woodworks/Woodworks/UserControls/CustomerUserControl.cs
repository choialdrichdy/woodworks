﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using RestSharp;
using Woodworks.Forms;
using Woodworks.Network;
using Woodworks.Models;
using Newtonsoft.Json;
using Equin.ApplicationFramework;

namespace Woodworks.UserControls
{
    public partial class CustomerUserControl : UserControl
    {
        private RestClient client;
        List<Customer> customers;
        BindingListView<Customer> customerlists;

        public CustomerUserControl()
        {
            InitializeComponent();
            refreshCustomers();
            setPermissions();
        }

        private void refreshCustomers()
        {
            client = WoodworksClient.Instance;

            var request = new RestRequest("customers", Method.GET);
            //request.AddParameter("application/json", credentials, ParameterType.RequestBody);

            IRestResponse response = client.Execute(request);
            String content = response.Content;

            customers = JsonConvert.DeserializeObject<List<Customer>>(content);
            customerlists = new BindingListView<Customer>(customers);
            customerDGV.DataSource = customerlists;

        }

        private void addCustomerButton_Click(object sender, EventArgs e)
        {
            this.Enabled = false;
            CustomerView window = new CustomerView("Add", null);
            window.FormClosed += (s, args) => { this.Enabled = true; refreshCustomers(); };
            window.Show();
        }

        private void editCustomerButton_Click(object sender, EventArgs e)
        {
            if (customerDGV.CurrentCell != null)
            {
                Customer customer = customerlists.ElementAt(customerDGV.CurrentCell.RowIndex);
                this.Enabled = false;
                CustomerView window = new CustomerView("Edit", customer);
                window.FormClosed += (s, args) => { this.Enabled = true; refreshCustomers(); };
                window.Show();
            }
            else
            {
                MessageBox.Show("No customer selected.", "Edit error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void deleteCustomerButton_Click(object sender, EventArgs e)
        {
            if (customerDGV.CurrentCell != null)
            {
                Customer customer = customerlists.ElementAt(customerDGV.CurrentCell.RowIndex);
                this.Enabled = false;

                DialogResult result = MessageBox.Show("Are you sure you want to delete the selected customer?", "Delete confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                if (result == DialogResult.Yes)
                {
                    var request = new RestRequest("customers/" + customer.id, Method.DELETE);

                    IRestResponse response = client.Execute(request);
                    if (response.StatusCode.ToString().Equals("OK"))
                    {
                        MessageBox.Show("Customer deleted", "Success", MessageBoxButtons.OK, MessageBoxIcon.None);
                        refreshCustomers();
                    }
                    else
                    {
                        MessageBox.Show("Something went wrong. Please try again.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }

                this.Enabled = true;
            }
            else
            {
                MessageBox.Show("No customer selected.", "Edit error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void excelAddButton_Click(object sender, EventArgs e)
        {
            OpenFileDialog a = new OpenFileDialog();
            DialogResult b = a.ShowDialog();
            if (b == DialogResult.OK)
            {
                var request = new RestRequest("customers/excel", Method.POST);
                request.AddFile("filename", a.FileName);
                IRestResponse response = client.Execute(request);
                string content = response.Content;
                MessageBox.Show(content, "Message", MessageBoxButtons.OK, MessageBoxIcon.None);
                refreshCustomers();
            }
        }

        private void setPermissions()
        {
            if (!Services.Session.loggedInUser.user_permissions.Contains("create_customers"))
            {
                addCustomerButton.Enabled = false;
                excelAddButton.Enabled = false;
            }
            if (!Services.Session.loggedInUser.user_permissions.Contains("edit_customers"))
            {
                editCustomerButton.Enabled = false;
            }
            if (!Services.Session.loggedInUser.user_permissions.Contains("delete_customers"))
            {
                deleteCustomerButton.Enabled = false;
            }
        }

        private void searchTextbox_TextChanged(object sender, EventArgs e)
        {
            string searchString = ((TextBox)sender).Text;
            customerlists.ApplyFilter(
                delegate (Customer customer)
                {
                    return customer.customer_name.ToLower().Contains(searchString.ToLower());
                }
            );
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Services.Services.exportCustomers(customerlists.ToList());
        }

        private void button2_Click(object sender, EventArgs e)
        {
            refreshCustomers();
        }

        private void clrBtn_Click(object sender, EventArgs e)
        {
            customerlists.RemoveFilter();
        }
    }
}
