﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using RestSharp;
using Woodworks.Models;
using Woodworks.Network;
using Newtonsoft.Json;
using Woodworks.Forms;

namespace Woodworks.UserControls
{
    public partial class UserUserControl : UserControl
    {
        private RestClient client;
        List<User> users;

        public UserUserControl()
        {
            InitializeComponent();
            refreshUsers();
        }

        private void refreshUsers()
        {
            client = WoodworksClient.Instance;

            var request = new RestRequest("users", Method.GET);
            //request.AddParameter("application/json", credentials, ParameterType.RequestBody);

            IRestResponse response = client.Execute(request);
            String content = response.Content;

            users = JsonConvert.DeserializeObject<List<User>>(content);
            userDGV.DataSource = users;

        }

        private void addBtn_Click(object sender, EventArgs e)
        {
            this.Enabled = false;
            UserView window = new UserView();
            window.FormClosed += (s, args) => { this.Enabled = true; refreshUsers(); };
            window.Show();
        }

        private void editBtn_Click(object sender, EventArgs e)
        {
            if (userDGV.CurrentCell != null)
            {
                User user = users.ElementAt(userDGV.CurrentCell.RowIndex);
                this.Enabled = false;
                UserView window = new UserView("Edit", user);
                window.FormClosed += (s, args) => { this.Enabled = true; refreshUsers(); };
                window.Show();
            }
            else
            {
                MessageBox.Show("No wood selected.", "Edit error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void deleteBtn_Click(object sender, EventArgs e)
        {
            if (userDGV.CurrentCell != null)
            {
                User user = users.ElementAt(userDGV.CurrentCell.RowIndex);
                this.Enabled = false;

                DialogResult result = MessageBox.Show("Are you sure you want to delete the selected user?", "Delete confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                if (result == DialogResult.Yes)
                {
                    var request = new RestRequest("user/" + user.id, Method.DELETE);

                    IRestResponse response = client.Execute(request);
                    if (response.StatusCode.ToString().Equals("OK"))
                    {
                        MessageBox.Show("Delete successful", "Success", MessageBoxButtons.OK, MessageBoxIcon.None);
                        refreshUsers();
                    }
                    else
                    {
                        MessageBox.Show("Something went wrong. Please try again.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }

                this.Enabled = true;
            }
        }

        private void manageBtn_Click(object sender, EventArgs e)
        {
            if (userDGV.CurrentCell != null)
            {
                User user = users.ElementAt(userDGV.CurrentCell.RowIndex);
                this.Enabled = false;
                UserPermissionView window = new UserPermissionView(user);
                window.FormClosed += (s, args) => { this.Enabled = true; refreshUsers(); };
                window.Show();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Enabled = false;
            UserView window = new UserView();
            window.FormClosed += (s, args) => { this.Enabled = true; refreshUsers(); };
            window.Show();
        }
    }
}
