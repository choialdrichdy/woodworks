﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Woodworks.Models
{
    public class DiscountProfile
    {
        public int id { get; set; }
        public String profile_name { get; set; }
        public List<DiscountDetails> details { get; set; }
    }
}
