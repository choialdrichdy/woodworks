﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Woodworks.Models
{
    public class WoodDiscount
    {
        public int id { get; set; }
        public String wood_type { get; set; }
        public String wood_height { get; set; }
        public String wood_width { get; set; }
        public String wood_thickness { get; set; }
        public float discount { get; set; }
    }
}
