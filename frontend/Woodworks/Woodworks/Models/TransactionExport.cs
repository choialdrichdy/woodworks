﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Woodworks.Models
{
    public class TransactionExport
    {
        public String date;
        public String customer;
        public String address;
        public float total;
        public String dr_no;
        public List<DetailExport> details;
    }

    public class DetailExport
    {
        public float thickness;
        public float height;
        public float width;
        public String description;
        public float price;
        public int bf_lf;
        public float total_amount;
        public int qty;
        public float less;
    }
}
