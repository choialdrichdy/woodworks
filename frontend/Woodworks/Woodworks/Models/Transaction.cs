﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Woodworks.Models.DGVModels;

namespace Woodworks.Models
{
    public class Transaction
    {
        public int id { get; set; }
        public string delivery_receipt_no { get; set; }
        public int customer_id { get; set; }
        public String transaction_status { get; set; }
        public float total_amount { get; set; }
        public float amount_payed { get; set; }
        public List<Order> details { get; set; }
        public Customer customer { get; set; }

        public String transaction_name
        {
            get
            {
                return delivery_receipt_no + " (" + getBalance() + ")";
            }
        }

        public float getBalance()
        {
            return total_amount - amount_payed;
        }
    }
}
