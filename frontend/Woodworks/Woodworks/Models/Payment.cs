﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Woodworks.Models
{
    class Payment
    {
        public int id;
        public int customer_id;
        public int user_id;
        public float amount_paid;
        public String payment_type;
        public String payment_status;
        public String check_number;
        public DateTime check_date;
        public bool is_cleared;
        public String remarks;
        public String check_bank;
        public String check_branch;
        public List<int> transaction_ids;
    }
}
