﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Woodworks.Models
{
    class Discount
    {
        public int wood_id { get; set; }
        public float discount_percent { get; set; }
    }
}
