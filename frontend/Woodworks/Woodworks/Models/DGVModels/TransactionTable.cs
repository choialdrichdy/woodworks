﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Woodworks.Models.DGVModels
{
    public class TransactionTable
    {
        [System.ComponentModel.DisplayName("ID")]
        public int id { get; set; }
        [System.ComponentModel.DisplayName("DR No.")]
        public string delivery_receipt_no { get; set; }
        [System.ComponentModel.DisplayName("Customer Name")]
        public string customer_name { get; set; }
        [System.ComponentModel.DisplayName("Total amount")]
        public float total_amount { get; set; }
        [System.ComponentModel.DisplayName("Amount Paid")]
        public float amount_payed { get; set; }

        public static List<TransactionTable> convertToTableReady(List<Transaction> transactions)
        {
            List<TransactionTable> transtable = new List<TransactionTable>();
            foreach(Transaction transaction in transactions)
            {
                TransactionTable onetranstable = new TransactionTable();
                onetranstable.id = transaction.id;
                onetranstable.delivery_receipt_no = transaction.delivery_receipt_no;
                onetranstable.customer_name = transaction.customer.customer_name;
                onetranstable.total_amount = transaction.total_amount;
                onetranstable.amount_payed = transaction.amount_payed;
                transtable.Add(onetranstable);
            }
            return transtable;
        }
    }
}
