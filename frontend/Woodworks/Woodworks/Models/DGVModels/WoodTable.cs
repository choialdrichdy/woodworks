﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Woodworks.Models.DGVModels
{
    class WoodTable
    {
        [System.ComponentModel.DisplayName("ID")]
        public int id { get; set; }
        [System.ComponentModel.DisplayName("Type")]
        public String wood_type { get; set; }
        [System.ComponentModel.DisplayName("Thickness")]
        public string wood_thickness { get; set; }
        [System.ComponentModel.DisplayName("Width")]
        public string wood_width { get; set; }
        [System.ComponentModel.DisplayName("Height")]
        public string wood_height { get; set; }
        [System.ComponentModel.DisplayName("Price")]
        public float wood_price { get; set; }
        [System.ComponentModel.DisplayName("Stock")]
        public int wood_stock { get; set; }

        public static List<WoodTable> convertToTableReady(List<Wood> woods)
        {
            List<WoodTable> transtable = new List<WoodTable>();
            foreach (Wood wood in woods)
            {
                WoodTable onetranstable = new WoodTable();
                onetranstable.id = wood.id;
                onetranstable.wood_type = wood.wood_type;
                onetranstable.wood_thickness = Services.Services.doubleToFraction(wood.wood_thickness);
                onetranstable.wood_width = Services.Services.doubleToFraction(wood.wood_width);
                onetranstable.wood_height = Services.Services.doubleToFraction(wood.wood_height);
                onetranstable.wood_price = wood.wood_price;
                onetranstable.wood_stock = wood.wood_stock;
                transtable.Add(onetranstable);
            }
            return transtable;
        }
    }
}
