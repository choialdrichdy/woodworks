﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Woodworks.Models.DGVModels
{
    public class PaymentTable
    {
        [System.ComponentModel.DisplayName("ID")]
        public int id { get; set; }
        [System.ComponentModel.DisplayName("Customer name")]
        public String customer_name { get; set; }
        [System.ComponentModel.DisplayName("Amount paid")]
        public float amount_paid { get; set; }
        [System.ComponentModel.DisplayName("Remarks")]
        public String remarks { get; set; }
        [System.ComponentModel.DisplayName("Type")]
        public String payment_type { get; set; }
        [System.ComponentModel.DisplayName("Payment date")]
        public DateTime payment_date { get; set; }
        [System.ComponentModel.DisplayName("Check number")]
        public String check_number { get; set; }
        [System.ComponentModel.DisplayName("Check bank")]
        public String check_bank { get; set; }
        [System.ComponentModel.DisplayName("Check branch")]
        public String check_branch { get; set; }
        [System.ComponentModel.DisplayName("Check date")]
        public DateTime check_date { get; set; }
    }
}
