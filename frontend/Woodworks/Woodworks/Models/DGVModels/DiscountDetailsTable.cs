﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Woodworks.Models.DGVModels
{
    public class DiscountDetailsTable
    {
        public int wood_id { get; set; }
        public string wood_type { get; set; }
        public string wood_thickness { get; set; }
        public string wood_width { get; set; }
        public string wood_height { get; set; }
        public float discount_percent { get; set; }

        public static List<DiscountDetailsTable> convertToTableReady(List<DiscountDetails> details)
        {
            List<DiscountDetailsTable> transtable = new List<DiscountDetailsTable>();
            foreach (DiscountDetails detail in details)
            {
                DiscountDetailsTable onetranstable = new DiscountDetailsTable();
                onetranstable.wood_id = detail.wood_id;
                onetranstable.wood_type = detail.wood.wood_type;
                onetranstable.wood_thickness = Services.Services.doubleToFraction(detail.wood.wood_thickness);
                onetranstable.wood_width = Services.Services.doubleToFraction(detail.wood.wood_width);
                onetranstable.wood_height = Services.Services.doubleToFraction(detail.wood.wood_height);
                onetranstable.discount_percent = detail.discount_percent;
                transtable.Add(onetranstable);
            }
            return transtable;
        }
    }
}
