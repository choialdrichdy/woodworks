﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Woodworks.Models
{
    public class Customer
    {
        [System.ComponentModel.DisplayName("ID")]
        public int id { get; set; }
        [System.ComponentModel.DisplayName("Name")]
        public String customer_name { get; set; }
        [System.ComponentModel.DisplayName("Address")]
        public String customer_address { get; set; }
        [System.ComponentModel.DisplayName("Terms")]
        public int customer_terms { get; set; }
        [System.ComponentModel.DisplayName("Current Balance")]
        public float current_balance { get; set; }
        [System.ComponentModel.DisplayName("PDC")]
        public float pdc { get; set; }
    }
}
