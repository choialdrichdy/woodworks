﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Woodworks.Models
{
    public class Order
    {
        [System.ComponentModel.DisplayName("Qty")]
        public int quantity { get; set; }
        [System.ComponentModel.DisplayName("ID")]
        public int wood_id { get; set; }
        [System.ComponentModel.DisplayName("Type")]
        public String wood_type { get;  set; }
        [System.ComponentModel.DisplayName("Thickness")]
        public String wood_thickness { get; set; }
        [System.ComponentModel.DisplayName("Width")]
        public String wood_width { get; set; }
        [System.ComponentModel.DisplayName("Height")]
        public String wood_height { get; set; }
        [System.ComponentModel.DisplayName("BF/LF")]
        public int bf_lf { get; set; }
        [System.ComponentModel.DisplayName("Price")]
        public float wood_price { get; set; }
        [System.ComponentModel.DisplayName("Less")]
        public float discount_percent { get; set; }
        [System.ComponentModel.DisplayName("Total")]
        public float total_price { get; set; }
        public Wood wood { get; set; }
    }
}
