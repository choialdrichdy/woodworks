﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Woodworks.Models
{
    public class CustomerDiscount
    {
        public int profile_id { get; set; }
        public int customer_id { get; set; }
        public Boolean is_active { get; set; }
    }
}
