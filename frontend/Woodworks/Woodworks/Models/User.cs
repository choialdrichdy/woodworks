﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Woodworks.Models
{
    public class User
    {
        public int id { get; set; }
        public String first_name { get; set; }
        public String last_name { get; set; }
        public String username { get; set; }
        public String password { get; set; }
        public List<String> user_permissions { get; set; }
    }
}
