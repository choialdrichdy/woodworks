﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Woodworks.Models
{
    class SpecialPrice
    {
        public int customer_id;
        public int wood_id;
        public float special_price;
    }
}
