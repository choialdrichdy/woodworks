﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Woodworks.Models
{
    public class Wood
    {
        [System.ComponentModel.DisplayName("ID")]
        public int id { get; set; }
        [System.ComponentModel.DisplayName("Type")]
        public String wood_type { get; set; }
        [System.ComponentModel.DisplayName("Thickness")]
        public float wood_thickness { get; set; }
        [System.ComponentModel.DisplayName("Width")]
        public float wood_width { get; set; }
        [System.ComponentModel.DisplayName("Height")]
        public float wood_height { get; set; }
        [System.ComponentModel.DisplayName("Price")]
        public float wood_price { get; set; }
        [System.ComponentModel.DisplayName("Stock")]
        public int wood_stock { get; set; }
        public String full_name
        {
            get
            {
                return (wood_type + " " + Services.Services.doubleToFraction(wood_thickness) + " " + Services.Services.doubleToFraction(wood_width) + " " + Services.Services.doubleToFraction(wood_height)).Replace(" ","");
            }
        }
    }
}
