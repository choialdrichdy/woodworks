﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Woodworks.Models
{
    public class DiscountDetails
    {
        public int profile_id { get; set; }
        public int wood_id { get; set; }
        public float discount_percent { get; set; }
        public Wood wood { get; set; }
    }
}
