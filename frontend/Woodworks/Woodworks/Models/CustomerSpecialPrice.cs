﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Woodworks.Models
{
    class CustomerSpecialPrice
    {
        public int customer_id { get; set; }
        public int wood_id { get; set; }
        public float special_price { get; set; }
    }
}
