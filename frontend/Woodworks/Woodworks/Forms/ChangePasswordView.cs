﻿using Newtonsoft.Json.Linq;
using RestSharp;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Woodworks.Network;

namespace Woodworks.Forms
{
    public partial class ChangePasswordView : Form
    {
        public ChangePasswordView()
        {
            InitializeComponent();
        }

        private void chgpwBtn_Click(object sender, EventArgs e)
        {
            if (newpwTextbox.Text.Equals(confirmpwTextbox.Text))
            {
                JObject credentials = new JObject();
                credentials["old_password"] = oldpwTextbox.Text;
                credentials["new_password"] = newpwTextbox.Text;

                RestClient client = WoodworksClient.Instance;

                var request = new RestRequest("users/changepw", Method.POST);
                request.AddParameter("application/json", credentials, ParameterType.RequestBody);

                IRestResponse response = client.Execute(request);
                var content = response.Content;

                MessageBox.Show(content, "Information", MessageBoxButtons.OK, MessageBoxIcon.None);
                this.Close();
            }
            else
            {
                MessageBox.Show("New password does not match.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
