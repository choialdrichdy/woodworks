﻿namespace Woodworks.Forms
{
    partial class ChangePasswordView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.oldpwTextbox = new System.Windows.Forms.TextBox();
            this.newpwTextbox = new System.Windows.Forms.TextBox();
            this.confirmpwTextbox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.chgpwBtn = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // oldpwTextbox
            // 
            this.oldpwTextbox.Location = new System.Drawing.Point(137, 42);
            this.oldpwTextbox.Name = "oldpwTextbox";
            this.oldpwTextbox.PasswordChar = '*';
            this.oldpwTextbox.Size = new System.Drawing.Size(135, 20);
            this.oldpwTextbox.TabIndex = 0;
            // 
            // newpwTextbox
            // 
            this.newpwTextbox.Location = new System.Drawing.Point(137, 68);
            this.newpwTextbox.Name = "newpwTextbox";
            this.newpwTextbox.PasswordChar = '*';
            this.newpwTextbox.Size = new System.Drawing.Size(135, 20);
            this.newpwTextbox.TabIndex = 1;
            // 
            // confirmpwTextbox
            // 
            this.confirmpwTextbox.Location = new System.Drawing.Point(137, 94);
            this.confirmpwTextbox.Name = "confirmpwTextbox";
            this.confirmpwTextbox.PasswordChar = '*';
            this.confirmpwTextbox.Size = new System.Drawing.Size(135, 20);
            this.confirmpwTextbox.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(54, 45);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(77, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Old password :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(48, 71);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(83, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "New password :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 97);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(119, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Confirm new password :";
            // 
            // chgpwBtn
            // 
            this.chgpwBtn.Location = new System.Drawing.Point(90, 132);
            this.chgpwBtn.Name = "chgpwBtn";
            this.chgpwBtn.Size = new System.Drawing.Size(113, 23);
            this.chgpwBtn.TabIndex = 6;
            this.chgpwBtn.Text = "Change password";
            this.chgpwBtn.UseVisualStyleBackColor = true;
            this.chgpwBtn.Click += new System.EventHandler(this.chgpwBtn_Click);
            // 
            // ChangePasswordView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(303, 176);
            this.Controls.Add(this.chgpwBtn);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.confirmpwTextbox);
            this.Controls.Add(this.newpwTextbox);
            this.Controls.Add(this.oldpwTextbox);
            this.Name = "ChangePasswordView";
            this.Text = "ChangePasswordView";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox oldpwTextbox;
        private System.Windows.Forms.TextBox newpwTextbox;
        private System.Windows.Forms.TextBox confirmpwTextbox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button chgpwBtn;
    }
}