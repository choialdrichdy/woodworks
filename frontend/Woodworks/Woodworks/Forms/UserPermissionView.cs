﻿using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Woodworks.Models;
using Woodworks.Network;

namespace Woodworks.Forms
{
    public partial class UserPermissionView : Form
    {
        public User user;
    
        public UserPermissionView()
        {
            InitializeComponent();
            setPermissions();
        }

        public UserPermissionView(User user)
        {
            InitializeComponent();
            this.user = user;
            setPermissions();
        }

        private void setPermissions()
        {
            List<String> permissions = user.user_permissions;

            if (permissions.Contains("create_item"))
            {
                createItemCkbx.Checked = true;
            }
            if (permissions.Contains("edit_items"))
            {
                editItemCkbx.Checked = true;
            }
            if (permissions.Contains("delete_items"))
            {
                deleteItemCkbx.Checked = true;
            }
            if (permissions.Contains("manage_stocks"))
            {
                manageStockCkbx.Checked = true;
            }
            if (permissions.Contains("create_customers"))
            {
                createCustomerCkbx.Checked = true;
            }
            if (permissions.Contains("edit_customers"))
            {
                editCustomerCkbx.Checked = true;
            }
            if (permissions.Contains("delete_customers"))
            {
                deleteCustomerCkbx.Checked = true;
            }
            if (permissions.Contains("create_transactions"))
            {
                createTransactionCkbx.Checked = true;
            }
            if (permissions.Contains("edit_transactions"))
            {
                editTransactionCkbx.Checked = true;
            }
            if (permissions.Contains("export_transaction"))
            {
                exportTransactionCkbx.Checked = true;
            }
            if (permissions.Contains("manage_payments"))
            {
                managePaymentCkbx.Checked = true;
            }
            if (permissions.Contains("manage_discount_rules"))
            {
                manageDiscountruleCkbx.Checked = true;
            }
            if (permissions.Contains("manage_special_price"))
            {
                manageSpecialpriceCkbx.Checked = true;
            }
            if (permissions.Contains("manage_customer_discounts"))
            {
                manageCustomerdiscountsCkbx.Checked = true;
            }
            if (permissions.Contains("admin"))
            {
                adminCkbx.Checked = true;
            }
        }
        private void button1_Click(object sender, EventArgs e)
        {
            List<String> permissions = new List<String>();
            if (createItemCkbx.Checked)
            {
                permissions.Add("create_item");
            }
            if (editItemCkbx.Checked)
            {
                permissions.Add("edit_items");
            }
            if (deleteItemCkbx.Checked)
            {
                permissions.Add("delete_items");
            }
            if (manageStockCkbx.Checked)
            {
                permissions.Add("manage_stocks");
            }
            if (createCustomerCkbx.Checked)
            {
                permissions.Add("create_customers");
            }
            if (editCustomerCkbx.Checked)
            {
                permissions.Add("edit_customers");
            }
            if (deleteCustomerCkbx.Checked)
            {
                permissions.Add("delete_customers");
            }
            if (managePaymentCkbx.Checked)
            {
                permissions.Add("manage_payments");
            }
            if (createTransactionCkbx.Checked)
            {
                permissions.Add("create_transactions");
            }
            if (editTransactionCkbx.Checked)
            {
                permissions.Add("edit_transactions");
            }
            if (exportTransactionCkbx.Checked)
            {
                permissions.Add("export_transaction");
            }
            if (manageDiscountruleCkbx.Checked)
            {
                permissions.Add("manage_discount_rules");
            }
            if (manageSpecialpriceCkbx.Checked)
            {
                permissions.Add("manage_special_price");
            }
            if (manageCustomerdiscountsCkbx.Checked)
            {
                permissions.Add("manage_customer_discounts");
            }
            if (adminCkbx.Checked)
            {
                permissions.Add("admin");
            }
            RestClient client = WoodworksClient.Instance;
            string permissionsjson = JsonConvert.SerializeObject(permissions);


            var request = new RestRequest("user/" + user.id + "/permissions", Method.PUT);
            request.AddParameter("application/json", permissionsjson, ParameterType.RequestBody);

            IRestResponse response = client.Execute(request);
            var content = response.Content;

            MessageBox.Show(content, "Information", MessageBoxButtons.OK, MessageBoxIcon.None);
            this.Close();
        }

        private void adminCkbx_CheckedChanged(object sender, EventArgs e)
        {

        }
    }
}
