﻿using Equin.ApplicationFramework;
using Newtonsoft.Json;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using RestSharp;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Woodworks.Models;
using Woodworks.Network;

namespace Woodworks.Forms
{
    public partial class TransactionView : Form
    {
        RestClient client;
        List<Order> orders;
        List<Customer> customers;
        List<Wood> woods;
        List<Discount> discounts;
        String mode;

        BindingListView<Order> orderlist;

        Customer selectedCustomer;
        Wood selectedWood;
        Transaction transaction;
        public TransactionView()
        {
            InitializeComponent();
            initAdd();
        }
        
        public TransactionView(Transaction transaction)
        {
            InitializeComponent();
            initEdit(transaction.customer_id, transaction.id);
        }

        public void initAdd()
        {
            mode = "add";
            transaction = new Transaction();
            client = WoodworksClient.Instance;

            var request = new RestRequest("customers/simple", Method.GET);

            IRestResponse response = client.Execute(request);
            var content = response.Content;


            customers = JsonConvert.DeserializeObject<List<Customer>>(content);

            orders = new List<Order>();
            orderlist = new BindingListView<Order>(orders);
            orderDGV.DataSource = orderlist;

            customerCombobox.DataSource = customers;
            customerCombobox.DisplayMember = "customer_name";

            var customerAutocomplete = new AutoCompleteStringCollection();
            customerAutocomplete.AddRange(customers.Select(customer => customer.customer_name).ToArray());
            customerCombobox.AutoCompleteCustomSource = customerAutocomplete;
            customerCombobox.SelectedIndex = -1;

            var woodAutocomplete = new AutoCompleteStringCollection();
            woodAutocomplete.AddRange(woods.Select(wood => wood.full_name).ToArray());
            woodCombobox.AutoCompleteCustomSource = woodAutocomplete;
            woodCombobox.SelectedIndex = -1;
        }

        private void initEdit(int customer_id, int transaction_id)
        {
            mode = "edit";
            deleteButton.Enabled = false;

            client = WoodworksClient.Instance;

            var request = new RestRequest("customers/" + customer_id + "/transactions/" + transaction_id, Method.GET);

            IRestResponse response = client.Execute(request);
            var content = response.Content;
            Console.WriteLine(content);

            transaction = JsonConvert.DeserializeObject<Transaction>(content);
            selectedCustomer = transaction.customer;
            requestWood(selectedCustomer);
            woodGroupbox.Enabled = true;
            customerCombobox.Text = selectedCustomer.customer_name;
            customerCombobox.Enabled = false;
            setCustomerCombobox(selectedCustomer);
            drTextbox.Enabled = false;
            setTransactionDetails(transaction);

            orders = Services.Services.formatOrder(transaction.details);
            orderlist = new BindingListView<Order>(orders);
            orderDGV.DataSource = orderlist;

            var woodAutocomplete = new AutoCompleteStringCollection();
            woodAutocomplete.AddRange(woods.Select(wood => wood.full_name).ToArray());
            woodCombobox.AutoCompleteCustomSource = woodAutocomplete;
            woodCombobox.SelectedIndex = -1;
        }
        private void customerCombobox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (customerCombobox.SelectedIndex != -1)
            {
                selectedCustomer = (Customer)customerCombobox.SelectedItem;
                setCustomerCombobox(selectedCustomer);
                transaction.customer_id = selectedCustomer.id;
                requestWood(selectedCustomer);

                

                

                woodGroupbox.Enabled = true;
            }
        }

        private void woodCombobox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (woodCombobox.SelectedIndex != -1)
            {
                selectedWood = (Wood)woodCombobox.SelectedItem;
                setWoodCombobox(selectedWood);
            }
        }

        private void setWoodCombobox(Wood wood)
        {
            typeTextbox.Text = wood.wood_type.ToString();
            lengthTextbox.Text = Services.Services.doubleToFraction(wood.wood_thickness);
            widthTextbox.Text = Services.Services.doubleToFraction(wood.wood_width);
            heightTextbox.Text = Services.Services.doubleToFraction(wood.wood_height);
            priceTextbox.Text = wood.wood_price.ToString();
        }

        private void setCustomerCombobox(Customer customer)
        {
            addressTextbox.Text = customer.customer_address;
            currentBalanceTextbox.Text = customer.current_balance.ToString();
            termsTextbox.Text = customer.customer_terms.ToString();
        }

        private void setTransactionDetails(Transaction transaction)
        {
            drTextbox.Text = transaction.delivery_receipt_no;
            transactionStatusTextbox.Text = transaction.transaction_status;
            totalAmountTextbox.Text = transaction.total_amount.ToString();
        }

        private Boolean checkIfEnoughStock(Wood wood, int qty)
        {
            if (wood.wood_stock > qty)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        private void confirmButton_Click(object sender, EventArgs e)
        {
            if (selectedWood != null)
            {
                if (checkIfEnoughStock(selectedWood, (int)quantityNumeric.Value))
                {
                    Order order = new Order();
                    order.wood = selectedWood;
                    order.wood_id = selectedWood.id;
                    order.wood_type = selectedWood.wood_type;
                    order.wood_thickness = Services.Services.doubleToFraction(selectedWood.wood_thickness);
                    order.wood_width = Services.Services.doubleToFraction(selectedWood.wood_width);
                    order.wood_height = Services.Services.doubleToFraction(selectedWood.wood_height);
                    order.wood_price = selectedWood.wood_price;
                    order.quantity = (int)quantityNumeric.Value;
                    order.bf_lf = Services.Services.computeBFLF(order);

                    Discount discount = discounts.SingleOrDefault(i => i.wood_id == order.wood_id) ?? new Discount();
                    order.discount_percent = discount.discount_percent;
                    order.total_price = (order.bf_lf * order.wood_price * (100 - order.discount_percent)) / 100;

                    addToOrders(order);
                }
                else
                {
                    MessageBox.Show("Not enough stock [Available: " + selectedWood.wood_stock + "]", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void deleteButton_Click(object sender, EventArgs e)
        {
            if (orderDGV.CurrentCell != null)
            {
                orders.RemoveAt(orderDGV.CurrentCell.RowIndex);
                setTotalAmount();
                orderlist.Refresh();
            }
        }

        private void requestWood(Customer selectedCustomer)
        {
            var request = new RestRequest("customers/" + selectedCustomer.id + "/woods", Method.GET);

            IRestResponse response = client.Execute(request);
            var content = response.Content;


            var request2 = new RestRequest("customers/" + selectedCustomer.id + "/discounts", Method.GET);

            IRestResponse response2 = client.Execute(request2);
            var content2 = response2.Content;

            discounts = JsonConvert.DeserializeObject<List<Discount>>(content2);

            woods = JsonConvert.DeserializeObject <List<Wood>> (content);
            woodCombobox.DataSource = woods;
            woodCombobox.DisplayMember = "full_name";
        }

        private void addToOrders(Order order)
        {
            if (!orders.Any(i => i.wood_id == order.wood_id))
            {
                orders.Add(order);
            }
            else
            {
                Order existingOrder = orders.Single(i => i.wood_id == order.wood_id);
                existingOrder.quantity = existingOrder.quantity + order.quantity;
                existingOrder.bf_lf = Services.Services.computeBFLF(existingOrder);
                existingOrder.total_price = (existingOrder.bf_lf * existingOrder.wood_price * (100 - existingOrder.discount_percent)) / 100;
            }
            orderlist.Refresh();
            setTotalAmount();
            clearWood();
            woodCombobox.Focus();
        }

        private void clearWood()
        {
            woodCombobox.SelectedIndex = -1;
            typeTextbox.Text = "";
            lengthTextbox.Text = "";
            widthTextbox.Text = "";
            heightTextbox.Text = "";
            priceTextbox.Text = "";
            quantityNumeric.Value = 0;
        }

        private void setTotalAmount()
        {
            float totalPrice = 0;
            foreach (Order order in orders)
            {
                totalPrice = totalPrice + order.total_price;
            }
            totalAmountTextbox.Text = totalPrice.ToString();
        }

        private float getTruePrice(Order order)
        {
            return order.wood_price;
        }

        private void addTransactionButton_Click(object sender, EventArgs e)
        {
            if (drTextbox.Text == "")
            {
                MessageBox.Show("DR No. is empty", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            transaction.total_amount = float.Parse(totalAmountTextbox.Text);
            transaction.details = orders;
            transaction.delivery_receipt_no = drTextbox.Text;
            transaction.customer_id = selectedCustomer.id;
            string transactionjson = JsonConvert.SerializeObject(transaction);
            if (mode == "add")
            {
                var request = new RestRequest("transactions", Method.POST);
                request.AddParameter("application/json", transactionjson, ParameterType.RequestBody);
                IRestResponse response = client.Execute(request);

                if (response.StatusCode.ToString().Equals("OK"))
                {
                    MessageBox.Show("Transaction added", "Success", MessageBoxButtons.OK, MessageBoxIcon.None);
                    this.Close();
                }
                else
                {
                    MessageBox.Show("Something went wrong. Please try again.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else if (mode == "edit")
            {
                var request = new RestRequest("transactions", Method.PUT);
                request.AddParameter("application/json", transactionjson, ParameterType.RequestBody);
                IRestResponse response = client.Execute(request);
                if (response.StatusCode.ToString().Equals("OK"))
                {
                    MessageBox.Show("Transaction edited", "Success", MessageBoxButtons.OK, MessageBoxIcon.None);
                    this.Close();
                }
                else
                {
                    MessageBox.Show("Something went wrong. Please try again.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            
        }

        private void excelBtn_Click(object sender, EventArgs e)
        {
            OpenFileDialog a = new OpenFileDialog();
            DialogResult b = a.ShowDialog();
            if (b == DialogResult.OK)
            {
                FileStream fs = new FileStream(a.FileName, FileMode.Open, FileAccess.Read);
                XSSFWorkbook workbook = new XSSFWorkbook(fs);
                XSSFSheet sheet = (XSSFSheet)workbook.GetSheet("Sheet1");
                for (int i = 0; i <= sheet.LastRowNum; i++)
                {
                    if (i > 0)
                    {
                        Order oneorder = new Order();
                        oneorder.wood = new Wood();
                        IRow d = sheet.GetRow(i);
                        foreach (ICell cell in d.Cells)
                        {
                            if (cell != null)
                            {

                                // TODO: you can add more cell types capatibility, e. g. formula
                                switch (cell.CellType)
                                {
                                    case NPOI.SS.UserModel.CellType.Numeric:
                                        if (cell.ColumnIndex == 0)
                                        {
                                            oneorder.quantity = (int)cell.NumericCellValue;
                                        }
                                        if (cell.ColumnIndex == 1)
                                        {
                                            oneorder.wood_id = (int)cell.NumericCellValue;
                                        }
                                        if (cell.ColumnIndex == 3)
                                        {
                                            oneorder.wood.wood_thickness = (float)cell.NumericCellValue;
                                            oneorder.wood_thickness = Services.Services.doubleToFraction(oneorder.wood.wood_thickness);
                                        }
                                        if (cell.ColumnIndex == 4)
                                        {
                                            oneorder.wood.wood_width = (float)cell.NumericCellValue;
                                            oneorder.wood_width = Services.Services.doubleToFraction(oneorder.wood.wood_width);
                                        }
                                        if (cell.ColumnIndex == 5)
                                        {
                                            oneorder.wood.wood_height = (float)cell.NumericCellValue;
                                            oneorder.wood_height = Services.Services.doubleToFraction(oneorder.wood.wood_height);
                                        }
                                        if (cell.ColumnIndex == 6)
                                        {
                                            oneorder.bf_lf = (int)cell.NumericCellValue;
                                        }
                                        if (cell.ColumnIndex == 7)
                                        {
                                            oneorder.wood_price = (float)cell.NumericCellValue;
                                        }
                                        if (cell.ColumnIndex == 8)
                                        {
                                            oneorder.discount_percent = (float)cell.NumericCellValue;
                                        }
                                        if (cell.ColumnIndex == 9)
                                        {
                                            oneorder.total_price = (float)cell.NumericCellValue;
                                        }

                                        //dataGridView1[j, i].Value = sh.GetRow(i).GetCell(j).NumericCellValue;
                                        break;
                                    case NPOI.SS.UserModel.CellType.String:
                                        if (cell.ColumnIndex == 2)
                                        {
                                            oneorder.wood_type = cell.StringCellValue;
                                        }

                                        break;
                                }
                            }
                        }
                       addToOrders(oneorder);
                    }
                }
            }
        }
    }
}
