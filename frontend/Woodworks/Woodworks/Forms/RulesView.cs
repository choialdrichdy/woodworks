﻿using Equin.ApplicationFramework;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Woodworks.Models;
using Woodworks.Network;

namespace Woodworks.Forms
{
    public partial class RulesView : Form
    {
        String mode;
        RestClient client;
        List<Wood> woods;
        List<Customer> customers;

        Customer selectedCustomer;
        Wood selectedWood;

        List<WoodDiscount> discounts;
        BindingListView<WoodDiscount> discountlists;



        public RulesView(String mode)
        {
            this.mode = mode;
            InitializeComponent();

            client = WoodworksClient.Instance;

            var request = new RestRequest("woods", Method.GET);

            IRestResponse response = client.Execute(request);
            var content = response.Content;
            woods = JsonConvert.DeserializeObject<List<Wood>>(content);
            woodCombobox.DataSource = woods;
            woodCombobox.DisplayMember = "full_name";
            discounts = new List<WoodDiscount>();
            discountlists = new BindingListView<WoodDiscount>(discounts);
            if ( mode == "special" )
            {
                customerNameLabel.Visible = true;
                customerNameCombobox.Visible = true;
                var request2 = new RestRequest("customers", Method.GET);

                IRestResponse response2 = client.Execute(request2);
                var content2 = response2.Content;
                customers = JsonConvert.DeserializeObject<List<Customer>>(content2);
                customerNameCombobox.DataSource = customers;
                customerNameCombobox.DisplayMember = "customer_name";
                discountLabel.Text = "Special Price : ";
            }
            if (mode == "rules" )
            {
                profileNameLabel.Visible = true;
                profileNameTextbox.Visible = true;

                discountLabel.Text = "Discount Percent : ";
            }
            
            
            discountsDGV.DataSource = discountlists;
        }

        private void woodCombobox_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void addBtn_Click(object sender, EventArgs e)
        {
            if (discountTextbox.Text.Equals(""))
            {
                return;
            }
            selectedWood = (Wood)woodCombobox.SelectedItem;
            WoodDiscount discount = new WoodDiscount();
            discount.id = selectedWood.id;
            discount.wood_type = selectedWood.wood_type;
            discount.wood_width = Services.Services.doubleToFraction(selectedWood.wood_width);
            discount.wood_height = Services.Services.doubleToFraction(selectedWood.wood_height);
            discount.wood_thickness = Services.Services.doubleToFraction(selectedWood.wood_thickness);
            discount.discount = float.Parse(discountTextbox.Text);
            if (!discounts.Any(i => i.id == discount.id))
            {
                discounts.Add(discount);
            }
            else
            {
                WoodDiscount existingDiscount = discounts.Single(i => i.id == discount.id);
                existingDiscount.discount = discount.discount;
            }
            discountlists.Refresh();
            /*setTotalAmount();
            selectedWood = (Wood)woodCombobox.SelectedItem;
            WoodDiscount discount = new WoodDiscount();
            discount.id = selectedWood.id;
            discount.wood_type = selectedWood.wood_type;
            discount.wood_width = selectedWood.wood_width;
            discount.wood_height = selectedWood.wood_height;
            discount.wood_thickness = selectedWood.wood_thickness;
            discount.discount = float.Parse(discountTextbox.Text);
            discounts.Add(discount);
            discountlists.Refresh();*/
        }

        private void confirmBtn_Click(object sender, EventArgs e)
        {
            String json;
            if (mode == "special")
            {
                List<CustomerSpecialPrice> csplist = new List<CustomerSpecialPrice>();
                foreach (WoodDiscount discount in discounts)
                {
                    CustomerSpecialPrice csp = new CustomerSpecialPrice();
                    csp.customer_id = selectedCustomer.id;
                    csp.wood_id = discount.id;
                    csp.special_price = discount.discount;
                    csplist.Add(csp);
                }
                json = formulateJSON<List<CustomerSpecialPrice>>(csplist);
                var request = new RestRequest("specialprices/" + selectedCustomer.id, Method.POST);
                request.AddParameter("application/json", json, ParameterType.RequestBody);
                IRestResponse response = client.Execute(request);
                if (response.StatusCode.ToString().Equals("OK"))
                {
                    MessageBox.Show("Special prices updated", "Success", MessageBoxButtons.OK, MessageBoxIcon.None);
                    this.Close();
                }
                else
                {
                    MessageBox.Show("Something went wrong. Please try again.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else if (mode == "rules")
            {
                DiscountProfile profile = new DiscountProfile();
                profile.profile_name = profileNameTextbox.Text;
                profile.details = new List<DiscountDetails>();
                foreach (WoodDiscount discount in discounts)
                {
                    DiscountDetails detail = new DiscountDetails();
                    detail.wood_id = discount.id;
                    detail.discount_percent = discount.discount;
                    profile.details.Add(detail);
                }
                json = formulateJSON<DiscountProfile>(profile);
                var request = new RestRequest("rules", Method.POST);
                request.AddParameter("application/json", json, ParameterType.RequestBody);
                IRestResponse response = client.Execute(request);
                if (response.StatusCode.ToString().Equals("OK"))
                {
                    MessageBox.Show("Rules applied", "Success", MessageBoxButtons.OK, MessageBoxIcon.None);
                    this.Close();
                }
                else
                {
                    MessageBox.Show("Something went wrong. Please try again.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private String formulateJSON<T>(T stuff)
        {
            return JsonConvert.SerializeObject(stuff);
        }
        private void customerNameCombobox_SelectedIndexChanged(object sender, EventArgs e)
        {
            selectedCustomer = (Customer)customerNameCombobox.SelectedItem;
            var request = new RestRequest("customers/" + selectedCustomer.id + "/specialprice", Method.GET);

            IRestResponse response = client.Execute(request);
            var content = response.Content;
            List<WoodDiscount> discountslist = Services.Services.formatWoodDiscount(JsonConvert.DeserializeObject<List<WoodDiscount>>(content));
            discounts.Clear();
            discounts.AddRange(discountslist);
            discountlists.Refresh();
        }

        private void dropButton_Click(object sender, EventArgs e)
        {
            if (discountsDGV.CurrentCell != null)
            {
                discounts.RemoveAt(discountsDGV.CurrentCell.RowIndex);
                discountlists.Refresh();
            }
        }

        private void cancelBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
