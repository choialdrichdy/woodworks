﻿using Newtonsoft.Json.Linq;
using RestSharp;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Woodworks.Models;
using Woodworks.Network;

namespace Woodworks
{
    public partial class StockView : Form
    {
        Wood selectedWood;

        public StockView(Wood wood)
        {
            InitializeComponent();
            selectedWood = wood;
        }

        private void confirmButton_Click(object sender, EventArgs e)
        {
            int stock = 0;
            int.TryParse(stockTextbox.Text, out stock);
            if (stock > 0)
            {
                RestClient client = WoodworksClient.Instance;

                JObject stockjson = new JObject();
                stockjson["num_stock"] = stock;
                var request = new RestRequest("woods/" + selectedWood.id + "/stock", Method.POST);
                request.AddParameter("application/json", stockjson, ParameterType.RequestBody);

                IRestResponse response = client.Execute(request);
                if (response.StatusCode.ToString().Equals("OK"))
                {
                    MessageBox.Show("Stock updated", "Success", MessageBoxButtons.OK, MessageBoxIcon.None);
                    this.Close();
                }
                else
                {
                    MessageBox.Show("Something went wrong. Please try again.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                MessageBox.Show("Please enter a valid amount.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
