﻿using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Woodworks.Models;
using Woodworks.Network;

namespace Woodworks.Forms
{
    public partial class CustomerDiscountView : Form
    {
        RestClient client;
        List<Customer> customers;
        List<CustomerDiscount> customerdiscounts;
        DiscountProfile selectedProfile;

        public CustomerDiscountView(DiscountProfile profile)
        {
            if (profile == null)
            {
                selectedProfile = new DiscountProfile();
            }
            else
            {
                selectedProfile = profile;
            }

            InitializeComponent();
            profileNameTextbox.Text = selectedProfile.profile_name;
            profileNameTextbox.Enabled = false;

            client = WoodworksClient.Instance;
            var request = new RestRequest("customers/simple", Method.GET);
            //request.AddParameter("application/json", credentials, ParameterType.RequestBody);

            IRestResponse response = client.Execute(request);
            String content = response.Content;
            customers = JsonConvert.DeserializeObject<List<Customer>>(content);
            customerListBox.DataSource = customers;
            customerListBox.DisplayMember = "customer_name";

            var request2 = new RestRequest("/rules/" + selectedProfile.id + "/customers", Method.GET);
            //request.AddParameter("application/json", credentials, ParameterType.RequestBody);

            IRestResponse response2 = client.Execute(request2);
            String content2 = response2.Content;
            customerdiscounts = JsonConvert.DeserializeObject<List<CustomerDiscount>>(content2);
            setCheckbox();
        }

        private void setCheckbox()
        {
            foreach (CustomerDiscount customerdiscount in customerdiscounts)
            {
                foreach (Customer customer in customers)
                {
                    if(customer.id == customerdiscount.customer_id)
                    {
                        int index = customerListBox.Items.IndexOf(customer);
                        customerListBox.SetItemChecked(index, true);
                    }
                }
            }
        }
        private void saveBtn_Click(object sender, EventArgs e)
        {
            List<int> customer_ids = new List<int>();
            var customers = customerListBox.CheckedItems;
            foreach (Customer customer in customers)
            {
                customer_ids.Add(customer.id);
            }
            var idsjson = JsonConvert.SerializeObject(customer_ids);
            var request = new RestRequest("/rules/" + selectedProfile.id + "/customers", Method.PUT);
            request.AddParameter("application/json", idsjson, ParameterType.RequestBody);

            IRestResponse response = client.Execute(request);
            if (response.StatusCode.ToString().Equals("OK"))
            {
                MessageBox.Show("Discount updated successfully", "Success", MessageBoxButtons.OK, MessageBoxIcon.None);
                this.Close();
            }
            else
            {
                MessageBox.Show("Something went wrong. Please try again.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
