﻿namespace Woodworks.Forms
{
    partial class RulesView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.dropButton = new System.Windows.Forms.Button();
            this.addBtn = new System.Windows.Forms.Button();
            this.discountTextbox = new System.Windows.Forms.TextBox();
            this.discountLabel = new System.Windows.Forms.Label();
            this.woodCombobox = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.profileNameTextbox = new System.Windows.Forms.TextBox();
            this.profileNameLabel = new System.Windows.Forms.Label();
            this.discountsDGV = new System.Windows.Forms.DataGridView();
            this.confirmBtn = new System.Windows.Forms.Button();
            this.cancelBtn = new System.Windows.Forms.Button();
            this.customerNameLabel = new System.Windows.Forms.Label();
            this.customerNameCombobox = new System.Windows.Forms.ComboBox();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.discountsDGV)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.dropButton);
            this.groupBox1.Controls.Add(this.addBtn);
            this.groupBox1.Controls.Add(this.discountTextbox);
            this.groupBox1.Controls.Add(this.discountLabel);
            this.groupBox1.Controls.Add(this.woodCombobox);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(12, 50);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(478, 130);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Rule Information";
            // 
            // dropButton
            // 
            this.dropButton.Location = new System.Drawing.Point(290, 101);
            this.dropButton.Name = "dropButton";
            this.dropButton.Size = new System.Drawing.Size(75, 23);
            this.dropButton.TabIndex = 6;
            this.dropButton.Text = "Drop";
            this.dropButton.UseVisualStyleBackColor = true;
            this.dropButton.Click += new System.EventHandler(this.dropButton_Click);
            // 
            // addBtn
            // 
            this.addBtn.Location = new System.Drawing.Point(94, 101);
            this.addBtn.Name = "addBtn";
            this.addBtn.Size = new System.Drawing.Size(75, 23);
            this.addBtn.TabIndex = 5;
            this.addBtn.Text = "Add";
            this.addBtn.UseVisualStyleBackColor = true;
            this.addBtn.Click += new System.EventHandler(this.addBtn_Click);
            // 
            // discountTextbox
            // 
            this.discountTextbox.Location = new System.Drawing.Point(136, 63);
            this.discountTextbox.Name = "discountTextbox";
            this.discountTextbox.Size = new System.Drawing.Size(202, 20);
            this.discountTextbox.TabIndex = 4;
            // 
            // discountLabel
            // 
            this.discountLabel.AutoSize = true;
            this.discountLabel.Location = new System.Drawing.Point(48, 66);
            this.discountLabel.Name = "discountLabel";
            this.discountLabel.Size = new System.Drawing.Size(35, 13);
            this.discountLabel.TabIndex = 3;
            this.discountLabel.Text = "label2";
            // 
            // woodCombobox
            // 
            this.woodCombobox.FormattingEnabled = true;
            this.woodCombobox.Location = new System.Drawing.Point(136, 23);
            this.woodCombobox.Name = "woodCombobox";
            this.woodCombobox.Size = new System.Drawing.Size(202, 21);
            this.woodCombobox.TabIndex = 2;
            this.woodCombobox.SelectedIndexChanged += new System.EventHandler(this.woodCombobox_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(88, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(42, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Wood :";
            // 
            // profileNameTextbox
            // 
            this.profileNameTextbox.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.profileNameTextbox.Location = new System.Drawing.Point(106, 26);
            this.profileNameTextbox.Name = "profileNameTextbox";
            this.profileNameTextbox.Size = new System.Drawing.Size(175, 20);
            this.profileNameTextbox.TabIndex = 0;
            this.profileNameTextbox.Visible = false;
            // 
            // profileNameLabel
            // 
            this.profileNameLabel.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.profileNameLabel.AutoSize = true;
            this.profileNameLabel.Location = new System.Drawing.Point(27, 29);
            this.profileNameLabel.Name = "profileNameLabel";
            this.profileNameLabel.Size = new System.Drawing.Size(73, 13);
            this.profileNameLabel.TabIndex = 1;
            this.profileNameLabel.Text = "Profile Name :";
            this.profileNameLabel.Visible = false;
            // 
            // discountsDGV
            // 
            this.discountsDGV.AllowUserToAddRows = false;
            this.discountsDGV.AllowUserToDeleteRows = false;
            this.discountsDGV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.discountsDGV.Location = new System.Drawing.Point(13, 186);
            this.discountsDGV.Name = "discountsDGV";
            this.discountsDGV.ReadOnly = true;
            this.discountsDGV.Size = new System.Drawing.Size(477, 323);
            this.discountsDGV.TabIndex = 1;
            // 
            // confirmBtn
            // 
            this.confirmBtn.Location = new System.Drawing.Point(63, 515);
            this.confirmBtn.Name = "confirmBtn";
            this.confirmBtn.Size = new System.Drawing.Size(75, 23);
            this.confirmBtn.TabIndex = 2;
            this.confirmBtn.Text = "Save";
            this.confirmBtn.UseVisualStyleBackColor = true;
            this.confirmBtn.Click += new System.EventHandler(this.confirmBtn_Click);
            // 
            // cancelBtn
            // 
            this.cancelBtn.Location = new System.Drawing.Point(361, 515);
            this.cancelBtn.Name = "cancelBtn";
            this.cancelBtn.Size = new System.Drawing.Size(75, 23);
            this.cancelBtn.TabIndex = 3;
            this.cancelBtn.Text = "Cancel";
            this.cancelBtn.UseVisualStyleBackColor = true;
            this.cancelBtn.Click += new System.EventHandler(this.cancelBtn_Click);
            // 
            // customerNameLabel
            // 
            this.customerNameLabel.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.customerNameLabel.AutoSize = true;
            this.customerNameLabel.Location = new System.Drawing.Point(12, 29);
            this.customerNameLabel.Name = "customerNameLabel";
            this.customerNameLabel.Size = new System.Drawing.Size(88, 13);
            this.customerNameLabel.TabIndex = 2;
            this.customerNameLabel.Text = "Customer Name :";
            this.customerNameLabel.Visible = false;
            // 
            // customerNameCombobox
            // 
            this.customerNameCombobox.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.customerNameCombobox.FormattingEnabled = true;
            this.customerNameCombobox.Location = new System.Drawing.Point(106, 26);
            this.customerNameCombobox.Name = "customerNameCombobox";
            this.customerNameCombobox.Size = new System.Drawing.Size(175, 21);
            this.customerNameCombobox.TabIndex = 3;
            this.customerNameCombobox.Visible = false;
            this.customerNameCombobox.SelectedIndexChanged += new System.EventHandler(this.customerNameCombobox_SelectedIndexChanged);
            // 
            // RulesView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(502, 550);
            this.Controls.Add(this.profileNameLabel);
            this.Controls.Add(this.customerNameLabel);
            this.Controls.Add(this.profileNameTextbox);
            this.Controls.Add(this.customerNameCombobox);
            this.Controls.Add(this.cancelBtn);
            this.Controls.Add(this.confirmBtn);
            this.Controls.Add(this.discountsDGV);
            this.Controls.Add(this.groupBox1);
            this.Name = "RulesView";
            this.Text = "RulesView";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.discountsDGV)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label profileNameLabel;
        private System.Windows.Forms.TextBox profileNameTextbox;
        private System.Windows.Forms.DataGridView discountsDGV;
        private System.Windows.Forms.Button addBtn;
        private System.Windows.Forms.TextBox discountTextbox;
        private System.Windows.Forms.Label discountLabel;
        private System.Windows.Forms.ComboBox woodCombobox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button confirmBtn;
        private System.Windows.Forms.Button cancelBtn;
        private System.Windows.Forms.Label customerNameLabel;
        private System.Windows.Forms.ComboBox customerNameCombobox;
        private System.Windows.Forms.Button dropButton;
    }
}