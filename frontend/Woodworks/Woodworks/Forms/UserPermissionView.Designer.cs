﻿namespace Woodworks.Forms
{
    partial class UserPermissionView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.adminCkbx = new System.Windows.Forms.CheckBox();
            this.button1 = new System.Windows.Forms.Button();
            this.createItemCkbx = new System.Windows.Forms.CheckBox();
            this.deleteItemCkbx = new System.Windows.Forms.CheckBox();
            this.editItemCkbx = new System.Windows.Forms.CheckBox();
            this.manageStockCkbx = new System.Windows.Forms.CheckBox();
            this.createCustomerCkbx = new System.Windows.Forms.CheckBox();
            this.deleteCustomerCkbx = new System.Windows.Forms.CheckBox();
            this.managePaymentCkbx = new System.Windows.Forms.CheckBox();
            this.editCustomerCkbx = new System.Windows.Forms.CheckBox();
            this.createTransactionCkbx = new System.Windows.Forms.CheckBox();
            this.exportTransactionCkbx = new System.Windows.Forms.CheckBox();
            this.editTransactionCkbx = new System.Windows.Forms.CheckBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.manageSpecialpriceCkbx = new System.Windows.Forms.CheckBox();
            this.manageCustomerdiscountsCkbx = new System.Windows.Forms.CheckBox();
            this.manageDiscountruleCkbx = new System.Windows.Forms.CheckBox();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.manageStockCkbx);
            this.groupBox1.Controls.Add(this.editItemCkbx);
            this.groupBox1.Controls.Add(this.deleteItemCkbx);
            this.groupBox1.Controls.Add(this.createItemCkbx);
            this.groupBox1.Location = new System.Drawing.Point(13, 42);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(381, 100);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Inventory";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.editCustomerCkbx);
            this.groupBox2.Controls.Add(this.managePaymentCkbx);
            this.groupBox2.Controls.Add(this.deleteCustomerCkbx);
            this.groupBox2.Controls.Add(this.createCustomerCkbx);
            this.groupBox2.Location = new System.Drawing.Point(13, 148);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(381, 100);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Customer";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.editTransactionCkbx);
            this.groupBox3.Controls.Add(this.exportTransactionCkbx);
            this.groupBox3.Controls.Add(this.createTransactionCkbx);
            this.groupBox3.Location = new System.Drawing.Point(12, 254);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(382, 100);
            this.groupBox3.TabIndex = 2;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Transaction";
            // 
            // adminCkbx
            // 
            this.adminCkbx.AutoSize = true;
            this.adminCkbx.Location = new System.Drawing.Point(13, 19);
            this.adminCkbx.Name = "adminCkbx";
            this.adminCkbx.Size = new System.Drawing.Size(86, 17);
            this.adminCkbx.TabIndex = 3;
            this.adminCkbx.Text = "Administrator";
            this.adminCkbx.UseVisualStyleBackColor = true;
            this.adminCkbx.CheckedChanged += new System.EventHandler(this.adminCkbx_CheckedChanged);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(169, 476);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 4;
            this.button1.Text = "Save";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // createItemCkbx
            // 
            this.createItemCkbx.AutoSize = true;
            this.createItemCkbx.Location = new System.Drawing.Point(36, 28);
            this.createItemCkbx.Name = "createItemCkbx";
            this.createItemCkbx.Size = new System.Drawing.Size(85, 17);
            this.createItemCkbx.TabIndex = 0;
            this.createItemCkbx.Text = "Create Items";
            this.createItemCkbx.UseVisualStyleBackColor = true;
            // 
            // deleteItemCkbx
            // 
            this.deleteItemCkbx.AutoSize = true;
            this.deleteItemCkbx.Location = new System.Drawing.Point(36, 51);
            this.deleteItemCkbx.Name = "deleteItemCkbx";
            this.deleteItemCkbx.Size = new System.Drawing.Size(85, 17);
            this.deleteItemCkbx.TabIndex = 1;
            this.deleteItemCkbx.Text = "Delete Items";
            this.deleteItemCkbx.UseVisualStyleBackColor = true;
            // 
            // editItemCkbx
            // 
            this.editItemCkbx.AutoSize = true;
            this.editItemCkbx.Location = new System.Drawing.Point(221, 28);
            this.editItemCkbx.Name = "editItemCkbx";
            this.editItemCkbx.Size = new System.Drawing.Size(72, 17);
            this.editItemCkbx.TabIndex = 2;
            this.editItemCkbx.Text = "Edit Items";
            this.editItemCkbx.UseVisualStyleBackColor = true;
            // 
            // manageStockCkbx
            // 
            this.manageStockCkbx.AutoSize = true;
            this.manageStockCkbx.Location = new System.Drawing.Point(221, 51);
            this.manageStockCkbx.Name = "manageStockCkbx";
            this.manageStockCkbx.Size = new System.Drawing.Size(101, 17);
            this.manageStockCkbx.TabIndex = 3;
            this.manageStockCkbx.Text = "Manage Stocks";
            this.manageStockCkbx.UseVisualStyleBackColor = true;
            // 
            // createCustomerCkbx
            // 
            this.createCustomerCkbx.AutoSize = true;
            this.createCustomerCkbx.Location = new System.Drawing.Point(36, 27);
            this.createCustomerCkbx.Name = "createCustomerCkbx";
            this.createCustomerCkbx.Size = new System.Drawing.Size(109, 17);
            this.createCustomerCkbx.TabIndex = 1;
            this.createCustomerCkbx.Text = "Create Customers";
            this.createCustomerCkbx.UseVisualStyleBackColor = true;
            // 
            // deleteCustomerCkbx
            // 
            this.deleteCustomerCkbx.AutoSize = true;
            this.deleteCustomerCkbx.Location = new System.Drawing.Point(36, 50);
            this.deleteCustomerCkbx.Name = "deleteCustomerCkbx";
            this.deleteCustomerCkbx.Size = new System.Drawing.Size(109, 17);
            this.deleteCustomerCkbx.TabIndex = 2;
            this.deleteCustomerCkbx.Text = "Delete Customers";
            this.deleteCustomerCkbx.UseVisualStyleBackColor = true;
            // 
            // managePaymentCkbx
            // 
            this.managePaymentCkbx.AutoSize = true;
            this.managePaymentCkbx.Location = new System.Drawing.Point(221, 50);
            this.managePaymentCkbx.Name = "managePaymentCkbx";
            this.managePaymentCkbx.Size = new System.Drawing.Size(114, 17);
            this.managePaymentCkbx.TabIndex = 3;
            this.managePaymentCkbx.Text = "Manage Payments";
            this.managePaymentCkbx.UseVisualStyleBackColor = true;
            // 
            // editCustomerCkbx
            // 
            this.editCustomerCkbx.AutoSize = true;
            this.editCustomerCkbx.Location = new System.Drawing.Point(221, 27);
            this.editCustomerCkbx.Name = "editCustomerCkbx";
            this.editCustomerCkbx.Size = new System.Drawing.Size(96, 17);
            this.editCustomerCkbx.TabIndex = 4;
            this.editCustomerCkbx.Text = "Edit Customers";
            this.editCustomerCkbx.UseVisualStyleBackColor = true;
            // 
            // createTransactionCkbx
            // 
            this.createTransactionCkbx.AutoSize = true;
            this.createTransactionCkbx.Location = new System.Drawing.Point(37, 34);
            this.createTransactionCkbx.Name = "createTransactionCkbx";
            this.createTransactionCkbx.Size = new System.Drawing.Size(121, 17);
            this.createTransactionCkbx.TabIndex = 1;
            this.createTransactionCkbx.Text = "Create Transactions";
            this.createTransactionCkbx.UseVisualStyleBackColor = true;
            // 
            // exportTransactionCkbx
            // 
            this.exportTransactionCkbx.AutoSize = true;
            this.exportTransactionCkbx.Location = new System.Drawing.Point(37, 57);
            this.exportTransactionCkbx.Name = "exportTransactionCkbx";
            this.exportTransactionCkbx.Size = new System.Drawing.Size(120, 17);
            this.exportTransactionCkbx.TabIndex = 2;
            this.exportTransactionCkbx.Text = "Export Transactions";
            this.exportTransactionCkbx.UseVisualStyleBackColor = true;
            // 
            // editTransactionCkbx
            // 
            this.editTransactionCkbx.AutoSize = true;
            this.editTransactionCkbx.Location = new System.Drawing.Point(222, 34);
            this.editTransactionCkbx.Name = "editTransactionCkbx";
            this.editTransactionCkbx.Size = new System.Drawing.Size(108, 17);
            this.editTransactionCkbx.TabIndex = 3;
            this.editTransactionCkbx.Text = "Edit Transactions";
            this.editTransactionCkbx.UseVisualStyleBackColor = true;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.manageSpecialpriceCkbx);
            this.groupBox4.Controls.Add(this.manageCustomerdiscountsCkbx);
            this.groupBox4.Controls.Add(this.manageDiscountruleCkbx);
            this.groupBox4.Location = new System.Drawing.Point(12, 360);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(382, 100);
            this.groupBox4.TabIndex = 5;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Discounts";
            // 
            // manageSpecialpriceCkbx
            // 
            this.manageSpecialpriceCkbx.AutoSize = true;
            this.manageSpecialpriceCkbx.Location = new System.Drawing.Point(222, 34);
            this.manageSpecialpriceCkbx.Name = "manageSpecialpriceCkbx";
            this.manageSpecialpriceCkbx.Size = new System.Drawing.Size(135, 17);
            this.manageSpecialpriceCkbx.TabIndex = 3;
            this.manageSpecialpriceCkbx.Text = "Manage Special Prices";
            this.manageSpecialpriceCkbx.UseVisualStyleBackColor = true;
            // 
            // manageCustomerdiscountsCkbx
            // 
            this.manageCustomerdiscountsCkbx.AutoSize = true;
            this.manageCustomerdiscountsCkbx.Location = new System.Drawing.Point(37, 57);
            this.manageCustomerdiscountsCkbx.Name = "manageCustomerdiscountsCkbx";
            this.manageCustomerdiscountsCkbx.Size = new System.Drawing.Size(162, 17);
            this.manageCustomerdiscountsCkbx.TabIndex = 2;
            this.manageCustomerdiscountsCkbx.Text = "Manage Customer Discounts";
            this.manageCustomerdiscountsCkbx.UseVisualStyleBackColor = true;
            // 
            // manageDiscountruleCkbx
            // 
            this.manageDiscountruleCkbx.AutoSize = true;
            this.manageDiscountruleCkbx.Location = new System.Drawing.Point(37, 34);
            this.manageDiscountruleCkbx.Name = "manageDiscountruleCkbx";
            this.manageDiscountruleCkbx.Size = new System.Drawing.Size(140, 17);
            this.manageDiscountruleCkbx.TabIndex = 1;
            this.manageDiscountruleCkbx.Text = "Manage Discount Rules";
            this.manageDiscountruleCkbx.UseVisualStyleBackColor = true;
            // 
            // UserPermissionView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(408, 521);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.adminCkbx);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "UserPermissionView";
            this.Text = "UserPermissionView";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.CheckBox adminCkbx;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.CheckBox manageStockCkbx;
        private System.Windows.Forms.CheckBox editItemCkbx;
        private System.Windows.Forms.CheckBox deleteItemCkbx;
        private System.Windows.Forms.CheckBox createItemCkbx;
        private System.Windows.Forms.CheckBox editCustomerCkbx;
        private System.Windows.Forms.CheckBox managePaymentCkbx;
        private System.Windows.Forms.CheckBox deleteCustomerCkbx;
        private System.Windows.Forms.CheckBox createCustomerCkbx;
        private System.Windows.Forms.CheckBox editTransactionCkbx;
        private System.Windows.Forms.CheckBox exportTransactionCkbx;
        private System.Windows.Forms.CheckBox createTransactionCkbx;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.CheckBox manageSpecialpriceCkbx;
        private System.Windows.Forms.CheckBox manageCustomerdiscountsCkbx;
        private System.Windows.Forms.CheckBox manageDiscountruleCkbx;
    }
}