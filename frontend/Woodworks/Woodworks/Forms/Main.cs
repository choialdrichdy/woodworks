﻿using Newtonsoft.Json.Linq;
using RestSharp;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Woodworks.Models;
using Woodworks.Network;
using Woodworks.UserControls;

namespace Woodworks
{
    public partial class Main : Form
    {
        public Main()
        {
            InitializeComponent();
            init();
        }

        public void init()
        {
            WoodUserControl wuc = new WoodUserControl();
            TabPage inventory = new TabPage("Inventory");
            inventory.Controls.Add(wuc);
            mainTabControl.Controls.Add(inventory);

            CustomerUserControl cuc = new CustomerUserControl();
            TabPage customers = new TabPage("Customers");
            customers.Controls.Add(cuc);
            mainTabControl.Controls.Add(customers);

            TransactionViewUserControl tvuc = new TransactionViewUserControl();
            TabPage transaction = new TabPage("Transaction");
            transaction.Controls.Add(tvuc);
            mainTabControl.Controls.Add(transaction);

            DiscountUserControl duc = new DiscountUserControl();
            TabPage discount = new TabPage("Discounts");
            discount.Controls.Add(duc);
            mainTabControl.Controls.Add(discount);

            PaymentUserControl puc = new PaymentUserControl();
            TabPage payment = new TabPage("Payments");
            payment.Controls.Add(puc);
            mainTabControl.Controls.Add(payment);

            if (Services.Session.loggedInUser.user_permissions.Contains("admin"))
            {
                UserUserControl uuc = new UserUserControl();
                TabPage user = new TabPage("Users");
                user.Controls.Add(uuc);
                mainTabControl.Controls.Add(user);
            }
            else
            {
                UserServicesUserControl usuc = new UserServicesUserControl();
                TabPage userservices = new TabPage("Services");
                userservices.Controls.Add(usuc);
                mainTabControl.Controls.Add(userservices);
            }
        }

        private void addWoodButton_Click(object sender, EventArgs e)
        {
            this.Enabled = false;
            WoodView window = new WoodView("Add", null);
            window.FormClosed += (s, args) => this.Enabled = true;
            window.Show();
        }
    }
}
