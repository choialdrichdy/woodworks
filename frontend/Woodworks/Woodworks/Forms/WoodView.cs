﻿using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Woodworks.Models;
using Woodworks.Network;

namespace Woodworks
{
    public partial class WoodView : Form
    {
        private String action;
        private Wood wood;

        public WoodView()
        {
            InitializeComponent();
        }

        public WoodView(String action, Wood wood)
        {
            this.action = action;

            InitializeComponent();
            if (this.action.Equals("Add"))
            {
                this.Text = "Add Wood";
                confirmButton.Text = "Add";
                this.wood = new Wood();
            }
            else if (this.action.Equals("Edit"))
            {
                this.Text = "Edit Wood";
                confirmButton.Text = "Edit";
                this.wood = wood;
                woodTypeTextbox.Text = wood.wood_type;
                woodLengthTextbox.Text = Services.Services.doubleToFraction(wood.wood_thickness);
                woodWidthTextbox.Text = Services.Services.doubleToFraction(wood.wood_width);
                woodHeightTextbox.Text = Services.Services.doubleToFraction(wood.wood_height);
                woodPriceTextbox.Text = wood.wood_price.ToString();
            }
        }

        private void confirmWoodButton_Click(object sender, EventArgs e)
        {
            RestClient client = WoodworksClient.Instance;
            string woodjson = "";

            wood.wood_type = woodTypeTextbox.Text;
            wood.wood_thickness = (float)Services.Services.fractionToDouble(woodLengthTextbox.Text);
            wood.wood_width = (float)Services.Services.fractionToDouble(woodWidthTextbox.Text);
            wood.wood_height = (float)Services.Services.fractionToDouble(woodHeightTextbox.Text);
            wood.wood_price = float.Parse(woodPriceTextbox.Text);

            if (action.Equals("Add")) {
                List<Wood> listwoods = new List<Wood>();

                listwoods.Add(wood);
                woodjson = JsonConvert.SerializeObject(listwoods);


                var request = new RestRequest("woods", Method.POST);
                request.AddParameter("application/json", woodjson, ParameterType.RequestBody);

                IRestResponse response = client.Execute(request);
                if (response.StatusCode.ToString().Equals("OK"))
                {
                    MessageBox.Show("Add successful", "Success", MessageBoxButtons.OK, MessageBoxIcon.None);
                    this.Close();
                }
                else
                {
                    MessageBox.Show("Something went wrong. Please try again.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else if (action.Equals("Edit"))
            {
                woodjson = JsonConvert.SerializeObject(wood);

                var request = new RestRequest("woods/" + wood.id, Method.PUT);
                request.AddParameter("application/json", woodjson, ParameterType.RequestBody);

                IRestResponse response = client.Execute(request);
                if (response.StatusCode.ToString().Equals("OK"))
                {
                    MessageBox.Show("Edit successful", "Success", MessageBoxButtons.OK, MessageBoxIcon.None);
                    this.Close();
                }
                else
                {
                    MessageBox.Show("Something went wrong. Please try again.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }

        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

    }
}
