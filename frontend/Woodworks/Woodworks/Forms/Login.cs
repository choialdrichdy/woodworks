﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Woodworks.Models;
using Woodworks.Network;

namespace Woodworks
{
    public partial class Login : Form
    {
        RestClient client = null;

        public Login()
        {
            InitializeComponent();
        }

        private void loginButton_Click(object sender, EventArgs e)
        {
            String username = usernameTextbox.Text;
            String password = passwordTextbox.Text;

            JObject credentials = new JObject();
            credentials["username"] = username;
            credentials["password"] = password;

            client = WoodworksClient.Instance;

            var request = new RestRequest("login", Method.POST);
            request.AddParameter("application/json", credentials, ParameterType.RequestBody);

            IRestResponse response = client.Execute(request);
            var content = response.Content;


            User user = JsonConvert.DeserializeObject<User>(content);

            if(user.id != 0)
            {
                Services.Session.loggedInUser = user;
                this.Hide();
                Main main = new Main();
                main.FormClosed += (s, args) => this.Close();
                main.Show();
            }
            else
            {
                MessageBox.Show("Username or password is invalid.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void exitButton_Click(object sender, EventArgs e)
        {
            System.Windows.Forms.Application.Exit();
        }
    }
}
