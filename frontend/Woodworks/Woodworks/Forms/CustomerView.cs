﻿using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Woodworks.Models;
using Woodworks.Network;

namespace Woodworks.Forms
{
    public partial class CustomerView : Form
    {
        private String action;
        private Customer customer;

        public CustomerView()
        {
            InitializeComponent();
        }

        public CustomerView(String action, Customer customer)
        {
            this.action = action;
            InitializeComponent();
            if (this.action.Equals("Add"))
            {
                this.Text = "Add Customer";
                confirmButton.Text = "Add";
                this.customer = new Customer();
            }
            else if (this.action.Equals("Edit"))
            {
                this.Text = "Edit Customer";
                confirmButton.Text = "Edit";
                this.customer = customer;
                nameTextbox.Text = customer.customer_name;
                addressTextbox.Text = customer.customer_address;
                termsNumeric.Value = customer.customer_terms;
            }
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void confirmButton_Click(object sender, EventArgs e)
        {
            RestClient client = WoodworksClient.Instance;
            string customerjson = "";

            customer.customer_name = nameTextbox.Text;
            customer.customer_address = addressTextbox.Text;
            customer.customer_terms = (int)termsNumeric.Value;

            if (action.Equals("Add"))
            {
                List<Customer> listcustomers = new List<Customer>();

                listcustomers.Add(customer);
                customerjson = JsonConvert.SerializeObject(listcustomers);


                var request = new RestRequest("customers", Method.POST);
                request.AddParameter("application/json", customerjson, ParameterType.RequestBody);

                IRestResponse response = client.Execute(request);
                if (response.StatusCode.ToString().Equals("OK"))
                {
                    MessageBox.Show("Add successful", "Success", MessageBoxButtons.OK, MessageBoxIcon.None);
                    this.Close();
                }
                else
                {
                    MessageBox.Show("Something went wrong. Please try again.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else if (action.Equals("Edit"))
            {
                customerjson = JsonConvert.SerializeObject(customer);

                var request = new RestRequest("customers/" + customer.id, Method.PUT);
                request.AddParameter("application/json", customerjson, ParameterType.RequestBody);

                IRestResponse response = client.Execute(request);
                if (response.StatusCode.ToString().Equals("OK"))
                {
                    MessageBox.Show("Edit successful", "Success", MessageBoxButtons.OK, MessageBoxIcon.None);
                    this.Close();
                }
                else
                {
                    MessageBox.Show("Something went wrong. Please try again.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
    }
}
