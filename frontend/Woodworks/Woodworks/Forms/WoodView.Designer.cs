﻿namespace Woodworks
{
    partial class WoodView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.woodTypeLabel = new System.Windows.Forms.Label();
            this.woodThicknessLabel = new System.Windows.Forms.Label();
            this.woodWidthLabel = new System.Windows.Forms.Label();
            this.woodHeightLabel = new System.Windows.Forms.Label();
            this.woodTypeTextbox = new System.Windows.Forms.TextBox();
            this.woodPriceLabel = new System.Windows.Forms.Label();
            this.woodLengthTextbox = new System.Windows.Forms.TextBox();
            this.woodWidthTextbox = new System.Windows.Forms.TextBox();
            this.woodHeightTextbox = new System.Windows.Forms.TextBox();
            this.woodPriceTextbox = new System.Windows.Forms.TextBox();
            this.confirmButton = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.cancelButton = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // woodTypeLabel
            // 
            this.woodTypeLabel.AutoSize = true;
            this.woodTypeLabel.Location = new System.Drawing.Point(6, 26);
            this.woodTypeLabel.Name = "woodTypeLabel";
            this.woodTypeLabel.Size = new System.Drawing.Size(37, 13);
            this.woodTypeLabel.TabIndex = 0;
            this.woodTypeLabel.Text = "Type :";
            // 
            // woodThicknessLabel
            // 
            this.woodThicknessLabel.AutoSize = true;
            this.woodThicknessLabel.Location = new System.Drawing.Point(6, 52);
            this.woodThicknessLabel.Name = "woodThicknessLabel";
            this.woodThicknessLabel.Size = new System.Drawing.Size(62, 13);
            this.woodThicknessLabel.TabIndex = 1;
            this.woodThicknessLabel.Text = "Thickness :";
            // 
            // woodWidthLabel
            // 
            this.woodWidthLabel.AutoSize = true;
            this.woodWidthLabel.Location = new System.Drawing.Point(6, 78);
            this.woodWidthLabel.Name = "woodWidthLabel";
            this.woodWidthLabel.Size = new System.Drawing.Size(41, 13);
            this.woodWidthLabel.TabIndex = 2;
            this.woodWidthLabel.Text = "Width :";
            // 
            // woodHeightLabel
            // 
            this.woodHeightLabel.AutoSize = true;
            this.woodHeightLabel.Location = new System.Drawing.Point(6, 106);
            this.woodHeightLabel.Name = "woodHeightLabel";
            this.woodHeightLabel.Size = new System.Drawing.Size(44, 13);
            this.woodHeightLabel.TabIndex = 3;
            this.woodHeightLabel.Text = "Height :";
            // 
            // woodTypeTextbox
            // 
            this.woodTypeTextbox.Location = new System.Drawing.Point(90, 23);
            this.woodTypeTextbox.Name = "woodTypeTextbox";
            this.woodTypeTextbox.Size = new System.Drawing.Size(128, 20);
            this.woodTypeTextbox.TabIndex = 4;
            // 
            // woodPriceLabel
            // 
            this.woodPriceLabel.AutoSize = true;
            this.woodPriceLabel.Location = new System.Drawing.Point(6, 132);
            this.woodPriceLabel.Name = "woodPriceLabel";
            this.woodPriceLabel.Size = new System.Drawing.Size(37, 13);
            this.woodPriceLabel.TabIndex = 5;
            this.woodPriceLabel.Text = "Price :";
            // 
            // woodLengthTextbox
            // 
            this.woodLengthTextbox.Location = new System.Drawing.Point(90, 49);
            this.woodLengthTextbox.Name = "woodLengthTextbox";
            this.woodLengthTextbox.Size = new System.Drawing.Size(128, 20);
            this.woodLengthTextbox.TabIndex = 6;
            // 
            // woodWidthTextbox
            // 
            this.woodWidthTextbox.Location = new System.Drawing.Point(90, 75);
            this.woodWidthTextbox.Name = "woodWidthTextbox";
            this.woodWidthTextbox.Size = new System.Drawing.Size(128, 20);
            this.woodWidthTextbox.TabIndex = 7;
            // 
            // woodHeightTextbox
            // 
            this.woodHeightTextbox.Location = new System.Drawing.Point(90, 103);
            this.woodHeightTextbox.Name = "woodHeightTextbox";
            this.woodHeightTextbox.Size = new System.Drawing.Size(128, 20);
            this.woodHeightTextbox.TabIndex = 8;
            // 
            // woodPriceTextbox
            // 
            this.woodPriceTextbox.Location = new System.Drawing.Point(90, 129);
            this.woodPriceTextbox.Name = "woodPriceTextbox";
            this.woodPriceTextbox.Size = new System.Drawing.Size(128, 20);
            this.woodPriceTextbox.TabIndex = 9;
            // 
            // confirmButton
            // 
            this.confirmButton.Location = new System.Drawing.Point(12, 203);
            this.confirmButton.Name = "confirmButton";
            this.confirmButton.Size = new System.Drawing.Size(75, 23);
            this.confirmButton.TabIndex = 10;
            this.confirmButton.UseVisualStyleBackColor = true;
            this.confirmButton.Click += new System.EventHandler(this.confirmWoodButton_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.woodTypeLabel);
            this.groupBox1.Controls.Add(this.woodThicknessLabel);
            this.groupBox1.Controls.Add(this.woodPriceTextbox);
            this.groupBox1.Controls.Add(this.woodWidthLabel);
            this.groupBox1.Controls.Add(this.woodHeightTextbox);
            this.groupBox1.Controls.Add(this.woodHeightLabel);
            this.groupBox1.Controls.Add(this.woodTypeTextbox);
            this.groupBox1.Controls.Add(this.woodLengthTextbox);
            this.groupBox1.Controls.Add(this.woodWidthTextbox);
            this.groupBox1.Controls.Add(this.woodPriceLabel);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(224, 185);
            this.groupBox1.TabIndex = 11;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Details";
            // 
            // cancelButton
            // 
            this.cancelButton.Location = new System.Drawing.Point(161, 203);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(75, 23);
            this.cancelButton.TabIndex = 12;
            this.cancelButton.Text = "Cancel";
            this.cancelButton.UseVisualStyleBackColor = true;
            this.cancelButton.Click += new System.EventHandler(this.cancelButton_Click);
            // 
            // WoodView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(251, 240);
            this.Controls.Add(this.cancelButton);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.confirmButton);
            this.Name = "WoodView";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label woodTypeLabel;
        private System.Windows.Forms.Label woodThicknessLabel;
        private System.Windows.Forms.Label woodWidthLabel;
        private System.Windows.Forms.Label woodHeightLabel;
        private System.Windows.Forms.TextBox woodTypeTextbox;
        private System.Windows.Forms.Label woodPriceLabel;
        private System.Windows.Forms.TextBox woodLengthTextbox;
        private System.Windows.Forms.TextBox woodWidthTextbox;
        private System.Windows.Forms.TextBox woodHeightTextbox;
        private System.Windows.Forms.TextBox woodPriceTextbox;
        private System.Windows.Forms.Button confirmButton;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button cancelButton;
    }
}