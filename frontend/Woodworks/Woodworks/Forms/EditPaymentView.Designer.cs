﻿namespace Woodworks.Forms
{
    partial class EditPaymentView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.paymentDetailsGroupBox = new System.Windows.Forms.GroupBox();
            this.label7 = new System.Windows.Forms.Label();
            this.remarkTextbox = new System.Windows.Forms.TextBox();
            this.checkBranchTextbox = new System.Windows.Forms.TextBox();
            this.checkBankTextbox = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.checkDateLabel = new System.Windows.Forms.Label();
            this.checkDatePicker = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.checkNumberTextbox = new System.Windows.Forms.TextBox();
            this.amountPayedTextbox = new System.Windows.Forms.TextBox();
            this.checkRadioBtn = new System.Windows.Forms.RadioButton();
            this.cashRadioBtn = new System.Windows.Forms.RadioButton();
            this.confirmBtn = new System.Windows.Forms.Button();
            this.paymentDetailsGroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // paymentDetailsGroupBox
            // 
            this.paymentDetailsGroupBox.Controls.Add(this.label7);
            this.paymentDetailsGroupBox.Controls.Add(this.remarkTextbox);
            this.paymentDetailsGroupBox.Controls.Add(this.checkBranchTextbox);
            this.paymentDetailsGroupBox.Controls.Add(this.checkBankTextbox);
            this.paymentDetailsGroupBox.Controls.Add(this.label6);
            this.paymentDetailsGroupBox.Controls.Add(this.label5);
            this.paymentDetailsGroupBox.Controls.Add(this.checkDateLabel);
            this.paymentDetailsGroupBox.Controls.Add(this.checkDatePicker);
            this.paymentDetailsGroupBox.Controls.Add(this.label2);
            this.paymentDetailsGroupBox.Controls.Add(this.label1);
            this.paymentDetailsGroupBox.Controls.Add(this.checkNumberTextbox);
            this.paymentDetailsGroupBox.Controls.Add(this.amountPayedTextbox);
            this.paymentDetailsGroupBox.Controls.Add(this.checkRadioBtn);
            this.paymentDetailsGroupBox.Controls.Add(this.cashRadioBtn);
            this.paymentDetailsGroupBox.Location = new System.Drawing.Point(12, 12);
            this.paymentDetailsGroupBox.Name = "paymentDetailsGroupBox";
            this.paymentDetailsGroupBox.Size = new System.Drawing.Size(269, 238);
            this.paymentDetailsGroupBox.TabIndex = 1;
            this.paymentDetailsGroupBox.TabStop = false;
            this.paymentDetailsGroupBox.Text = "Payment Details";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(30, 179);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(55, 13);
            this.label7.TabIndex = 13;
            this.label7.Text = "Remarks :";
            // 
            // remarkTextbox
            // 
            this.remarkTextbox.Location = new System.Drawing.Point(91, 176);
            this.remarkTextbox.Multiline = true;
            this.remarkTextbox.Name = "remarkTextbox";
            this.remarkTextbox.Size = new System.Drawing.Size(172, 53);
            this.remarkTextbox.TabIndex = 12;
            // 
            // checkBranchTextbox
            // 
            this.checkBranchTextbox.Location = new System.Drawing.Point(91, 149);
            this.checkBranchTextbox.Name = "checkBranchTextbox";
            this.checkBranchTextbox.Size = new System.Drawing.Size(172, 20);
            this.checkBranchTextbox.TabIndex = 11;
            // 
            // checkBankTextbox
            // 
            this.checkBankTextbox.Location = new System.Drawing.Point(91, 123);
            this.checkBankTextbox.Name = "checkBankTextbox";
            this.checkBankTextbox.Size = new System.Drawing.Size(172, 20);
            this.checkBankTextbox.TabIndex = 10;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(4, 152);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(81, 13);
            this.label6.TabIndex = 9;
            this.label6.Text = "Check Branch :";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(13, 126);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(72, 13);
            this.label5.TabIndex = 8;
            this.label5.Text = "Check Bank :";
            // 
            // checkDateLabel
            // 
            this.checkDateLabel.AutoSize = true;
            this.checkDateLabel.Location = new System.Drawing.Point(15, 100);
            this.checkDateLabel.Name = "checkDateLabel";
            this.checkDateLabel.Size = new System.Drawing.Size(70, 13);
            this.checkDateLabel.TabIndex = 7;
            this.checkDateLabel.Text = "Check Date :";
            // 
            // checkDatePicker
            // 
            this.checkDatePicker.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.checkDatePicker.Location = new System.Drawing.Point(91, 97);
            this.checkDatePicker.Name = "checkDatePicker";
            this.checkDatePicker.Size = new System.Drawing.Size(172, 20);
            this.checkDatePicker.TabIndex = 6;
            this.checkDatePicker.Value = new System.DateTime(2017, 3, 20, 1, 14, 23, 0);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 74);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(84, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Check Number :";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 48);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(82, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Amount Payed :";
            // 
            // checkNumberTextbox
            // 
            this.checkNumberTextbox.Location = new System.Drawing.Point(91, 71);
            this.checkNumberTextbox.Name = "checkNumberTextbox";
            this.checkNumberTextbox.Size = new System.Drawing.Size(172, 20);
            this.checkNumberTextbox.TabIndex = 3;
            // 
            // amountPayedTextbox
            // 
            this.amountPayedTextbox.Location = new System.Drawing.Point(91, 45);
            this.amountPayedTextbox.Name = "amountPayedTextbox";
            this.amountPayedTextbox.Size = new System.Drawing.Size(172, 20);
            this.amountPayedTextbox.TabIndex = 2;
            // 
            // checkRadioBtn
            // 
            this.checkRadioBtn.AutoSize = true;
            this.checkRadioBtn.Location = new System.Drawing.Point(99, 20);
            this.checkRadioBtn.Name = "checkRadioBtn";
            this.checkRadioBtn.Size = new System.Drawing.Size(56, 17);
            this.checkRadioBtn.TabIndex = 1;
            this.checkRadioBtn.TabStop = true;
            this.checkRadioBtn.Text = "Check";
            this.checkRadioBtn.UseVisualStyleBackColor = true;
            // 
            // cashRadioBtn
            // 
            this.cashRadioBtn.AutoSize = true;
            this.cashRadioBtn.Location = new System.Drawing.Point(7, 20);
            this.cashRadioBtn.Name = "cashRadioBtn";
            this.cashRadioBtn.Size = new System.Drawing.Size(49, 17);
            this.cashRadioBtn.TabIndex = 0;
            this.cashRadioBtn.TabStop = true;
            this.cashRadioBtn.Text = "Cash";
            this.cashRadioBtn.UseVisualStyleBackColor = true;
            // 
            // confirmBtn
            // 
            this.confirmBtn.Location = new System.Drawing.Point(103, 259);
            this.confirmBtn.Name = "confirmBtn";
            this.confirmBtn.Size = new System.Drawing.Size(75, 23);
            this.confirmBtn.TabIndex = 2;
            this.confirmBtn.Text = "Confirm";
            this.confirmBtn.UseVisualStyleBackColor = true;
            this.confirmBtn.Click += new System.EventHandler(this.confirmBtn_Click);
            // 
            // EditPaymentView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(293, 294);
            this.Controls.Add(this.confirmBtn);
            this.Controls.Add(this.paymentDetailsGroupBox);
            this.Name = "EditPaymentView";
            this.Text = "EditPaymentView";
            this.paymentDetailsGroupBox.ResumeLayout(false);
            this.paymentDetailsGroupBox.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox paymentDetailsGroupBox;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox remarkTextbox;
        private System.Windows.Forms.TextBox checkBranchTextbox;
        private System.Windows.Forms.TextBox checkBankTextbox;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label checkDateLabel;
        private System.Windows.Forms.DateTimePicker checkDatePicker;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox checkNumberTextbox;
        private System.Windows.Forms.TextBox amountPayedTextbox;
        private System.Windows.Forms.RadioButton checkRadioBtn;
        private System.Windows.Forms.RadioButton cashRadioBtn;
        private System.Windows.Forms.Button confirmBtn;
    }
}