﻿using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Woodworks.Models;
using Woodworks.Network;

namespace Woodworks.Forms
{
    public partial class PaymentView : Form
    {
        public RestClient client;
        public List<Customer> customers;
        public Customer selectedCustomer;
        public List<Transaction> transactions;
        public float total_amount;
        public PaymentView()
        {
            InitializeComponent();
            total_amount = 0;
            updateTotalAmount();
            client = WoodworksClient.Instance;

            var request = new RestRequest("customers/simple", Method.GET);
            //request.AddParameter("application/json", credentials, ParameterType.RequestBody);

            IRestResponse response = client.Execute(request);
            String content = response.Content;
            customers = JsonConvert.DeserializeObject<List<Customer>>(content);
            customerListComboBox.DataSource = customers;
            customerListComboBox.DisplayMember = "customer_name";
        }

        private void confirmBtn_Click(object sender, EventArgs e)
        {
            Payment payment = new Payment();
            payment.customer_id = selectedCustomer.id;
            
            if(cashRadioBtn.Checked)
            {
                payment.payment_type = "cash";
            }
            else
            {
                payment.payment_type = "check";
                payment.check_number = checkNumberTextbox.Text;
                payment.check_date = checkDatePicker.Value;
                payment.check_bank = checkBankTextbox.Text;
                payment.check_branch = checkBranchTextbox.Text;
            }
            payment.amount_paid = float.Parse(amountPayedTextbox.Text);
            payment.remarks = remarkTextbox.Text;
            payment.transaction_ids = new List<int>();
            var transactions = getSelectedTransactions();
            foreach(Transaction transaction in transactions)
            {
                payment.transaction_ids.Add(transaction.id);
            }

            var paymentjson = JsonConvert.SerializeObject(payment);
            var request = new RestRequest("payments", Method.POST);
            request.AddParameter("application/json", paymentjson, ParameterType.RequestBody);

            IRestResponse response = client.Execute(request);
            if (response.StatusCode.ToString().Equals("OK"))
            {
                MessageBox.Show("Payment successful", "Success", MessageBoxButtons.OK, MessageBoxIcon.None);
                this.Close();
            }
            else
            {
                MessageBox.Show("Something went wrong. Please try again.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void cashRadioBtn_CheckedChanged(object sender, EventArgs e)
        {
            checkNumberTextbox.Enabled = false;
            checkDatePicker.Enabled = false;
            checkBankTextbox.Enabled = false;
            checkBranchTextbox.Enabled = false;
        }

        private void checkRadioBtn_CheckedChanged(object sender, EventArgs e)
        {
            checkNumberTextbox.Enabled = true;
            checkDatePicker.Enabled = true;
            checkBankTextbox.Enabled = true;
            checkBranchTextbox.Enabled = true;
        }

        private void customerListComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            selectedCustomer = (Customer) customerListComboBox.SelectedItem;
            var request = new RestRequest("/customers/" + selectedCustomer.id + "/transactions", Method.GET);

            IRestResponse response = client.Execute(request);
            String content = response.Content;
            transactions = JsonConvert.DeserializeObject<List<Transaction>>(content);
            transactionListBox.DataSource = transactions;
            transactionListBox.DisplayMember = "transaction_name";
        }

        private void transactionListBox_ItemCheck(object sender, ItemCheckEventArgs e)
        {
            this.BeginInvoke((MethodInvoker)delegate {
                getSelectedTransactions();
            });
        }

        private List<Transaction> getSelectedTransactions()
        {
            List<Transaction> selectedTransactions = new List<Transaction>();
            var transactions = transactionListBox.CheckedItems;
            float totalamt = 0;
            foreach (Transaction transaction in transactions)
            {
                selectedTransactions.Add(transaction);
                totalamt = totalamt + transaction.getBalance();
            }

            if (totalamt != total_amount)
            {
                total_amount = totalamt;
                updateTotalAmount();
            }

            return selectedTransactions;
        }

        private void updateTotalAmount()
        {
            totalAmountTextbox.Text = total_amount.ToString();
        }
    }
}
