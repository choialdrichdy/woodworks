﻿namespace Woodworks.Forms
{
    partial class CustomerDiscountView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.profileNameLabel = new System.Windows.Forms.Label();
            this.profileNameTextbox = new System.Windows.Forms.TextBox();
            this.customerListBox = new System.Windows.Forms.CheckedListBox();
            this.saveBtn = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.profileNameTextbox);
            this.groupBox1.Controls.Add(this.profileNameLabel);
            this.groupBox1.Location = new System.Drawing.Point(13, 13);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(259, 105);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            // 
            // profileNameLabel
            // 
            this.profileNameLabel.AutoSize = true;
            this.profileNameLabel.Location = new System.Drawing.Point(7, 32);
            this.profileNameLabel.Name = "profileNameLabel";
            this.profileNameLabel.Size = new System.Drawing.Size(71, 13);
            this.profileNameLabel.TabIndex = 0;
            this.profileNameLabel.Text = "Profile name :";
            // 
            // profileNameTextbox
            // 
            this.profileNameTextbox.Location = new System.Drawing.Point(7, 49);
            this.profileNameTextbox.Name = "profileNameTextbox";
            this.profileNameTextbox.Size = new System.Drawing.Size(246, 20);
            this.profileNameTextbox.TabIndex = 1;
            // 
            // customerListBox
            // 
            this.customerListBox.FormattingEnabled = true;
            this.customerListBox.Location = new System.Drawing.Point(13, 125);
            this.customerListBox.Name = "customerListBox";
            this.customerListBox.Size = new System.Drawing.Size(259, 379);
            this.customerListBox.TabIndex = 1;
            // 
            // saveBtn
            // 
            this.saveBtn.Location = new System.Drawing.Point(102, 511);
            this.saveBtn.Name = "saveBtn";
            this.saveBtn.Size = new System.Drawing.Size(75, 23);
            this.saveBtn.TabIndex = 2;
            this.saveBtn.Text = "Save";
            this.saveBtn.UseVisualStyleBackColor = true;
            this.saveBtn.Click += new System.EventHandler(this.saveBtn_Click);
            // 
            // CustomerDiscountView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 545);
            this.Controls.Add(this.saveBtn);
            this.Controls.Add(this.customerListBox);
            this.Controls.Add(this.groupBox1);
            this.Name = "CustomerDiscountView";
            this.Text = "CustomerDiscountView";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox profileNameTextbox;
        private System.Windows.Forms.Label profileNameLabel;
        private System.Windows.Forms.CheckedListBox customerListBox;
        private System.Windows.Forms.Button saveBtn;
    }
}