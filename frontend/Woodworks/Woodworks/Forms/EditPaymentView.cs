﻿using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Woodworks.Models;
using Woodworks.Models.DGVModels;
using Woodworks.Network;

namespace Woodworks.Forms
{
    public partial class EditPaymentView : Form
    {
        public RestClient client;
        Payment payment;

        public EditPaymentView()
        {
            InitializeComponent();
        }

        public EditPaymentView(PaymentTable payment)
        {
            InitializeComponent();
            client = WoodworksClient.Instance;
            this.payment = new Payment();
            this.payment.id = payment.id;
            this.payment.payment_type = payment.payment_type;
            this.payment.remarks = payment.remarks;
            this.payment.amount_paid = payment.amount_paid;
            this.payment.check_bank = payment.check_bank;
            this.payment.check_branch = payment.check_branch;
            this.payment.check_number = payment.check_number;
            this.payment.check_date = payment.check_date;

            if (this.payment.payment_type == "check")
            {
                checkDatePicker.Value = this.payment.check_date;
                checkNumberTextbox.Text = this.payment.check_number;
                checkBankTextbox.Text = this.payment.check_bank;
                checkBranchTextbox.Text = this.payment.check_branch;
                checkRadioBtn.Checked = true;
            }
            else
            {
                cashRadioBtn.Checked = true;
                checkDatePicker.Enabled = false;
                checkNumberTextbox.Enabled = false;
                checkBankTextbox.Enabled = false;
                checkBranchTextbox.Enabled = false;
            }
            cashRadioBtn.Enabled = false;
            checkRadioBtn.Enabled = false;
            remarkTextbox.Text = this.payment.remarks;
            amountPayedTextbox.Text = this.payment.amount_paid.ToString();

        }

        private void confirmBtn_Click(object sender, EventArgs e)
        {   
            if (cashRadioBtn.Checked)
            {
                payment.payment_type = "cash";
            }
            else
            {
                payment.payment_type = "check";
                payment.check_number = checkNumberTextbox.Text;
                payment.check_date = checkDatePicker.Value;
                payment.check_bank = checkBankTextbox.Text;
                payment.check_branch = checkBranchTextbox.Text;
            }
            payment.amount_paid = float.Parse(amountPayedTextbox.Text);
            payment.remarks = remarkTextbox.Text;

            var paymentjson = JsonConvert.SerializeObject(payment);
            var request = new RestRequest("payments/" + payment.id, Method.PUT);
            request.AddParameter("application/json", paymentjson, ParameterType.RequestBody);

            IRestResponse response = client.Execute(request);
            if (response.StatusCode.ToString().Equals("OK"))
            {
                MessageBox.Show("Edit successful", "Success", MessageBoxButtons.OK, MessageBoxIcon.None);
                this.Close();
            }
            else
            {
                MessageBox.Show("Something went wrong. Please try again.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
