﻿using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Woodworks.Models;
using Woodworks.Network;

namespace Woodworks.Forms
{
    public partial class UserView : Form
    {
        private String action;
        private User user;

        public UserView()
        {
            this.action = "Add";
            InitializeComponent();
        }

        public UserView(String action, User user)
        {
            this.action = action;
            InitializeComponent();
            if (this.action.Equals("Edit"))
            {
                this.user = user;
                fnTextbox.Text = user.first_name;
                lnTextbox.Text = user.last_name;
                unTextbox.Text = user.username;
                unTextbox.Enabled = false;
                pwTextbox.Enabled = false;
            }

        }

        private void confirmBtn_Click(object sender, EventArgs e)
        {
            RestClient client = WoodworksClient.Instance;
            string userjson = "";

            if (this.action.Equals("Add"))
            {
                user = new User();
                user.first_name = fnTextbox.Text;
                user.last_name = lnTextbox.Text;
                user.username = unTextbox.Text;
                user.password = pwTextbox.Text;

                userjson = JsonConvert.SerializeObject(user);
                var request = new RestRequest("users", Method.POST);
                request.AddParameter("application/json", userjson, ParameterType.RequestBody);

                IRestResponse response = client.Execute(request);
                if (response.StatusCode.ToString().Equals("OK"))
                {
                    MessageBox.Show("Add successful", "Success", MessageBoxButtons.OK, MessageBoxIcon.None);
                    this.Close();
                }
                else
                {
                    MessageBox.Show("Something went wrong. Please try again.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
           else if (this.action.Equals("Edit"))
            {
                user.first_name = fnTextbox.Text;
                user.last_name = lnTextbox.Text;
                userjson = JsonConvert.SerializeObject(user);
                var request = new RestRequest("user/" + user.id, Method.PUT);
                request.AddParameter("application/json", userjson, ParameterType.RequestBody);

                IRestResponse response = client.Execute(request);
                if (response.StatusCode.ToString().Equals("OK"))
                {
                    MessageBox.Show("Edit successful", "Success", MessageBoxButtons.OK, MessageBoxIcon.None);
                    this.Close();
                }
                else
                {
                    MessageBox.Show("Something went wrong. Please try again.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
    }
}
