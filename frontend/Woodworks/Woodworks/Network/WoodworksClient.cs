﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Woodworks.Network
{
    class WoodworksClient
    {
        private static WoodworksClient instance;
        private RestClient client;

        private WoodworksClient()
        {

            client = new RestClient();
            client.BaseUrl = new Uri("http://128.199.68.245/");
            client.CookieContainer = new System.Net.CookieContainer();
        }

        public static RestClient Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new WoodworksClient();
                }
                return instance.client;
            }
        }
    }
}
