﻿using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Woodworks.Models;
using Woodworks.Models.DGVModels;

namespace Woodworks.Services
{
    public static class Services
    {
        public static List<WoodDiscount> formatWoodDiscount(List<WoodDiscount> wooddiscounts)
        {
            foreach(WoodDiscount wooddiscount in wooddiscounts)
            {
                wooddiscount.wood_thickness = doubleToFraction(double.Parse(wooddiscount.wood_thickness));
                wooddiscount.wood_width = doubleToFraction(double.Parse(wooddiscount.wood_width));
                wooddiscount.wood_height = doubleToFraction(double.Parse(wooddiscount.wood_height));
            }
            return wooddiscounts;
        }

        public static List<Order> formatOrder(List<Order> orders)
        {
            foreach(Order order in orders)
            {
                order.wood_type = order.wood.wood_type;
                order.wood_thickness = doubleToFraction(order.wood.wood_thickness);
                order.wood_width = doubleToFraction(order.wood.wood_width);
                order.wood_height = doubleToFraction(order.wood.wood_height);
            }
            return orders;
        }

        public static int gcf(int a, int b)
        {
            while (b != 0) 
            {
                int t = b;
                b = a % b;
                a = t;
            }
            return a;
        }

        public static int getDenominator(int numerator)
        {
            return (int)Math.Pow(10f, numerator.ToString().Length);
        }

        public static string doubleToFraction(double number)
        {
            if (number == 0)
            {
                return "0";
            }
            string fraction;
            string wholeNumber = "";
            int decimalNumber = 0;
            bool isDecimal = false;
            var temp = number.ToString();
            for (int i = 0; i < temp.Length; i++)
            {
               if (temp[i] == '.')
                {
                    isDecimal = true;
                }
               if (temp[i] != '.')
                {
                    if (isDecimal)
                    {
                        int a = (int) temp[i] - '0';
                        if (decimalNumber == 0)
                        {
                            decimalNumber = a;
                        }
                        else
                        {
                            decimalNumber = (decimalNumber * 10) + a;
                        }
                    }
                    else
                    {
                        wholeNumber += temp[i];
                    }
                }
            }

            int denominator = getDenominator(decimalNumber);
            int gcfactor = gcf(decimalNumber, denominator);

            decimalNumber /= gcfactor;
            denominator /= gcfactor;
            if (int.Parse(wholeNumber) > 0 && decimalNumber > 0 && denominator > 1)
            {
                fraction = wholeNumber + " " + decimalNumber + "/" + denominator;
            }
            else if (int.Parse(wholeNumber) > 0 && decimalNumber == 0 && denominator == 1)
            {
                return wholeNumber;
            }
            else
            {
                fraction = decimalNumber + "/" + denominator;
            }
            return fraction;
        }

        public static double fractionToDouble(string fraction)
        {
            double result;
            if(double.TryParse(fraction, out result))
            {
                return result;
            }

            string[] split = fraction.Split(new char[] { ' ', '/' });

            if (split.Length == 2 || split.Length == 3)
            {
                int a, b;

                if (int.TryParse(split[0], out a) && int.TryParse(split[1], out b))
                {
                    if (split.Length == 2)
                    {
                        return (double)a / b;
                    }

                    int c;

                    if (int.TryParse(split[2], out c))
                    {
                        return a + (double)b / c;
                    }
                }
            }

            throw new FormatException("Not a valid fraction.");
        }

        public static int computeBFLF(Order order)
        {
            if (order.quantity == 0)
                return 0;
            if (order.wood_type.Equals("KD TNG") || order.wood_type.Equals("KD S-Cut") || order.wood_type.Equals("KD V-Cut"))
            {
                return (int)Math.Round(order.wood.wood_width * order.wood.wood_height * order.quantity / 12);
            }
            if (order.wood_type.Equals("KD CROWN") || order.wood_type.Equals("KD CENTER") || order.wood_type.Equals("KD BB 41") 
                || order.wood_type.Equals("KD BB 42") || order.wood_type.Equals("KD Dowell")
                || order.wood_type.Equals("KD Cor A") || order.wood_type.Equals("KD Cor B") 
                || order.wood_type.Equals("KD Cas A") || order.wood_type.Equals("KD Cas B"))
            {
                return (int)Math.Round(order.wood.wood_thickness * order.wood.wood_height * order.quantity);
            }
            if (order.wood.wood_thickness < 1 && order.wood.wood_width < 2)
            {
                return (int)Math.Round(order.wood.wood_width * order.wood.wood_height * order.quantity);
            }
            if (order.wood.wood_thickness >= 1 && order.wood.wood_width > 1)
            {
                return (int)Math.Round(order.wood.wood_thickness * order.wood.wood_width * order.wood.wood_height * order.quantity / 12);
            }
            return (int)Math.Round(order.wood.wood_thickness * order.wood.wood_width * order.wood.wood_height * order.quantity);
        }

        public static void exportPayments(List<PaymentTable> payments)
        {
            SaveFileDialog a = new SaveFileDialog();
            a.Filter = "Excel File|*.xlsx";
            a.Title = "Save an Excel File";
            DialogResult b = a.ShowDialog();
            if(b == DialogResult.Cancel)
            {
                return;
            }
            XSSFWorkbook workbook = new XSSFWorkbook();
            XSSFSheet sheet = (XSSFSheet)workbook.CreateSheet("Sheet1");
            IRow row1 = sheet.CreateRow(0);
            ICell header1 = row1.CreateCell(0);
            header1.SetCellValue("ID");
            ICell header2 = row1.CreateCell(1);
            header2.SetCellValue("Customer Name");
            ICell header3 = row1.CreateCell(2);
            header3.SetCellValue("Amount Paid");
            ICell header4 = row1.CreateCell(3);
            header4.SetCellValue("Remarks");
            ICell header5 = row1.CreateCell(4);
            header5.SetCellValue("Type");
            ICell header6 = row1.CreateCell(5);
            header6.SetCellValue("Check Date");
            ICell header7 = row1.CreateCell(6);
            header7.SetCellValue("Check Bank");
            ICell header8 = row1.CreateCell(7);
            header8.SetCellValue("Check Branch");
            ICell header9 = row1.CreateCell(8);
            header9.SetCellValue("Check Number");
            int rownumber = 1;
            foreach (PaymentTable onepayment in payments)
            {
                IRow payment = sheet.CreateRow(rownumber);
                payment.CreateCell(0).SetCellValue(onepayment.id);
                payment.CreateCell(1).SetCellValue(onepayment.customer_name);
                payment.CreateCell(2).SetCellValue(onepayment.amount_paid);
                payment.CreateCell(3).SetCellValue(onepayment.remarks);
                payment.CreateCell(4).SetCellValue(onepayment.payment_type);
                payment.CreateCell(5).SetCellValue(onepayment.check_date);
                payment.CreateCell(6).SetCellValue(onepayment.check_bank);
                payment.CreateCell(7).SetCellValue(onepayment.check_branch);
                payment.CreateCell(8).SetCellValue(onepayment.check_number);
                rownumber++;
            }

            FileStream fs = new FileStream(a.FileName, FileMode.Create, FileAccess.Write);
            workbook.Write(fs);
        }

        public static void exportCustomers(List<Customer> customers)
        {
            SaveFileDialog a = new SaveFileDialog();
            a.Filter = "Excel File|*.xlsx";
            a.Title = "Save an Excel File";
            DialogResult b = a.ShowDialog();
            if (b == DialogResult.Cancel)
            {
                return;
            }
            XSSFWorkbook workbook = new XSSFWorkbook();
            XSSFSheet sheet = (XSSFSheet)workbook.CreateSheet("Sheet1");
            IRow row1 = sheet.CreateRow(0);
            ICell header1 = row1.CreateCell(0);
            header1.SetCellValue("Customer ID");
            ICell header2 = row1.CreateCell(1);
            header2.SetCellValue("Customer Name");
            ICell header3 = row1.CreateCell(2);
            header3.SetCellValue("Current Balance");
            ICell header4 = row1.CreateCell(3);
            header4.SetCellValue("PDC");
            int rownumber = 1;
            foreach (Customer onecustomer in customers)
            {
                IRow customer = sheet.CreateRow(rownumber);
                customer.CreateCell(0).SetCellValue(onecustomer.id);
                customer.CreateCell(1).SetCellValue(onecustomer.customer_name);
                customer.CreateCell(2).SetCellValue(onecustomer.current_balance);
                customer.CreateCell(3).SetCellValue(onecustomer.pdc);
                rownumber++;
            }

            FileStream fs = new FileStream(a.FileName, FileMode.Create, FileAccess.Write);
            workbook.Write(fs);
        }

        public static void exportTransaction(TransactionExport onetransaction)
        {
            SaveFileDialog a = new SaveFileDialog();
            a.Filter = "Excel File|*.xlsx";
            a.Title = "Save an Excel File";
            DialogResult b = a.ShowDialog();
            if (b == DialogResult.Cancel)
            {
                return;
            }
            XSSFWorkbook workbook = new XSSFWorkbook();
            ICellStyle centerStyle = workbook.CreateCellStyle();
            centerStyle.Alignment = NPOI.SS.UserModel.HorizontalAlignment.Center;
            XSSFSheet sheet = (XSSFSheet)workbook.CreateSheet("Sheet1");
            sheet.AddMergedRegion(new NPOI.SS.Util.CellRangeAddress(5, 5, 2, 4));
            sheet.AddMergedRegion(new NPOI.SS.Util.CellRangeAddress(0, 1, 1, 4));
            sheet.AddMergedRegion(new NPOI.SS.Util.CellRangeAddress(2, 2, 1, 4));
            sheet.AddMergedRegion(new NPOI.SS.Util.CellRangeAddress(3, 3, 1, 4));
            sheet.AddMergedRegion(new NPOI.SS.Util.CellRangeAddress(4, 4, 1, 4));
            IRow row1 = sheet.CreateRow(0);
            row1.RowStyle = centerStyle;
            row1.CreateCell(1).SetCellValue("B88");
            sheet.CreateRow(1);
            IRow infoRow = sheet.CreateRow(3);
            infoRow.RowStyle = centerStyle;
            infoRow.CreateCell(0).SetCellValue("Customer:");
            infoRow.CreateCell(1).SetCellValue(onetransaction.customer);
            infoRow.CreateCell(5).SetCellValue("DR No.");
            infoRow.CreateCell(6).SetCellValue(onetransaction.dr_no);
            IRow infoRow2 = sheet.CreateRow(4);
            infoRow2.RowStyle = centerStyle;
            infoRow2.CreateCell(0).SetCellValue("Address:");
            infoRow2.CreateCell(1).SetCellValue(onetransaction.address);
            infoRow2.CreateCell(5).SetCellValue("Date");
            infoRow2.CreateCell(6).SetCellValue(onetransaction.date);
            IRow headers = sheet.CreateRow(5);
            ICellStyle headerStyle = workbook.CreateCellStyle();
            headerStyle.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thick;
            headerStyle.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thick;
            headerStyle.BorderRight = NPOI.SS.UserModel.BorderStyle.Thick;
            headerStyle.BorderTop = NPOI.SS.UserModel.BorderStyle.Thick;
            headerStyle.Alignment = NPOI.SS.UserModel.HorizontalAlignment.Center;
            //headers.RowStyle = headerStyle;
            ICell header1 = headers.CreateCell(0);
            header1.SetCellValue("QTY");
            ICell header2 = headers.CreateCell(1);
            header2.SetCellValue("Description");
            ICell header3 = headers.CreateCell(2);
            header3.SetCellValue("Dimension");
            ICell header4 = headers.CreateCell(5);
            header4.SetCellValue("BF/LF");
            ICell header5 = headers.CreateCell(6);
            header5.SetCellValue("Price");
            ICell header6 = headers.CreateCell(7);
            header6.SetCellValue("Less");
            ICell header7 = headers.CreateCell(8);
            header7.SetCellValue("TOTAL AMOUNT");
            header1.CellStyle = headerStyle;
            header2.CellStyle = headerStyle;
            header3.CellStyle = headerStyle;
            header4.CellStyle = headerStyle;
            header5.CellStyle = headerStyle;
            header6.CellStyle = headerStyle;
            header7.CellStyle = headerStyle;
            int rownumber = 6;
            foreach (DetailExport onedetail in onetransaction.details)
            {
                IRow detail = sheet.CreateRow(rownumber);
                detail.RowStyle = centerStyle;
                detail.CreateCell(0).SetCellValue(onedetail.qty);
                detail.CreateCell(1).SetCellValue(onedetail.description);
                detail.CreateCell(2).SetCellValue(doubleToFraction(onedetail.thickness));
                detail.CreateCell(3).SetCellValue(doubleToFraction(onedetail.width));
                detail.CreateCell(4).SetCellValue(doubleToFraction(onedetail.height));
                detail.CreateCell(5).SetCellValue(onedetail.bf_lf);
                detail.CreateCell(6).SetCellValue(onedetail.price);
                detail.CreateCell(7).SetCellValue(onedetail.less);
                detail.CreateCell(8).SetCellValue(onedetail.total_amount);
                rownumber++;
            }

            IRow totalrow = sheet.CreateRow(rownumber);
            totalrow.RowStyle = centerStyle;
            totalrow.CreateCell(8).SetCellValue("PHP " + onetransaction.total);
            FileStream fs = new FileStream(a.FileName, FileMode.Create, FileAccess.Write);
            workbook.Write(fs);
        }
    }
}
