from setuptools import setup, find_packages

requires = [
]

setup(name='woodworks',
      version='0.1',
      install_requires=requires,
      description='Woodworks',
      url='',
      author='Aldrich Choi',
      author_email='',
      packages=find_packages(),
      entry_points="""\
      [paste.app_factory]
      main = woodworks.wsgi:main
      """,
      zip_safe=False,
      )