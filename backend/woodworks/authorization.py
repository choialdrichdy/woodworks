from pyramid.security import Everyone, ALL_PERMISSIONS, Allow, Deny, Authenticated

from woodworks import db
from woodworks.models import User


class Root(object):
	__acl__ = [
		(Allow, Everyone, 'public'),
		(Allow, Authenticated, 'auth'),

		(Deny, Everyone, ALL_PERMISSIONS)
	]

	def __init__(self, request):
		pass

def get_principals(uid, request):
	user = db.get_session().query(User).get(uid)
	if user is None:
		return None
	else:
		return user.user_permissions