from passlib.hash import pbkdf2_sha256
from pyramid.security import authenticated_userid
from pyramid.httpexceptions import HTTPUnprocessableEntity, HTTPNotFound, HTTPUnauthorized

from woodworks import db
from woodworks.models import User


def hash_password(pw):
    hash = pbkdf2_sha256.encrypt(pw)
    return hash

def check_password(pw, hashed_pw):
    return pbkdf2_sha256.verify(pw, hashed_pw)

def get_current_user(request):
    uid = authenticated_userid(request)
    if uid is None:
        return None
    data = db.get_session().query(User).get(uid)
    if data is None:
        forget(request)
        return None
    return data

def authenticate(request):
    params = request.json_body
    username = params.get('username', None)
    password = params.get('password', None)

    if username is '' or password is '':
        errors = dict()
        if username is '':
            errors['username'] = "Username is required"
        if password is '':
            errors['password'] = "Password is required"
        raise HTTPUnprocessableEntity(errors)
    session = db.get_session()
    user = session.query(User).filter(User.username == username, User.is_active).first()
    if user is None:
        raise HTTPNotFound("test")

    if not check_password(password, user.password):
        raise HTTPUnauthorized({'password': ['Your email or password is invalid']})

    return user