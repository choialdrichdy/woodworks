from wsgiref.simple_server import make_server
from pyramid.config import Configurator
from pyramid.response import Response
from pyramid.authentication import AuthTktAuthenticationPolicy
from pyramid.authorization import ACLAuthorizationPolicy
from pyramid.security import DENY_ALL

from woodworks.authorization import get_principals, Root


def main(global_config, **settings):
	config = Configurator(settings=settings)

	# Security policies
	authn_policy = AuthTktAuthenticationPolicy(
        settings['woodworks.secret'], callback=get_principals,
        hashalg='sha512')
	authz_policy = ACLAuthorizationPolicy()
	config.set_authentication_policy(authn_policy)
	config.set_authorization_policy(authz_policy)
	# config.set_default_permission(DENY_ALL)
	config.set_root_factory(Root)

	config.add_route('login', '/login')

	config.add_route('woods', '/woods')
	config.add_route('woods_excel', '/woods/excel')
	config.add_route('wood', '/woods/{id}')
	config.add_route('wood_stock', '/woods/{id}/stock')

	config.add_route('customers', '/customers')
	config.add_route('customers_simple', '/customers/simple')
	config.add_route('customers_excel', '/customers/excel')
	config.add_route('customer', '/customers/{id}')
	config.add_route('customer_woods', '/customers/{id}/woods')
	config.add_route('customer_transactions', '/customers/{id}/transactions')
	config.add_route('customer_transaction', '/customers/{customer_id}/transactions/{transaction_id}')
	config.add_route('transaction_details', '/customers/{customer_id}/transactions/{transaction_id}/details')
	config.add_route('customer_discounts', '/customers/{id}/discounts')
	config.add_route('customer_special_price', '/customers/{id}/specialprice')
	
	config.add_route('transactions', '/transactions')
	config.add_route('transaction_export', '/transactions/{id}/export')
	config.add_route('excel_wood_stock', '/woods/stocks/excel')
	# config.add_route('transaction', '/transactions/{id}')

	# config.add_route('discount_profiles', '/discounts')
	# config.add_route('discount_profile', '/discounts/{id}')

	config.add_route('users', '/users')
	config.add_route('user', '/user/{id}')
	config.add_route('change_permission', '/user/{id}/permissions')
	config.add_route('change_passwords', '/users/changepw')

	config.add_route('payments', '/payments')
	config.add_route('payment', '/payments/{id}')

	config.add_route('discounts', '/discounts')
	config.add_route('special_prices', '/specialprices/{id}')
	config.add_route('rules', '/rules')
	config.add_route('rules_customer', '/rules/{id}/customers')
	config.scan('woodworks.views')
	config.include('pyramid_excel')
	# config.add_view(hello_world, route_name='hello')
	return config.make_wsgi_app()