from pyramid.view import view_config
from pyramid.response import Response
from pyramid.security import remember, forget, authenticated_userid
from pyramid.httpexceptions import HTTPUnprocessableEntity

from woodworks import db, security
from woodworks.security import authenticate, get_current_user, check_password, hash_password
from woodworks.models import User
from woodworks.services.encoding import ModelDictEncoder, ModelJSONEncoder

import logging
log = logging.getLogger(__name__)


@view_config(route_name='users', request_method='POST', renderer='json', permission='auth')
def post_users(request):
    session = db.get_session()
    params = request.json_body
    user = User(first_name=params.get('first_name'), last_name=params.get('last_name'), username=params.get('username'), password=hash_password(params.get('password')))
    session.add(user)
    session.commit()
    return Response()


@view_config(route_name='users', request_method='GET', renderer='json', permission='auth')
def get_users(request):
    session = db.get_session()
    users = session.query(User).filter(User.is_active).all()

    return ModelDictEncoder({User: {'exclude': ['password']}}).encode(users)


@view_config(route_name='user', request_method='PUT', renderer='json', permission='auth')
def put_user(request):
    session = db.get_session()
    id = request.matchdict['id']
    params = request.json_body
    user = session.query(User).get(id)
    print(user)
    user.first_name = params.get('first_name')
    user.last_name = params.get('last_name')
    session.commit()
    return Response()


@view_config(route_name='user', request_method='DELETE', renderer='json', permission='auth')
def delete_user(request):
    session = db.get_session()
    id = request.matchdict['id']
    user = session.query(User).get(id)
    user.is_active = False
    session.commit()
    return Response()


@view_config(route_name='change_passwords', request_method='POST', renderer='json', permission='auth')
def post_change_password(request):
    session = db.get_session()
    params = request.json_body
    currentuser = get_current_user(request)
    user = session.query(User).filter(User.username == currentuser.username, User.is_active).first()
    if check_password(params.get('old_password'), user.password):
        user.password = hash_password(params.get('new_password'))
        session.commit()
        return Response('Change successful')
    else:
        return Response('Old password does not match.')

@view_config(route_name='change_permission', request_method='PUT', renderer='json', permission='auth')
def put_user(request):
    session = db.get_session()
    id = request.matchdict['id']
    params = request.json_body
    user = session.query(User).get(id)
    print(user)
    user.user_permissions = params
    session.commit()
    return Response("Edit successful")