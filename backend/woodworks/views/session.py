from pyramid.view import view_config
from pyramid.response import Response
from pyramid.security import remember, forget, authenticated_userid
from pyramid.httpexceptions import HTTPUnprocessableEntity

from woodworks import db, security
from woodworks.security import authenticate, get_current_user
from woodworks.models import User, LoginLog
from woodworks.services.encoding import ModelJSONEncoder, ModelDictEncoder


@view_config(route_name='login', request_method='GET', renderer='json', permission='auth')
def get_session(request):
    """
    Returns the current authenticated user if any, otherwise
    raises a 403.
    """
    _ = request.localizer.translate
    user = get_current_user(request)
    if user is None:
        # Either not authenticated with provider, or user ID given
        # by provider doesn't exist in DB. If latter, we should clear
        # out credentials given by provider so as not to confuse rest of
        # site.
        headers = forget(request)
        return Response('You are not yet logged in.', status=401, headers=headers)
    return ModelDictEncoder().encode(user)

@view_config(route_name='login', request_method='POST', renderer='json')
def login_user(request):
	if authenticated_userid(request) is not None:
		user = get_current_user(request)
		if user is not None:
			raise HTTPUnprocessableEntity('You are already logged in.')

	user = authenticate(request)
	if user is not None:
		session = db.get_session()
		session.add(LoginLog(user_id=user.id))
		session.commit()
		headers = remember(request, user.id)
		encoded_user = ModelDictEncoder().encode(user)
		return Response(headers=headers, body=ModelJSONEncoder().encode(encoded_user))