from pyramid.view import view_config
from pyramid.response import Response

from woodworks import db
from woodworks.security import get_current_user
from woodworks.models import Transaction, Payment, CashPayment, CheckPayment, PaymentDetail
from woodworks.services.encoding import ModelDictEncoder, ModelJSONEncoder


@view_config(route_name='payments', request_method='POST', renderer='json', permission='auth')
def post_payments(request):
	session = db.get_session()
	# TODO: exception handling
	params = request.json_body
	customer_id = params.get('customer_id')
	payment_type = params.get('payment_type')
	amount_paid = params.get('amount_paid')
	transaction_ids = params.get('transaction_ids')
	check_number = params.get('check_number', None)
	check_date = params.get('check_date', None)
	check_bank = params.get('check_bank', None)
	check_branch = params.get('check_branch', None)
	remarks = params.get('remarks', None)

	user =get_current_user(request)
	if payment_type == 'cash':
		payment = CashPayment(customer_id=customer_id, user_id=user.id, amount_paid=amount_paid, is_cleared=True, remarks=remarks)
	elif payment_type == 'check':
		payment = CheckPayment(customer_id=customer_id, user_id=user.id, amount_paid=amount_paid, check_number=check_number, check_date=check_date, check_bank=check_bank, check_branch=check_branch, remarks=remarks)
	session.add(payment)
	session.flush()

	remaining_amount = amount_paid
	if transaction_ids:
		transactions = session.query(Transaction).filter(Transaction.id.in_(transaction_ids)).all()
		for transaction in transactions:
			if remaining_amount == 0:
				amount = 0
			else:
				remaining_balance = transaction.total_amount - transaction.amount_paid_without_one(payment.id)
				if remaining_amount > remaining_balance:
					remaining_amount = remaining_amount - remaining_balance
					amount = remaining_balance
				else:
					amount = remaining_amount
			payment_detail = PaymentDetail(transaction_id=transaction.id, payment_id=payment.id, payment_amount=amount)
			session.add(payment_detail)
			session.flush()
	session.commit()
	return Response()

@view_config(route_name='payment', request_method='PUT', renderer='json', permission='auth')
def put_payment(request):
	session = db.get_session()
	# TODO: exception handling
	id = request.matchdict['id']
	params = request.json_body

	payment = session.query(Payment).filter(Payment.id == id).scalar()

	payment_type = params.get('payment_type')
	amount_paid = params.get('amount_paid')
	check_number = params.get('check_number', None)
	check_date = params.get('check_date', None)
	check_bank = params.get('check_bank', None)
	check_branch = params.get('check_branch', None)
	remarks = params.get('remarks', None)
	if payment is not None:
		payment.amount_paid = amount_paid
		payment.remarks = remarks
		if payment_type == 'check':
			payment.check_number = check_number
			payment.check_date = check_date
			payment.check_bank = check_bank
			payment.check_branch = check_branch
			if "is cleared" in payment.remarks:
				payment.is_cleared = True
			else:
				payment.is_cleared = False

	session.flush()

	remaining_amount = amount_paid

	payment_details = session.query(PaymentDetail).filter(PaymentDetail.payment_id == id).all()
	if payment_details:
		for detail in payment_details:
			if remaining_amount == 0:
				detail.payment_amount = 0
			else:
				remaining_balance = detail.transaction.total_amount - detail.transaction.amount_paid_without_one(id)
				if remaining_balance != 0:
					if remaining_amount > remaining_balance:
						remaining_amount = remaining_amount - remaining_balance
						detail.payment_amount = remaining_balance
					else:
						detail.payment_amount = remaining_amount
						remaining_amount = 0
			session.flush()
	session.commit()
	return Response()

@view_config(route_name='payments', request_method='GET', renderer='json', permission='auth')
def get_payments(request):
	session = db.get_session()
	# TODO: exception handling
	payments = session.query(Payment).all()

	paymentlist = []

	for payment in payments:
		paymentdict = dict()
		paymentdict['id'] = payment.id
		paymentdict['customer_name'] = payment.customer.customer_name
		paymentdict['amount_paid'] = payment.amount_paid
		paymentdict['remarks'] = payment.remarks
		paymentdict['payment_type'] = payment.payment_type
		paymentdict['payment_date'] = payment.payment_date
		if payment.payment_type == 'check':
			paymentdict['check_number'] = payment.check_number if payment.check_number else None
			paymentdict['check_bank'] = payment.check_bank if payment.check_bank else None
			paymentdict['check_branch'] = payment.check_branch if payment.check_branch else None
			paymentdict['check_date'] = payment.check_date if payment.check_date else None
		paymentlist.append(paymentdict)
	return ModelDictEncoder().encode(paymentlist)
