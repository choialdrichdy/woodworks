from pyramid.view import view_config
from pyramid.response import Response
from pyramid.httpexceptions import HTTPUnprocessableEntity

from woodworks import db
from woodworks.models import CustomerDiscount, DiscountProfile, CustomerSpecialPrice, ProfileDetail, Customer
from woodworks.services.encoding import ModelDictEncoder, ModelJSONEncoder
from woodworks.services.discount import reset_profile, reset_special_price


@view_config(route_name='discounts', request_method='GET', renderer='json', permission='auth')
def get_discounts(request):
	session = db.get_session()
	allprofiles = session.query(DiscountProfile).all()

	return ModelDictEncoder({DiscountProfile: {'expand': ['details']}, ProfileDetail: {'expand': ['wood']}}).encode(allprofiles)

@view_config(route_name='special_prices', request_method='POST', renderer='json', permission='auth')
def post_special_prices(request):
	session = db.get_session()
	prices = request.json_body
	id = request.matchdict['id']
	customer = session.query(Customer).filter(Customer.id == id).scalar()
	reset_special_price(customer, session)
	for price in prices:
		session.add(CustomerSpecialPrice(customer_id=customer.id, wood_id=price['wood_id'], special_price=price['special_price']))
	session.commit()
	return Response()

@view_config(route_name='rules', request_method='POST', renderer='json', permission='auth')
def post_rules(request):
	session = db.get_session()
	rule = request.json_body
	print(rule)
	profile_name = rule['profile_name']
	details = rule['details']
	profile = DiscountProfile(profile_name=profile_name)
	session.add(profile)
	session.flush()
	for detail in details:
		session.add(ProfileDetail(profile_id=profile.id, wood_id=detail['wood_id'], discount_percent=detail['discount_percent']))
	session.commit()
	return Response()

@view_config(route_name='rules_customer', request_method='GET', renderer='json', permission='auth')
def get_rules_customer(request):
	session = db.get_session()
	id = request.matchdict['id']
	customer_discounts = (
		session.query(CustomerDiscount)
		.filter(CustomerDiscount.is_active)
		.filter(CustomerDiscount.profile_id == id)
		.all()
	)
	return ModelDictEncoder().encode(customer_discounts)

@view_config(route_name='rules_customer', request_method='PUT', renderer='json', permission='auth')
def put_rules_customer(request):
	session = db.get_session()
	id = request.matchdict['id']
	customer_ids = request.json_body
	profile = (
		session.query(DiscountProfile).filter(DiscountProfile.id == id).scalar()
	)
	if profile is None:
		raise HTTPUnprocessableEntity("No profile found.")
	else:
		customer_discounts = reset_profile(profile, session)
		for id in customer_ids:
			exists = False
			for customer_discount in customer_discounts:
				if customer_discount.customer_id == id:
					customer_discount.is_active = True
					exists = True
					break
			if not exists:
				session.add(CustomerDiscount(profile_id=profile.id, customer_id=id, is_active=True))
		session.commit()
		return Response()
