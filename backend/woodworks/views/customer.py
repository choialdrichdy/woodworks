from pyramid.view import view_config
from pyramid.response import Response

from sqlalchemy.sql import func, and_

from woodworks import db
from woodworks.models import Customer, ProfileDetail, CustomerDiscount, DiscountProfile, Wood, CustomerSpecialPrice, Transaction
from woodworks.services.encoding import ModelDictEncoder, ModelJSONEncoder

import pyexcel

@view_config(route_name='customers', request_method='POST', renderer='json', permission='auth')
def post_customers(request):
	session = db.get_session()
	# TODO: exception handling
	params = request.json_body
	listcustomer = []
	for param in params:
		customer_name = param.get('customer_name', None)
		customer_address = param.get('customer_address', None)
		customer_terms = param.get('customer_terms', None)
		customer = Customer(customer_name=customer_name, customer_address=customer_address, customer_terms=customer_terms)
		session.add(customer)
		listcustomer.append(customer)
	session.commit()
	listcustdict = ModelDictEncoder().encode(listcustomer)
	return listcustdict

@view_config(route_name='customers_excel', request_method='POST', renderer='json', permission='auth')
def post_customers_excel(request):
	session = db.get_session()
	response = "Customers added:\n"
	filename = request.POST['filename'].filename;
	extension = filename.split(".")[-1]
	body = request.POST['filename'].file;
	content = body.read()
	# content = content.decode('utf-8')
	sheet = pyexcel.get_sheet(file_type=extension, file_content=content, name_columns_by_row=0)
	listcustomer = list(sheet.rows())
	# TODO: exception handling
	for onecustomer in listcustomer:
		customer_name = onecustomer[0]
		customer_address = onecustomer[1]
		customer_terms = onecustomer[2]
		customer = (
			session.query(Customer)
			.filter(Customer.customer_name == customer_name)
			.scalar()
		)
		if customer is not None:
			customer.customer_terms = customer_terms
			customer.is_active = True
		else:
			customer = Customer(customer_name=customer_name, customer_address=customer_address, customer_terms=customer_terms)
			session.add(customer)
			response += str(customer.customer_name + "\n")
	session.commit()
	return Response()

@view_config(route_name='customers', request_method='GET', renderer='json', permission='auth')
def get_customers(request):
	session = db.get_session()
	allcustomers = session.query(Customer).filter(Customer.is_active).all()

	return ModelDictEncoder({Customer: {'expand': ['current_balance', 'pdc']}}).encode(allcustomers)

@view_config(route_name='customers_simple', request_method='GET', renderer='json', permission='auth')
def get_customers_simple(request):
	session = db.get_session()
	allcustomers = session.query(Customer).filter(Customer.is_active).all()

	return ModelDictEncoder().encode(allcustomers)

@view_config(route_name='customers', request_method='PUT')
def put_customers(request):
	return Response()

@view_config(route_name='customers', request_method='DELETE')
def delete_customers(request):
	return Response()

@view_config(route_name='customer', request_method='PUT', renderer='json', permission='auth')
def put_customer(request):
	session = db.get_session()
	params = request.json_body
	customer_id = request.matchdict['id']
	customer_name = params.get('customer_name', None)
	customer_address = params.get('customer_address', None)
	customer_terms = params.get('customer_terms', None)
	customer = Customer(id=customer_id, customer_name=customer_name, customer_address=customer_address, customer_terms=customer_terms)
	session.merge(customer)
	session.commit()

	return ModelDictEncoder({Transaction: {'expand': ['details']}}).encode(customer)

@view_config(route_name='customer', request_method='GET', renderer='json', permission='auth')
def get_customer(request):
	session = db.get_session()
	id = request.matchdict['id']
	customer = session.query(Customer).filter(Customer.is_active).filter(Customer.id == id).scalar()

	return ModelDictEncoder().encode(customer)

@view_config(route_name='customer', request_method='DELETE', renderer='json', permission='auth')
def delete_customer(request):
	session = db.get_session()
	id = request.matchdict['id']
	customer = session.query(Customer).filter(Customer.is_active).filter(Customer.id == id).scalar()
	customer.is_active = False
	session.commit()
	return Response()

@view_config(route_name='customer_woods', request_method='GET', renderer='json', permission='auth')
def get_customer_woods(request):
	session = db.get_session()
	id = request.matchdict['id']
	customer = session.query(Customer).filter(Customer.id == id).scalar()
	# discounts = (
		# session.query(, func.row_number().over(
		# partition_by=ProfileDetail.wood_id,
		# order_by=ProfileDetail.discount_percent.desc())
		# .label('row_number'))
		# .filter(ProfileDetail.profile_id == DiscountProfile.id)
		# .filter(CustomerDiscount.profile_id == DiscountProfile.id)
		# .filter(CustomerDiscount.customer_id == customer.id)
		# .filter(DiscountProfile.is_active)
		# .subquery()
	# )

	customer_woods = (
		session.query(Wood.id, Wood.wood_type, Wood.wood_thickness, Wood.wood_width, Wood.wood_height, Wood.wood_stock, func.coalesce(CustomerSpecialPrice.special_price, Wood.wood_price).label('wood_price'))
		.outerjoin(CustomerSpecialPrice, and_(Wood.id == CustomerSpecialPrice.wood_id, customer.id == CustomerSpecialPrice.customer_id))
		.filter(Wood.is_active)
	).all()

	woodslist = []
	for id, type, thickness, width, height, stock, price in customer_woods:
		wooddict = dict()
		wooddict['id'] = id
		wooddict['wood_type'] = type
		wooddict['wood_thickness'] = thickness
		wooddict['wood_width'] = width
		wooddict['wood_height'] = height
		wooddict['wood_stock'] = stock
		wooddict['wood_price'] = price
		woodslist.append(wooddict)

	return ModelDictEncoder().encode(woodslist)

@view_config(route_name='customer_transactions', request_method='GET', renderer='json', permission='auth')
def get_customer_transactions(request):
	session = db.get_session()
	customer_id = request.matchdict['id']

	transaction = (
		session.query(Transaction)
		.filter(Transaction.customer_id == customer_id)
		.all()
	)
	return ModelDictEncoder({Transaction: {'expand': ['customer', 'details', 'amount_payed']}}).encode(transaction)

@view_config(route_name='customer_transaction', request_method='GET', renderer='json', permission='auth')
def get_customer_transaction(request):
	session = db.get_session()
	customer_id = request.matchdict['customer_id']
	transaction_id = request.matchdict['transaction_id']

	transaction = (
		session.query(Transaction)
		.filter(Transaction.id == transaction_id)
		.filter(Transaction.customer_id == customer_id)
		.scalar()
	)
	print(ModelDictEncoder({Transaction: {'expand': ['customer','details']}}).encode(transaction))
	return ModelDictEncoder({Transaction: {'expand': ['customer', 'details']}}).encode(transaction)

@view_config(route_name='customer_transaction', request_method='PUT', renderer='json', permission='auth')
def put_customer_transaction(request):
	pass

@view_config(route_name='customer_discounts', request_method='GET', renderer='json', permission='auth')
def get_customer_discounts(request):
	session = db.get_session()
	id = request.matchdict['id']
	customer = session.query(Customer).filter(Customer.id == id).scalar()
	discounts = (
		session.query(ProfileDetail, func.row_number().over(
		partition_by=ProfileDetail.wood_id,
		order_by=ProfileDetail.discount_percent.desc())
		.label('row_number'))
		.filter(ProfileDetail.profile_id == DiscountProfile.id)
		.filter(CustomerDiscount.profile_id == DiscountProfile.id)
		.filter(CustomerDiscount.customer_id == customer.id)
		.filter(DiscountProfile.is_active)
		.filter(CustomerDiscount.is_active)
		.subquery()
	)

	customer_discounts = (
		session.query(discounts.c.wood_id, discounts.c.discount_percent).filter(discounts.c.row_number == 1)
	).all()

	discountlist = []

	for wood_id, discount_percent in customer_discounts:
		discountdict = dict()
		discountdict['wood_id'] = wood_id
		discountdict['discount_percent'] = discount_percent
		discountlist.append(discountdict)

	return ModelDictEncoder().encode(discountlist)

@view_config(route_name='customer_special_price', request_method='GET', renderer='json', permission='auth')
def get_special_prices(request):
	session = db.get_session()
	id = request.matchdict['id']
	customer = session.query(Customer).filter(Customer.is_active).filter(Customer.id == id).scalar()
	special_prices = (
		session.query(CustomerSpecialPrice)
		.filter(CustomerSpecialPrice.customer_id == customer.id)
		.all()
	)

	specialpricelist = []

	for special_price in special_prices:
		specialpricedict = dict()
		specialpricedict['id'] = special_price.wood_id
		specialpricedict['wood_type'] = special_price.wood.wood_type
		specialpricedict['wood_thickness'] = special_price.wood.wood_thickness
		specialpricedict['wood_width'] = special_price.wood.wood_width
		specialpricedict['wood_height'] = special_price.wood.wood_height
		specialpricedict['discount'] = special_price.special_price
		specialpricelist.append(specialpricedict)

	return ModelDictEncoder().encode(specialpricelist)