from pyramid.view import view_config
from pyramid.response import Response

from woodworks import db
from woodworks.models import Wood
from woodworks.services.encoding import ModelDictEncoder, ModelJSONEncoder
from woodworks.services.inventory import update_stock, _mixed_to_float

from fractions import Fraction

import pyexcel

@view_config(route_name='woods', request_method='POST', renderer='json', permission='auth')
def post_woods(request):
	session = db.get_session()
	# TODO: exception handling
	params = request.json_body
	listwood = []
	for param in params:
		wood_type = param.get('wood_type', None)
		wood_thickness = _mixed_to_float(param.get('wood_thickness', None))
		wood_width = _mixed_to_float(param.get('wood_width', None))
		wood_height = _mixed_to_float(param.get('wood_height', None))
		wood_price = param.get('wood_price', '0')

		wood = (
			session.query(Wood)
			.filter(Wood.wood_type == wood_type)
			.filter(Wood.wood_thickness == wood_thickness)
			.filter(Wood.wood_width == wood_width)
			.filter(Wood.wood_height == wood_height)
			.scalar()
		)

		if wood is not None:
			wood.wood_price = wood_price
			wood.is_active = True
		else:
			wood = Wood(wood_type=wood_type, wood_thickness=wood_thickness, wood_width=wood_width, wood_height=wood_height, wood_price=wood_price)
			session.add(wood)
			listwood.append(wood)
	session.commit()
	listwooddict = ModelDictEncoder().encode(listwood)
	return listwooddict

@view_config(route_name='woods_excel', request_method='POST', renderer='json', permission='auth')
def post_woods_excel(request):
	session = db.get_session()
	response = "Woods added:\n"
	filename = request.POST['filename'].filename;
	extension = filename.split(".")[-1]
	body = request.POST['filename'].file;
	content = body.read()
	# content = content.decode('utf-8')
	sheet = pyexcel.get_sheet(file_type=extension, file_content=content, name_columns_by_row=0)
	listwood = list(sheet.rows())
	# TODO: exception handling
	for onewood in listwood:
		wood_type = onewood[0]
		wood_thickness = _mixed_to_float(onewood[1])
		wood_width = _mixed_to_float(onewood[2])
		wood_height = _mixed_to_float(onewood[3]) if onewood[3] else 0
		wood_stock = onewood[4] if onewood[4] else 0
		wood_price = onewood[5] if onewood[5] else 0

		print(wood_type)
		print(wood_thickness)
		print(wood_width)
		print(wood_height)
		wood_query = (
			session.query(Wood)
			.filter(Wood.wood_type == wood_type)
			.filter(Wood.wood_thickness == wood_thickness)
			.filter(Wood.wood_width == wood_width)
			.filter(Wood.wood_height == wood_height)
		)
		wood = wood_query.scalar()
		if wood is not None:
			wood.wood_price = wood_price
			update_stock(wood.id, wood_stock * -1, session)
			wood.is_active = True
		else:
			wood = Wood(wood_type=wood_type, wood_thickness=wood_thickness, wood_width=wood_width, wood_height=wood_height, wood_stock=wood_stock, wood_price=wood_price)
			session.add(wood)
			response += str("{} {} {} {}\n".format(wood.wood_type, wood.wood_thickness, wood.wood_width, wood.wood_height))
	session.commit()
	return Response(response)

@view_config(route_name='woods', request_method='GET', renderer='json', permission='auth')
def get_woods(request):
	session = db.get_session()
	allwoods = session.query(Wood).filter(Wood.is_active).all()
	return ModelDictEncoder().encode(allwoods)

@view_config(route_name='woods', request_method='PUT')
def put_woods(request):
	return Response()

@view_config(route_name='woods', request_method='DELETE')
def delete_woods(request):
	return Response()

@view_config(route_name='wood', request_method='PUT', renderer='json', permission='auth')
def put_wood(request):
	session = db.get_session()
	params = request.json_body
	wood_id = request.matchdict['id']
	wood_type = params.get('wood_type', None)
	wood_thickness = params.get('wood_thickness', None)
	wood_width = params.get('wood_width', None)
	wood_height = params.get('wood_height', None)
	wood_price = params.get('wood_price', '0')
	wood = Wood(id=wood_id, wood_type=wood_type, wood_thickness=wood_thickness, wood_width=wood_width, wood_height=wood_height, wood_price=wood_price)
	session.merge(wood)
	session.commit()

	return ModelDictEncoder().encode(wood)

@view_config(route_name='wood', request_method='GET', renderer='json', permission='auth')
def get_wood(request):
	session = db.get_session()
	id = request.matchdict['id']
	wood = session.query(Wood).filter(Wood.is_active).filter(Wood.id == id).scalar()

	return ModelDictEncoder().encode(wood)

@view_config(route_name='wood', request_method='DELETE', renderer='json', permission='auth')
def delete_wood(request):
	session = db.get_session()
	id = request.matchdict['id']
	wood = session.query(Wood).filter(Wood.is_active).filter(Wood.id == id).scalar()
	wood.is_active = False
	session.commit()
	return Response()

@view_config(route_name='wood_stock', request_method='POST', renderer='json', permission='auth')
def post_wood_stock(request):
	session = db.get_session()
	id = request.matchdict['id']
	body = request.json_body;
	stock = body.get('num_stock', 0)
	wood = session.query(Wood).filter(Wood.is_active).filter(Wood.id == id).scalar()
	update_stock(wood.id, stock * -1, session)
	session.commit()
	return Response("success")

@view_config(route_name='excel_wood_stock', request_method='POST', renderer='json', permission='auth')
def post_wood_stock_excel(request):
	session = db.get_session()
	response = "Wood stock updated:\n"
	filename = request.POST['filename'].filename;
	extension = filename.split(".")[-1]
	body = request.POST['filename'].file;
	content = body.read()
	# content = content.decode('utf-8')
	sheet = pyexcel.get_sheet(file_type=extension, file_content=content, name_columns_by_row=0)
	listwood = list(sheet.rows())
	for listwoo in listwood:
		wood = (
			session.query(Wood)
			.filter(Wood.is_active)
			.filter(Wood.wood_type == listwoo[0])
			.filter(Wood.wood_thickness == _mixed_to_float(listwoo[1]))
			.filter(Wood.wood_width == _mixed_to_float(listwoo[2]))
			.filter(Wood.wood_height == _mixed_to_float(listwoo[3]))
			.scalar()
		)
		if wood is not None:
			update_stock(wood.id, listwoo[4] * -1, session)
			response += str("{} {} {} {}\n".format(wood.wood_type, wood.wood_thickness, wood.wood_width, wood.wood_height))
		session.commit()
	return Response(response)