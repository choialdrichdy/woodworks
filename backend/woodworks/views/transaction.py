from pyramid.view import view_config
from pyramid.response import Response
from pyramid.httpexceptions import HTTPUnprocessableEntity

from woodworks import db
from woodworks.models import Transaction, TransactionDetail
from woodworks.services.encoding import ModelDictEncoder, ModelJSONEncoder
from woodworks.services.inventory import update_stock
from woodworks.services.transaction import get_wood_from_details


@view_config(route_name='transactions', request_method='GET', renderer='json', permission='auth')
def get_transactions(request):
	session = db.get_session()
	alltransactions = session.query(Transaction).all()
	print(ModelDictEncoder({Transaction: {'expand': ['amount_payed']}}).encode(alltransactions))
	return ModelDictEncoder({Transaction: {'expand': ['amount_payed', 'customer']}}).encode(alltransactions)


@view_config(route_name='transactions', request_method='POST', renderer='json', permission='auth')
def post_transactions(request):
	session = db.get_session()
	params = request.json_body
	print(params)
	delivery_receipt_no = params.get('delivery_receipt_no', None)
	customer_id = params.get('customer_id', None)
	total_amount = params.get('total_amount', None)
	woods = params.get('details', None)
	transaction = Transaction(delivery_receipt_no=delivery_receipt_no, customer_id=customer_id, total_amount=total_amount)
	session.add(transaction)
	session.flush()
	for wood in woods:
		if update_stock(wood.get('wood_id', 0), wood.get('quantity', 0), session):
			transaction_detail = TransactionDetail(
				transaction_id=transaction.id,
				wood_id=wood.get('wood_id', 0),
				bf_lf=wood.get('bf_lf', 0),
				wood_price=wood.get('wood_price', 0),
				discount_percent=wood.get('discount', 0),
				quantity=wood.get('quantity', 0),
				total_price=wood.get('total_price', 0)
			)
			session.add(transaction_detail)
		else:
			raise HTTPUnprocessableEntity("No wood found or not enough stock.")
	# TODO: exception handling
	# params = request.json_body
	# listwood = []
	# for param in params:
		# wood_type = param.get('wood_type', None)
		# wood_length = param.get('wood_length', None)
		# wood_width = param.get('wood_width', None)
		# wood_height = param.get('wood_height', None)
		# wood_price = param.get('wood_price', '0')
		# wood = Wood(wood_type=wood_type, wood_length=wood_length, wood_width=wood_width, wood_height=wood_height, wood_price=wood_price)
		# session.add(wood)
		# listwood.append(wood)
	session.commit()
	# listwooddict = ModelDictEncoder().encode(listwood)
	# return listwooddict
	return Response()

@view_config(route_name='transactions', request_method='PUT', renderer='json', permission='auth')
def put_transactions(request):
	session = db.get_session()
	params = request.json_body
	id = params.get('id', None)
	delivery_receipt_no = params.get('delivery_receipt_no', None)
	customer_id = params.get('customer_id', None)
	total_amount = params.get('total_amount', None)
	woods = params.get('details', None)
	transaction = (
		session.query(Transaction)
		.filter(Transaction.id == id)
		.filter(Transaction.delivery_receipt_no == delivery_receipt_no)
		.scalar()
	)
	if transaction is None:
		raise HTTPUnprocessableEntity("No transaction found.")
	transaction.total_amount = total_amount
	print(transaction.details)
	session.flush()
	for wood in woods:
		transaction_detail = get_wood_from_details(transaction.id, wood.get('wood_id', 0), session)
		if transaction_detail is None:
			if update_stock(wood.get('wood_id', 0), wood.get('quantity', 0), session):
				transaction_detail = TransactionDetail(
				transaction_id=transaction.id,
				wood_id=wood.get('wood_id', 0),
				bf_lf=wood.get('bf_lf', 0),
				wood_price=wood.get('wood_price', 0),
				discount_percent=wood.get('discount_percent', 0),
				quantity=wood.get('quantity', 0),
				total_price=wood.get('total_price', 0)
				)
				session.add(transaction_detail)
			else:
				raise HTTPUnprocessableEntity("No wood found or not enough stock.")
		else:
			difference = wood.get('quantity', 0) - transaction_detail.quantity
			if difference != 0:
				if update_stock(transaction_detail.wood_id, difference, session):
					transaction_detail.discount_percent = wood.get('discount_percent', 0)
					transaction_detail.quantity = wood.get('quantity', 0)
					transaction_detail.bf_lf = wood.get('bf_lf', 0)
					transaction_detail.total_price = wood.get('total_price', 0)
				else:
					raise HTTPUnprocessableEntity("No wood found or not enough stock.")
	session.commit()
	return Response()

@view_config(route_name='transaction_details', request_method='GET', renderer='json', permission='auth')
def get_details(request):
	session = db.get_session()
	customer_id = request.matchdict['customer_id']
	transaction_id = request.matchdict['transaction_id']

	details = (
		session.query(TransactionDetail)
		.filter(TransactionDetail.transaction_id == transaction_id)
		.all()
	)
	print(ModelDictEncoder({}).encode(details))
	return ModelDictEncoder().encode(details)

@view_config(route_name='transaction_export', request_method='GET', renderer='json', permission='auth')
def export_transaction(request):
	session = db.get_session()
	transaction_id = request.matchdict['id']
	transaction = session.query(Transaction).filter(Transaction.id == transaction_id).first()
	if transaction is not None:
		transactiondict = dict()
		transactiondict['customer'] = transaction.customer.customer_name
		transactiondict['address'] = transaction.customer.customer_address
		transactiondict['dr_no'] = transaction.delivery_receipt_no
		transactiondict['date'] = transaction.created_at.strftime('%m/%d/%y')
		detailslist = []
		for detail in transaction.details:
			detaildict = dict()
			detaildict['qty'] = detail.quantity
			detaildict['description'] = detail.wood.wood_type
			detaildict['thickness'] = detail.wood.wood_thickness
			detaildict['width'] = detail.wood.wood_width
			detaildict['height'] = detail.wood.wood_height
			detaildict['bf_lf'] = detail.bf_lf
			detaildict['price'] = detail.wood_price
			detaildict['less'] = detail.discount_percent
			detaildict['total_amount'] = detail.total_price
			detailslist.append(detaildict)
		transactiondict['details'] = detailslist
		transactiondict['total'] = transaction.total_amount
		return ModelDictEncoder().encode(transactiondict)
