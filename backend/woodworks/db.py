from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker


def get_session():
	engine = create_engine('postgresql://woodworks:woodworks@localhost/woodworks', pool_size=20, max_overflow=0)
	Session = sessionmaker(bind=engine)
	session = Session()
	return session