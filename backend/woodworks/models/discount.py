from sqlalchemy import Column, UnicodeText, Integer, DateTime, Float, ForeignKey, Boolean
from sqlalchemy.sql import func, true, false
from sqlalchemy.orm import relationship

from woodworks.models.base import Base


class DiscountProfile(Base):
	__tablename__ = "discount_profiles"

	__default_exclude__ = ['created_at', 'updated_at']
	__default_include__ = ['details']

	id = Column(Integer, primary_key=True)
	profile_name = Column(UnicodeText, nullable=False)
	is_active = Column(Boolean, nullable=False, server_default=true())

	created_at = Column(DateTime(timezone=True), server_default=func.now(), nullable=False)
	updated_at = Column(DateTime(timezone=True), server_default=func.now(), onupdate=func.now(), nullable=False)

	details = relationship("ProfileDetail")

class ProfileDetail(Base):
	__tablename__ = "profile_details"

	profile_id = Column(Integer, ForeignKey('discount_profiles.id'), primary_key=True)
	wood_id = Column(Integer, ForeignKey('woods.id'), primary_key=True)
	discount_percent = Column(Float, nullable=False)

	profile = relationship("DiscountProfile")
	wood = relationship("Wood")


class CustomerDiscount(Base):
	__tablename__ = "customer_discounts"

	customer_id = Column(Integer, ForeignKey('customers.id'), primary_key=True)
	profile_id = Column(Integer, ForeignKey('discount_profiles.id'), primary_key=True)
	is_active = Column(Boolean, nullable=False, server_default=true())

	created_at = Column(DateTime(timezone=True), server_default=func.now(), nullable=False)
	updated_at = Column(DateTime(timezone=True), server_default=func.now(), onupdate=func.now(), nullable=False)

	customer = relationship("Customer")
	profile = relationship("DiscountProfile")


class CustomerSpecialPrice(Base):
	__tablename__ = "customer_special_prices"

	customer_id = Column(Integer, ForeignKey('customers.id'), primary_key=True)
	wood_id = Column(Integer, ForeignKey('woods.id'), primary_key=True)
	special_price = Column(Float, nullable=False)
	is_active = Column(Boolean, nullable=False, server_default=true())

	created_at = Column(DateTime(timezone=True), server_default=func.now(), nullable=False)
	updated_at = Column(DateTime(timezone=True), server_default=func.now(), onupdate=func.now(), nullable=False)

	customer = relationship("Customer")
	wood = relationship("Wood")