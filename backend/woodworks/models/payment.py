from sqlalchemy import Column, UnicodeText, Integer, Date, DateTime, Float, Boolean, ForeignKey
from sqlalchemy.sql import func, false
from sqlalchemy.orm import relationship

from woodworks.models.base import Base


class Payment(Base):
	__tablename__ = "payments"

	__default_exclude__ = ['created_at', 'updated_at']

	id = Column(Integer, primary_key=True)
	customer_id = Column(Integer, ForeignKey("customers.id"), nullable=False)
	user_id = Column(Integer, ForeignKey("users.id"), nullable=False)
	amount_paid = Column(Float, nullable=False)
	payment_type = Column(UnicodeText, nullable=False)
	remarks = Column(UnicodeText)

	is_cleared = Column(Boolean, nullable=False, server_default=false())
	payment_date = Column(Date, nullable=False, server_default=func.now())
	created_at = Column(DateTime(timezone=True), server_default=func.now(), nullable=False)
	updated_at = Column(DateTime(timezone=True), server_default=func.now(), onupdate=func.now(), nullable=False)

	customer = relationship("Customer")
	payments = relationship("PaymentDetail")

	__mapper_args__ = {'polymorphic_on': 'payment_type'}

class CheckPayment(Payment):
	__mapper_args__ = {'polymorphic_identity': 'check'}
	check_number = Column(UnicodeText, nullable=True)
	check_date = Column(Date, nullable=False)
	check_bank = Column(UnicodeText, nullable=True)
	check_branch = Column(UnicodeText, nullable=True)
	
class CashPayment(Payment):
	__mapper_args__ = {'polymorphic_identity': 'cash'}

class PaymentDetail(Base):
	__tablename__ = "payment_details"

	transaction_id = Column(Integer, ForeignKey("transactions.id"), nullable=False, primary_key=True)
	payment_id = Column(Integer, ForeignKey("payments.id"), nullable=False, primary_key=True)
	payment_amount = Column(Float, nullable=False)

	created_at = Column(DateTime(timezone=True), server_default=func.now(), nullable=False)
	updated_at = Column(DateTime(timezone=True), server_default=func.now(), onupdate=func.now(), nullable=False)

	transaction = relationship("Transaction")
	payment = relationship("Payment")
