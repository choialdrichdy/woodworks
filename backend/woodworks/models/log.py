from sqlalchemy import Column, UnicodeText, Integer, DateTime, Float, ForeignKey
from sqlalchemy.sql import func
from sqlalchemy.orm import relationship
from woodworks.models.base import Base

# TODO: 'http://docs.sqlalchemy.org/en/latest/orm/extensions/declarative/inheritance.html#resolving-column-conflicts'
class Log(Base):
	__tablename__ = "logs"

	__default_exclude__ = ['created_at', 'updated_at']

	id = Column(Integer, primary_key=True)
	user_id = Column(Integer, ForeignKey('users.id'))
	type = Column(UnicodeText)

	created_at = Column(DateTime(timezone=True), server_default=func.now(), nullable=False)
	updated_at = Column(DateTime(timezone=True), server_default=func.now(), onupdate=func.now(), nullable=False)

	user = relationship("User")
	__mapper_args__ = {'polymorphic_on': 'type'}


class LoginLog(Log):
	__mapper_args__ = {'polymorphic_identity': 'login'}

class StockLog(Log):
	__mapper_args__ = {'polymorphic_identity': 'update-wood-stock'}
	wood_id = Column(Integer, ForeignKey('woods.id'))
	number_of_stock = Column(Integer, server_default='0')
