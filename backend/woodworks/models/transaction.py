from sqlalchemy import Column, UnicodeText, Integer, Date, DateTime, Float, UniqueConstraint, CheckConstraint, ForeignKey
from sqlalchemy.sql import func
from sqlalchemy.orm import relationship

from woodworks import db
from woodworks.models.payment import PaymentDetail
from woodworks.models.base import Base


class Transaction(Base):
	__tablename__ = "transactions"

	__default_exclude__ = []

	id = Column(Integer, primary_key=True)
	delivery_receipt_no = Column(UnicodeText, nullable=False, unique=True)
	customer_id = Column(Integer, ForeignKey('customers.id'))
	transaction_status = Column(UnicodeText, nullable=False, server_default='new')
	total_amount = Column(Float, nullable=False)
	transaction_date = Column(Date, nullable=False, server_default=func.now())

	created_at = Column(DateTime(timezone=True), server_default=func.now(), nullable=False)
	updated_at = Column(DateTime(timezone=True), server_default=func.now(), onupdate=func.now(), nullable=False)

	customer = relationship("Customer")
	details = relationship("TransactionDetail")

	@property
	def amount_payed(self):
		session = db.get_session()
		amount_paid = (
			session.query(func.coalesce(func.sum(PaymentDetail.payment_amount),0))
			.filter(PaymentDetail.transaction_id == self.id)
			.scalar()
		)

		return amount_paid

	def amount_paid_without_one(self, payment_id):
		session = db.get_session()
		amount_paid = (
			session.query(func.coalesce(func.sum(PaymentDetail.payment_amount),0))
			.filter(PaymentDetail.transaction_id == self.id)
			.filter(PaymentDetail.payment_id != payment_id)
			.scalar()
		)
		return amount_paid


class TransactionDetail(Base):
	__tablename__ = "transaction_details"

	__default_expand__ = ['wood']
	__default_exclude__ = []

	transaction_id = Column(Integer, ForeignKey('transactions.id'), primary_key=True)
	wood_id = Column(Integer, ForeignKey('woods.id'), primary_key=True)
	bf_lf = Column(Integer, nullable=False, server_default='0')
	wood_price = Column(Float, nullable=False)
	discount_percent = Column(Float, nullable=False, server_default='0')
	quantity = Column(Integer, nullable=False)
	total_price = Column(Float, nullable=False)

	created_at = Column(DateTime(timezone=True), server_default=func.now(), nullable=False)
	updated_at = Column(DateTime(timezone=True), server_default=func.now(), onupdate=func.now(), nullable=False)

	transaction = relationship("Transaction")
	wood = relationship("Wood")