from sqlalchemy import Column, UnicodeText, Integer, DateTime, Float, Boolean, UniqueConstraint, CheckConstraint
from sqlalchemy.sql import func, true
from woodworks.models.base import Base


class Wood(Base):
	__tablename__ = "woods"

	__default_exclude__ = ['created_at', 'updated_at']

	id = Column(Integer, primary_key=True)
	wood_type = Column(UnicodeText, nullable=False)
	wood_thickness = Column(Float, nullable=False)
	wood_width = Column(Float, nullable=False)
	wood_height = Column(Float, nullable=False)
	wood_price = Column(Float, nullable=True, server_default='0')
	wood_stock = Column(Integer, nullable=True, server_default='0')
	is_active = Column(Boolean, nullable=False, server_default=true())

	created_at = Column(DateTime(timezone=True), server_default=func.now(), nullable=False)
	updated_at = Column(DateTime(timezone=True), server_default=func.now(), onupdate=func.now(), nullable=False)

	__table_args__ = (
		UniqueConstraint(wood_type, wood_thickness, wood_width, wood_height),
		CheckConstraint('wood_price >= 0', name='check_wood_price')
	)