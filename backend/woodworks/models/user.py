from sqlalchemy import Column, UnicodeText, Integer, DateTime, Boolean, CheckConstraint, Text, ARRAY
from sqlalchemy.sql import func, true
from woodworks.models.base import Base


class User(Base):
	__tablename__ = "users"

	__default_exclude__ = ['password', 'created_at', 'updated_at']

	id = Column(Integer, primary_key=True)
	first_name = Column(UnicodeText, CheckConstraint('char_length(first_name) < 100'), nullable=False, server_default='')
	last_name = Column(UnicodeText,  CheckConstraint('char_length(first_name) < 100'), nullable=False, server_default='')
	username = Column(UnicodeText, nullable=False, unique=True)
	password = Column(Text, nullable=False)
	user_permissions = Column(ARRAY(UnicodeText), server_default='{}')
	is_active = Column(Boolean, nullable=False, server_default=true())

	created_at = Column(DateTime(timezone=True), server_default=func.now(), nullable=False)
	updated_at = Column(DateTime(timezone=True), server_default=func.now(), onupdate=func.now(), nullable=False)

