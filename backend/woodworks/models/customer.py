from sqlalchemy import Column, UnicodeText, Integer, DateTime, Boolean
from sqlalchemy.sql import func, true
from sqlalchemy.orm import relationship

from woodworks import db
from woodworks.models import Transaction
from woodworks.models.payment import PaymentDetail, Payment
from woodworks.models.base import Base


class Customer(Base):
	__tablename__ = "customers"

	__default_exclude__ = ['created_at', 'updated_at']
	__default_expand__ = []

	id = Column(Integer, primary_key=True)
	customer_name = Column(UnicodeText, nullable=False)
	customer_address = Column(UnicodeText, nullable=True, server_default='')
	customer_terms = Column(Integer, nullable=False, server_default='0')
	is_active = Column(Boolean, nullable=False, server_default=true())

	created_at = Column(DateTime(timezone=True), server_default=func.now(), nullable=False)
	updated_at = Column(DateTime(timezone=True), server_default=func.now(), onupdate=func.now(), nullable=False)

	transactions = relationship("Transaction")
	payments = relationship("Payment")

	@property
	def current_balance(self):
		total = 0
		cleared = 0
		for transaction in self.transactions:
			total += transaction.total_amount
		for payment in self.payments:
			if payment.is_cleared:
				cleared += payment.amount_paid
		return total - cleared
		# session = db.get_session()
		# amount_cleared = (
		# 	session.query(func.coalesce(func.sum(PaymentDetail.payment_amount), 0))
		# 	.filter(PaymentDetail.transaction_id == Transaction.id)
		# 	.filter(Transaction.customer_id == self.id)
		# 	.filter(Payment.id == PaymentDetail.payment_id)
		# 	.filter(Payment.is_cleared)
		# 	.scalar()
		# )
		# total = (
		# 	session.query(func.coalesce(func.sum(Transaction.total_amount), 0))
		# 	.filter(Transaction.customer_id == self.id)
		# 	.scalar()
		# )
		# return total - amount_cleared

	@property
	def pdc(self):
		not_cleared = 0
		for payment in self.payments:
			if not payment.is_cleared:
				not_cleared += payment.amount_paid
		return not_cleared
		# session = db.get_session()
		# amount_not_cleared = (
		# 	session.query(func.coalesce(func.sum(PaymentDetail.payment_amount), 0))
		# 	.filter(PaymentDetail.transaction_id == Transaction.id)
		# 	.filter(Transaction.customer_id == self.id)
		# 	.filter(Payment.id == PaymentDetail.payment_id)
		# 	.filter(~Payment.is_cleared)
		# 	.scalar()
		# )
		
		# return amount_not_cleared
