from woodworks import db
from woodworks.models import Wood, StockLog
from fractions import Fraction


def update_stock(wood_id, buying, session=None):
	if session is None:
		session = db.get_session()
	wood = (
		session.query(Wood)
		.filter(Wood.id == wood_id)
		.filter(Wood.is_active)
		.scalar()
	)

	if wood is not None and (wood.wood_stock - buying) >= 0:
		wood.wood_stock = wood.wood_stock - buying
		session.add(StockLog(wood_id=wood.id, number_of_stock=buying))
		session.flush()
		return True
	else:
		return False

def _mixed_to_float(mixed_number):
	print(type(mixed_number))
	print(mixed_number)
	if type(mixed_number) is float or type(mixed_number) is int:
		return mixed_number
	chunks = mixed_number.split(' ')
	result = 0;
	for x in range(0, len(chunks)):
		result = result + float(Fraction(chunks[x]))
	return result