from woodworks import db
from woodworks.models import CustomerDiscount, Customer, CustomerSpecialPrice


def reset_profile(profile, session=None):
	if session is None:
		session = db.get_session()
	customer_profiles = session.query(CustomerDiscount).filter(CustomerDiscount.profile_id == profile.id).all()
	for customer_profile in customer_profiles:
		customer_profile.is_active = False
	return customer_profiles

def reset_special_price(customer, session=None):
	if session is None:
		session = db.get_session()
	customer_special_prices = session.query(CustomerSpecialPrice).filter(CustomerSpecialPrice.customer_id == customer.id).delete()
	session.flush()