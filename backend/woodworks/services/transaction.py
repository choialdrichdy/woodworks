from woodworks import db
from woodworks.models import TransactionDetail

def get_wood_from_details(transaction_id, wood_id, session=None):
	if session is None:
		session = db.get_session()
	wood = (
		session.query(TransactionDetail)
		.filter(TransactionDetail.transaction_id == transaction_id)
		.filter(TransactionDetail.wood_id == wood_id)
		.scalar()
	)

	return wood