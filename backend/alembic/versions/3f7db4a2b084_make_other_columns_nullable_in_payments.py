"""Make other columns nullable in payments

Revision ID: 3f7db4a2b084
Revises: 78d41298949b
Create Date: 2017-03-19 19:44:36.363162

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import postgresql

# revision identifiers, used by Alembic.
revision = '3f7db4a2b084'
down_revision = '78d41298949b'
branch_labels = None
depends_on = None

def upgrade():
    ### commands auto generated by Alembic - please adjust! ###
    op.alter_column('payments', 'check_date',
               existing_type=postgresql.TIMESTAMP(),
               nullable=True)
    op.alter_column('payments', 'check_number',
               existing_type=sa.TEXT(),
               nullable=True,
               existing_server_default=sa.text("''::text"))
    op.alter_column('payments', 'is_cleared',
               existing_type=sa.BOOLEAN(),
               nullable=True,
               existing_server_default=sa.text('false'))
    ### end Alembic commands ###


def downgrade():
    ### commands auto generated by Alembic - please adjust! ###
    op.alter_column('payments', 'is_cleared',
               existing_type=sa.BOOLEAN(),
               nullable=False,
               existing_server_default=sa.text('false'))
    op.alter_column('payments', 'check_number',
               existing_type=sa.TEXT(),
               nullable=False,
               existing_server_default=sa.text("''::text"))
    op.alter_column('payments', 'check_date',
               existing_type=postgresql.TIMESTAMP(),
               nullable=False)
    ### end Alembic commands ###
