"""Adds is_active columns

Revision ID: 08a9604a7a90
Revises: 9d57140996fc
Create Date: 2016-12-09 19:46:07.130197

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '08a9604a7a90'
down_revision = '9d57140996fc'
branch_labels = None
depends_on = None

def upgrade():
    ### commands auto generated by Alembic - please adjust! ###
    op.add_column('customers', sa.Column('is_active', sa.Boolean(), server_default=sa.text('true'), nullable=False))
    op.add_column('discount_profiles', sa.Column('is_active', sa.Boolean(), server_default=sa.text('true'), nullable=False))
    op.add_column('users', sa.Column('is_active', sa.Boolean(), server_default=sa.text('true'), nullable=False))
    op.add_column('woods', sa.Column('is_active', sa.Boolean(), server_default=sa.text('true'), nullable=False))
    ### end Alembic commands ###


def downgrade():
    ### commands auto generated by Alembic - please adjust! ###
    op.drop_column('woods', 'is_active')
    op.drop_column('users', 'is_active')
    op.drop_column('discount_profiles', 'is_active')
    op.drop_column('customers', 'is_active')
    ### end Alembic commands ###
